
SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `env`.`cms_category`;
DROP TABLE IF EXISTS `env`.`cms_content`;

CREATE TABLE `cms_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(50) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(50) DEFAULT NULL COMMENT '最后修改人',
  `update_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  `del` int(1) DEFAULT NULL COMMENT '删除标志默认:0,删除:1',
  `status` int(1) DEFAULT NULL COMMENT '状态0:待审核;1:发布;2:取消发布;',
  `category_title` varchar(255) DEFAULT NULL COMMENT '栏目名称',
  `category_type` varchar(10) DEFAULT NULL COMMENT '栏目类型:0:列表;1:封面;2:链接;',
  `category_parent_id` varchar(255) DEFAULT NULL COMMENT '父类ID',
  `category_parent_name` varchar(255) DEFAULT NULL COMMENT '父类名称',
  `category_img` varchar(2000) DEFAULT NULL COMMENT '缩略图',
  `category_flag` varchar(255) DEFAULT NULL COMMENT '栏目属性',
  `category_descrip` varchar(500) DEFAULT NULL COMMENT '栏目描述',
  `category_keyword` varchar(300) DEFAULT NULL COMMENT '栏目关键字',
  `category_path` varchar(255) DEFAULT NULL COMMENT '栏目路径',
  `dict_id` int(11) DEFAULT NULL COMMENT '字典对应编号',
  `app_id` int(11) DEFAULT NULL COMMENT '所属应用ID',
  `app_code` int(11) DEFAULT NULL COMMENT '所属应用CODE',
  `category_manager_id` int(11) DEFAULT NULL COMMENT '发布用户id',
  `category_datetime` datetime DEFAULT NULL COMMENT '发布时间',
  `mdiy_model_id` varchar(255) DEFAULT NULL COMMENT '栏目内容模型id',
  `category_diy_url` varchar(255) DEFAULT NULL COMMENT '自定义链接',
  `category_url` varchar(50) DEFAULT NULL COMMENT '内容模板',
  `category_list_url` varchar(50) DEFAULT NULL COMMENT '列表模板',
  `category_sort` int(11) DEFAULT NULL COMMENT '自定义顺序',
  `category_pinyin` varchar(255) DEFAULT NULL COMMENT '拼音检索',
  `back1` varchar(50) DEFAULT NULL COMMENT '备用1',
  `back2` varchar(50) DEFAULT NULL COMMENT '备用2',
  `back3` varchar(50) DEFAULT NULL COMMENT '备用3',
  `back4` varchar(50) DEFAULT NULL COMMENT '备用4',
  `back5` varchar(50) DEFAULT NULL COMMENT '备用5',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=150 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='栏目分类'; 

CREATE TABLE `cms_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(50) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(50) DEFAULT NULL COMMENT '最后修改人',
  `update_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  `del` int(1) DEFAULT NULL COMMENT '删除标志默认:0,删除:1',
  `status` int(1) DEFAULT NULL COMMENT '状态0:待审核;1:发布;2:取消发布;',
  `content_title` varchar(255) DEFAULT NULL COMMENT '内容标题',
  `content_category_id` varchar(255) DEFAULT NULL COMMENT '所属栏目',
  `app_id` int(11) DEFAULT NULL COMMENT '所属应用ID',
  `app_code` int(11) DEFAULT NULL COMMENT '所属应用CODE',
  `content_url` varchar(255) DEFAULT NULL COMMENT '跳转链接地址',
  `content_keyword` varchar(255) DEFAULT NULL COMMENT '关键字',
  `content_img` varchar(1000) DEFAULT NULL COMMENT '缩略图',
  `content_sort` int(11) DEFAULT NULL COMMENT '自定义顺序',
  `content_datetime` datetime DEFAULT NULL COMMENT '发布时间',
  `content_source` varchar(255) DEFAULT NULL COMMENT '来源',
  `content_author` varchar(255) DEFAULT NULL COMMENT '作者',
  `content_display` varchar(10) DEFAULT NULL COMMENT '是否显示',
  `content_type` varchar(100) DEFAULT NULL COMMENT '内容类型，来源字典',
  `content_hit` bigint(22) DEFAULT NULL COMMENT '点击次数',
  `content_description` varchar(400) DEFAULT NULL COMMENT '描述',
  `back1` varchar(50) DEFAULT NULL COMMENT '备用1',
  `back2` varchar(50) DEFAULT NULL COMMENT '备用2',
  `back3` varchar(50) DEFAULT NULL COMMENT '备用3',
  `back4` varchar(50) DEFAULT NULL COMMENT '备用4',
  `back5` varchar(50) DEFAULT NULL COMMENT '备用5',
  `content_details` text COMMENT '内容',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=223 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='内容文章';


COMMIT;
