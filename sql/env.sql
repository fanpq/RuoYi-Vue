/*
MySQL Backup
Database: env
Backup Time: 2020-12-12 14:51:26
*/

SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `env`.`rec_company`;
DROP TABLE IF EXISTS `env`.`rec_cv`;
DROP TABLE IF EXISTS `env`.`rec_deliver`;
DROP TABLE IF EXISTS `env`.`rec_log`;
DROP TABLE IF EXISTS `env`.`rec_position`;
DROP TABLE IF EXISTS `env`.`rec_user`;
CREATE TABLE `rec_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` int(11) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) DEFAULT NULL COMMENT '最后修改人',
  `update_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  `del` int(1) DEFAULT NULL COMMENT '删除标志默认0，删除标志：1',
  `status` int(1) DEFAULT NULL COMMENT '状态0：待审核；1：有效；2：无效：',
  `user_id` int(11) DEFAULT NULL COMMENT '关联用户ID，来源于招聘用户表ID',
  `user_name` varchar(50) DEFAULT NULL COMMENT '关联用户名称，来源于招聘用户表',
  `com_name` varchar(255) DEFAULT NULL COMMENT '企业名称',
  `com_photo_url` varchar(100) DEFAULT NULL COMMENT '企业照片',
  `com_adr` varchar(255) DEFAULT NULL COMMENT '企业地址',
  `com_long` varchar(30) DEFAULT NULL COMMENT '地址经度',
  `com_lat` varchar(30) DEFAULT NULL COMMENT '地址纬度',
  `com_short_name` varchar(50) DEFAULT NULL COMMENT '企业简称',
  `industry` varchar(50) DEFAULT NULL COMMENT '所属行业',
  `com_num_people` int(1) DEFAULT NULL COMMENT '公司人数0:20以下;1:20-150人;2:150-500人;3:500-99人9;4:1000以上',
  `com_people` varchar(50) DEFAULT NULL COMMENT '法人代表',
  `com_reg_capital` int(11) DEFAULT NULL COMMENT '注册资本万元',
  `com_reg_date` date DEFAULT NULL COMMENT '成立时间',
  `com_desc` text COMMENT '公司简介',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `back1` varchar(50) DEFAULT NULL COMMENT '备用1',
  `back2` varchar(50) DEFAULT NULL COMMENT '备用2',
  `back3` varchar(50) DEFAULT NULL COMMENT '备用3',
  `back4` varchar(50) DEFAULT NULL COMMENT '备用4',
  `back5` varchar(50) DEFAULT NULL COMMENT '备用5',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='企业信息';
CREATE TABLE `rec_cv` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` int(11) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) DEFAULT NULL COMMENT '最后修改人',
  `update_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  `del` int(1) DEFAULT NULL COMMENT '删除标志默认0，删除标志：1',
  `status` int(1) DEFAULT NULL COMMENT '状态0：编辑；1：待审核；2：发布：3：取消发布；',
  `user_id` int(11) DEFAULT NULL COMMENT '关联用户ID，来源于招聘用户表ID',
  `user_name` varchar(50) DEFAULT NULL COMMENT '关联用户名称，来源于招聘用户表',
  `cv_type` int(1) DEFAULT NULL COMMENT '类型0：求职简历；1：专家简历',
  `cv_no` varchar(50) DEFAULT NULL COMMENT '简历编号',
  `name` varchar(255) DEFAULT NULL COMMENT '姓名',
  `photo_url` varchar(100) DEFAULT NULL COMMENT '头像',
  `sex` int(1) DEFAULT NULL COMMENT '性别0:女；1:男',
  `birthday` date DEFAULT NULL COMMENT '出生日期',
  `work_status` int(1) DEFAULT NULL COMMENT '当前在职状态0：在校-实习；1：在校-找工作；2：在职-考虑机会；3：在职-月内到岗；5：在职-暂不考虑；6：离职-立即到岗',
  `school` varchar(50) DEFAULT NULL COMMENT '毕业院校',
  `qualifications` int(1) DEFAULT NULL COMMENT '学历1:高/中专及以下；2：大专：3：本科；4：硕士；5：博士及以上',
  `speciality` varchar(50) DEFAULT NULL COMMENT '毕业专业',
  `graduate_date` date DEFAULT NULL COMMENT '毕业时间',
  `req_positon` varchar(50) DEFAULT NULL COMMENT '求职职岗位',
  `req_city` varchar(50) DEFAULT NULL COMMENT '期望城市',
  `req_industry` varchar(50) DEFAULT NULL COMMENT '期望行业',
  `req_salary` int(11) DEFAULT NULL COMMENT '期望薪资',
  `self_desc` varchar(255) DEFAULT NULL COMMENT '自我描述',
  `pro_desc` varchar(511) DEFAULT NULL COMMENT '专业及擅长描述',
  `cv_desc` text COMMENT '专家介绍类型为1时使用，富文本编辑',
  `cv_url` varchar(100) DEFAULT NULL COMMENT '简历',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `back1` varchar(50) DEFAULT NULL COMMENT '备用1',
  `back2` varchar(50) DEFAULT NULL COMMENT '备用2',
  `back3` varchar(50) DEFAULT NULL COMMENT '备用3',
  `back4` varchar(50) DEFAULT NULL COMMENT '备用4',
  `back5` varchar(50) DEFAULT NULL COMMENT '备用5',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='简历信息';
CREATE TABLE `rec_deliver` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` int(11) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) DEFAULT NULL COMMENT '最后修改人',
  `update_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  `del` int(1) DEFAULT NULL COMMENT '删除标志默认0，删除标志：1',
  `status` int(1) DEFAULT NULL COMMENT '状态0：待沟通；1：已入职；2：不合适；3、已剔除',
  `user_id` int(11) DEFAULT NULL COMMENT '简历用户ID，来源于招聘用户表ID',
  `user_name` varchar(50) DEFAULT NULL COMMENT '简历用户名称，来源于招聘用户表',
  `com_user_id` int(11) DEFAULT NULL COMMENT '公司关联用户ID，来源于招聘用户表ID',
  `com_user_name` varchar(50) DEFAULT NULL COMMENT '公司关联用户名称，来源于招聘用户表',
  `com_id` int(11) DEFAULT NULL COMMENT '公司ID',
  `com_name` varchar(255) DEFAULT NULL COMMENT '公司名称',
  `cv_id` int(11) DEFAULT NULL COMMENT '简历ID',
  `cv_name` varchar(255) DEFAULT NULL COMMENT '简历名称',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `back1` varchar(50) DEFAULT NULL COMMENT '备用1',
  `back2` varchar(50) DEFAULT NULL COMMENT '备用2',
  `back3` varchar(50) DEFAULT NULL COMMENT '备用3',
  `back4` varchar(50) DEFAULT NULL COMMENT '备用4',
  `back5` varchar(50) DEFAULT NULL COMMENT '备用5',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='简历投递表';
CREATE TABLE `rec_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` int(11) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) DEFAULT NULL COMMENT '最后修改人',
  `update_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  `del` int(1) DEFAULT NULL COMMENT '删除标志默认0，删除标志：1',
  `status` int(1) DEFAULT NULL COMMENT '状态0：编辑；1：待审核；2：发布：3：取消发布；',
  `log_obj_id` int(11) DEFAULT NULL COMMENT '日志记录对象ID',
  `log_type` int(2) DEFAULT NULL COMMENT '日志记录类型，0:浏览;1、查看;2、简历收藏;3、简历投递;4、公司收藏',
  `log_user_id` int(11) DEFAULT NULL COMMENT '操作人id，系统用户ID',
  `log_user_name` varchar(50) DEFAULT NULL COMMENT '操作人姓名，系统用户名称',
  `log_abstract` varchar(255) DEFAULT NULL COMMENT '日志摘要',
  `obj1` varchar(50) DEFAULT NULL COMMENT '自定义对象1',
  `obj2` varchar(50) DEFAULT NULL COMMENT '自定义对象2',
  `obj3` varchar(50) DEFAULT NULL COMMENT '自定义对象3',
  `obj4` varchar(50) DEFAULT NULL COMMENT '自定义对象4',
  `obj5` varchar(50) DEFAULT NULL COMMENT '自定义对象5',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `text` varchar(1024) DEFAULT NULL COMMENT '自定义json',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='操作日志记录';
CREATE TABLE `rec_position` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` int(11) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) DEFAULT NULL COMMENT '最后修改人',
  `update_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  `del` int(1) DEFAULT NULL COMMENT '删除标志默认0，删除标志：1',
  `status` int(1) DEFAULT NULL COMMENT '状态0：编辑；1：待审核；2：发布：3：取消发布；',
  `com_id` int(11) DEFAULT NULL COMMENT '所属企业',
  `com_name` varchar(255) DEFAULT NULL COMMENT '所属企业名称',
  `recruit_type` int(1) DEFAULT NULL COMMENT '招聘类型0:应届校招；1:实习生；2：社招：3:兼职',
  `position_name` varchar(50) DEFAULT NULL COMMENT '职位名称',
  `work_city` varchar(50) DEFAULT NULL COMMENT '工作城市',
  `work_adr` varchar(50) DEFAULT NULL COMMENT '工作地点',
  `exp_req` int(1) DEFAULT NULL COMMENT '经验要求0：不限；1：1-3年；2：3-5年：3：5-10年；4：10年以上',
  `edu_req` int(1) DEFAULT NULL COMMENT '学历要求0:不限;1:中专及以上;2:大专及以上;3:本科及以上;4:硕士及以上;5:博士及以上',
  `salary_req` int(1) DEFAULT NULL COMMENT '月薪资范围:0:面议；1：5k以下；2：5-8k；3：8-12k；4：12-18k；5：18-25k；6：25k以上',
  `salary_begin` int(11) DEFAULT NULL COMMENT '月薪资范围起K',
  `salary_end` int(11) DEFAULT NULL COMMENT '月薪资范围止K',
  `post_name` varchar(50) DEFAULT NULL COMMENT '岗位名称',
  `post_key` varchar(255) DEFAULT NULL COMMENT '岗位关键词',
  `post_desc` text COMMENT '岗位描述',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `back1` varchar(50) DEFAULT NULL COMMENT '备用1',
  `back2` varchar(50) DEFAULT NULL COMMENT '备用2',
  `back3` varchar(50) DEFAULT NULL COMMENT '备用3',
  `back4` varchar(50) DEFAULT NULL COMMENT '备用4',
  `back5` varchar(50) DEFAULT NULL COMMENT '备用5',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='职位信息';
CREATE TABLE `rec_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` int(11) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) DEFAULT NULL COMMENT '最后修改人',
  `update_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  `del` int(1) DEFAULT NULL COMMENT '删除标志默认0，删除标志：1',
  `status` int(1) DEFAULT NULL COMMENT '状态注册即为有效',
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID来源于系统用户表ID',
  `user_type` int(1) DEFAULT NULL COMMENT '用户类型0：招聘企事业；1：求职者；2：专家人才；',
  `user_name` varchar(50) DEFAULT NULL COMMENT '用户名称类型为0时，则手工输入\r类型为1or2时，则为姓名',
  `sex` int(1) DEFAULT NULL COMMENT '性别类型为1or2时用，0：女；1：男',
  `position_name` varchar(50) DEFAULT NULL COMMENT '职位类型为0时用',
  `industry` varchar(50) DEFAULT NULL COMMENT '从事行业类型为2时用',
  `com_id` int(11) DEFAULT NULL COMMENT '所属企业ID类型为0时，如发布招聘信息，则自动会写',
  `com_name` varchar(255) DEFAULT NULL COMMENT '企事业单位名称类型为0时，如发布招聘信息，则自动会写\r类型为2时，则手工输入',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱类型为0时用',
  `user_identity` int(1) DEFAULT NULL COMMENT '求职身份类型为1时用，0：在校生-在校/应届；1：无经验-往届；2：职场人-正式工作经历',
  `birthday` date DEFAULT NULL COMMENT '出生日期类型为1时用',
  `first_work_date` date DEFAULT NULL COMMENT '参加工作年月类型为1时用',
  `work_status` int(1) DEFAULT NULL COMMENT '当前求职状态类型为1时用，0：在校-实习；1：在校-找工作；2：在职-考虑机会；3：在职-月内到岗；5：在职-暂不考虑；6：离职-立即到岗',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `back1` varchar(50) DEFAULT NULL COMMENT '备用1',
  `back2` varchar(50) DEFAULT NULL COMMENT '备用2',
  `back3` varchar(50) DEFAULT NULL COMMENT '备用3',
  `back4` varchar(50) DEFAULT NULL COMMENT '备用4',
  `back5` varchar(50) DEFAULT NULL COMMENT '备用5',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户基本表';
BEGIN;
LOCK TABLES `env`.`rec_company` WRITE;
DELETE FROM `env`.`rec_company`;
INSERT INTO `env`.`rec_company` (`id`,`create_by`,`create_time`,`update_by`,`update_time`,`del`,`status`,`user_id`,`user_name`,`com_name`,`com_photo_url`,`com_adr`,`com_long`,`com_lat`,`com_short_name`,`industry`,`com_num_people`,`com_people`,`com_reg_capital`,`com_reg_date`,`com_desc`,`remark`,`back1`,`back2`,`back3`,`back4`,`back5`) VALUES (1, 1, '2020-12-04 00:15:41', 1, '2020-12-04 00:15:41', 0, 0, 2, NULL, '企业名称', '大师傅', '青羊区', NULL, NULL, '企业', '教育', 2, '苗苗', 100, '2020-12-02', '打法是否', NULL, 'admin', NULL, NULL, NULL, NULL),(2, NULL, NULL, NULL, NULL, 0, 0, 3, NULL, '我是企业', NULL, '地址', NULL, NULL, '好好', '钢铁', 4, '不知道', 1000, '2020-12-10', '打法是否', NULL, NULL, NULL, NULL, NULL, NULL),(3, 1, '2020-12-04 00:37:23', 1, '2020-12-04 00:37:23', 0, 0, 1, '管理员', '我的企业', '', '企业地址', NULL, NULL, '企业', '2', 1, '2', 2, '1899-12-31', '2', NULL, NULL, NULL, NULL, NULL, NULL),(4, 1, '2020-12-06 20:25:05', 1, '2020-12-06 20:25:05', 0, 0, NULL, NULL, '11', NULL, '11', NULL, NULL, NULL, NULL, 11, '11', NULL, NULL, '11', NULL, NULL, NULL, NULL, NULL, NULL),(5, 1, '2020-12-06 21:34:58', 1, '2020-12-06 21:34:58', 0, 0, 1, NULL, '企业信息1', NULL, '企业信息', NULL, NULL, NULL, NULL, 2, '企业信息', 122, NULL, '企业信息', NULL, NULL, NULL, NULL, NULL, NULL);
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `env`.`rec_cv` WRITE;
DELETE FROM `env`.`rec_cv`;
INSERT INTO `env`.`rec_cv` (`id`,`create_by`,`create_time`,`update_by`,`update_time`,`del`,`status`,`user_id`,`user_name`,`cv_type`,`cv_no`,`name`,`photo_url`,`sex`,`birthday`,`work_status`,`school`,`qualifications`,`speciality`,`graduate_date`,`req_positon`,`req_city`,`req_industry`,`req_salary`,`self_desc`,`pro_desc`,`cv_desc`,`cv_url`,`remark`,`back1`,`back2`,`back3`,`back4`,`back5`) VALUES (1, 1, '2020-12-09 23:04:53', 1, '2020-12-09 23:24:22', 0, 2, 1, '1', 1, '1', '1', '1', 0, '2020-12-09', 6, '兰州理工', 5, '1', '2020-12-09', '经理', '兰州', '环保', 100000, '挺好', '业及擅长描述', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL),(2, 1, NULL, 1, NULL, NULL, 2, 1, '苗苗', 1, NULL, '姓名', '头像', 0, NULL, 3, '兰州大学', 3, '宝石', NULL, '经理', '成都', '环保', 9000, NULL, '专业', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `env`.`rec_deliver` WRITE;
DELETE FROM `env`.`rec_deliver`;
INSERT INTO `env`.`rec_deliver` (`id`,`create_by`,`create_time`,`update_by`,`update_time`,`del`,`status`,`user_id`,`user_name`,`com_user_id`,`com_user_name`,`com_id`,`com_name`,`cv_id`,`cv_name`,`remark`,`back1`,`back2`,`back3`,`back4`,`back5`) VALUES (1, 1, '2020-12-09 23:25:56', 1, NULL, NULL, 0, 1, '1', 1, NULL, 3, '管理员', NULL, NULL, '管理员收藏的简历', NULL, NULL, NULL, NULL, NULL),(2, 1, '2020-12-09 23:26:49', 1, NULL, NULL, 0, 1, '1', 1, NULL, 3, '管理员', NULL, NULL, '1投递的简历', NULL, NULL, NULL, NULL, NULL),(3, 1, '2020-12-09 23:48:43', 1, NULL, NULL, 0, 1, '1', 3, NULL, 2, NULL, NULL, NULL, 'null收藏的简历', NULL, NULL, NULL, NULL, NULL),(4, 1, '2020-12-09 23:49:26', 1, NULL, NULL, 0, 1, '1', 2, NULL, 1, NULL, NULL, NULL, '1投递的简历', NULL, NULL, NULL, NULL, NULL),(5, 1, '2020-12-09 23:49:43', 1, NULL, NULL, 0, 1, '1', 3, NULL, 2, NULL, NULL, NULL, 'null收藏的简历', NULL, NULL, NULL, NULL, NULL),(6, 1, '2020-12-09 23:50:56', 1, NULL, NULL, 0, 1, '1', 1, NULL, 3, '管理员', NULL, NULL, '管理员收藏的简历', NULL, NULL, NULL, NULL, NULL);
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `env`.`rec_log` WRITE;
DELETE FROM `env`.`rec_log`;
INSERT INTO `env`.`rec_log` (`id`,`create_by`,`create_time`,`update_by`,`update_time`,`del`,`status`,`log_obj_id`,`log_type`,`log_user_id`,`log_user_name`,`log_abstract`,`obj1`,`obj2`,`obj3`,`obj4`,`obj5`,`remark`,`text`) VALUES (1, 1, '2020-12-06 18:48:26', 1, '2020-12-06 18:48:30', 0, 0, 1, 3, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),(2, 1, '2020-12-06 20:56:30', 1, '2020-12-06 20:56:30', 0, 0, 1, 4, NULL, NULL, '收藏了null', NULL, NULL, NULL, NULL, NULL, NULL, NULL),(3, 1, '2020-12-06 21:27:04', 1, '2020-12-06 21:27:04', 0, 0, 1, 4, NULL, NULL, '收藏了企业名称', NULL, NULL, NULL, NULL, NULL, NULL, NULL),(4, 1, '2020-12-06 21:27:40', 1, '2020-12-06 21:27:40', 0, 0, 1, 4, NULL, NULL, '收藏了企业名称', NULL, NULL, NULL, NULL, NULL, NULL, NULL),(5, 1, '2020-12-06 21:31:17', 1, '2020-12-06 21:31:17', 0, 0, 1, 4, 1, '苗苗', '苗苗收藏了企业名称', NULL, NULL, NULL, NULL, NULL, NULL, NULL),(6, 1, '2020-12-06 21:31:52', 1, '2020-12-06 21:31:52', 0, 0, 1, 4, NULL, NULL, '收藏了企业名称', NULL, NULL, NULL, NULL, NULL, NULL, NULL),(7, 1, '2020-12-06 21:32:14', 1, '2020-12-06 21:32:14', 0, 0, 1, 4, 1, '苗苗', '苗苗收藏了企业名称', NULL, NULL, NULL, NULL, NULL, NULL, NULL),(8, 1, '2020-12-06 21:35:57', 1, '2020-12-06 21:35:57', 0, 0, 1, 4, NULL, NULL, '收藏了企业名称', NULL, NULL, NULL, NULL, NULL, NULL, NULL),(9, 1, '2020-12-06 21:37:26', 1, '2020-12-06 21:37:26', 0, 0, 1, 4, 1, '苗苗', '苗苗收藏了企业名称', NULL, NULL, NULL, NULL, NULL, NULL, NULL),(10, 1, '2020-12-09 23:25:56', 1, '2020-12-09 23:25:56', 0, 0, 1, 2, 1, 'admin', '苗苗我的企业收藏了1的简历', '1', '苗苗', NULL, NULL, NULL, NULL, NULL),(11, 1, '2020-12-09 23:26:49', 1, '2020-12-09 23:26:49', 0, 0, 1, 3, 1, 'admin', '苗苗投递了我的企业', '1', '苗苗', NULL, NULL, NULL, NULL, NULL),(12, 1, '2020-12-09 23:48:43', 1, '2020-12-09 23:48:43', 0, 0, 1, 2, 1, 'admin', '苗苗我是企业收藏了1的简历', '1', '苗苗', NULL, NULL, NULL, NULL, NULL),(13, 1, '2020-12-09 23:49:26', 1, '2020-12-09 23:49:26', 0, 0, 1, 3, 1, 'admin', '苗苗投递了企业名称', '1', '苗苗', NULL, NULL, NULL, NULL, NULL),(14, 1, '2020-12-09 23:49:43', 1, '2020-12-09 23:49:43', 0, 0, 1, 2, 1, 'admin', '苗苗我是企业收藏了1的简历', '1', '苗苗', NULL, NULL, NULL, NULL, NULL),(15, 1, '2020-12-09 23:50:56', 1, '2020-12-09 23:50:56', 0, 0, 1, 2, 1, 'admin', '苗苗我的企业收藏了1的简历', '1', '苗苗', NULL, NULL, NULL, NULL, NULL);
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `env`.`rec_position` WRITE;
DELETE FROM `env`.`rec_position`;
INSERT INTO `env`.`rec_position` (`id`,`create_by`,`create_time`,`update_by`,`update_time`,`del`,`status`,`com_id`,`com_name`,`recruit_type`,`position_name`,`work_city`,`work_adr`,`exp_req`,`edu_req`,`salary_req`,`salary_begin`,`salary_end`,`post_name`,`post_key`,`post_desc`,`remark`,`back1`,`back2`,`back3`,`back4`,`back5`) VALUES (1, 1, '2020-12-08 00:26:25', 1, '2020-12-08 01:16:31', 0, 3, 1, '测试企业', 2, '职位名称111', '成都', '高新区', 1, 1, 4, NULL, NULL, '岗位名称', '岗位关键词', '岗位描述', NULL, NULL, NULL, NULL, NULL, NULL),(2, 1, '2020-12-08 01:01:18', 1, '2020-12-08 01:17:17', 0, 0, 3, '我的企业', 2, '职位名称111', '兰州', '七里河', 1, 1, 4, 1, 1, '岗位名称', '岗位关键词', '岗位描述', NULL, NULL, NULL, NULL, NULL, NULL),(3, 1, '2020-12-09 23:37:42', 1, '2020-12-09 23:37:42', 0, 0, 3, '我的企业', 2, '董事长', NULL, NULL, 3, 4, 5000, NULL, NULL, '岗位', '老大', '总裁', NULL, NULL, NULL, NULL, NULL, NULL);
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `env`.`rec_user` WRITE;
DELETE FROM `env`.`rec_user`;
INSERT INTO `env`.`rec_user` (`id`,`create_by`,`create_time`,`update_by`,`update_time`,`del`,`status`,`user_id`,`user_type`,`user_name`,`sex`,`position_name`,`industry`,`com_id`,`com_name`,`email`,`user_identity`,`birthday`,`first_work_date`,`work_status`,`remark`,`back1`,`back2`,`back3`,`back4`,`back5`) VALUES (1, 1, '2020-12-06 20:54:42', 1, '2020-12-06 20:54:46', 0, 0, 1, 0, '苗苗', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
UNLOCK TABLES;
COMMIT;
