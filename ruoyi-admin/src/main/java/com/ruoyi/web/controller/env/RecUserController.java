package com.ruoyi.web.controller.env;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginBody;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.service.SysLoginService;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.manage.constant.EnvConstants;
import com.ruoyi.manage.domain.RecCv;
import com.ruoyi.manage.domain.RecDeliver;
import com.ruoyi.manage.domain.RecUser;
import com.ruoyi.manage.service.IRecCvService;
import com.ruoyi.manage.service.IRecDeliverService;
import com.ruoyi.manage.service.IRecUserService;
import com.ruoyi.system.service.ISysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.Set;

/**
 * 用户基本Controller
 *
 * @author ruoyi
 * @date 2020-12-01
 */

@Api(value="用户信息Controller",tags={"用户注册登陆接口"})
@RestController
@RequestMapping("/env/user")
public class RecUserController extends BaseController
{
    private String prefix = "env/user";

    @Autowired
    private IRecUserService recUserService;
    @Autowired
    private ISysUserService userService;
    @Autowired
    private SysLoginService loginService;
    @Autowired
    private TokenService tokenService;
    @Autowired
    private IRecDeliverService recDeliverService;
    @Autowired
    private IRecCvService recCvService;

    @PreAuthorize("@ss.hasPermi('env:user:view')")
    @GetMapping()
    public String user()
    {
        return prefix + "/user";
    }

    /**
     * 查询用户基本列表
     */
    @PreAuthorize("@ss.hasPermi('env:user:list')")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(RecUser recUser)
    {
        startPage();
        List<RecUser> list = recUserService.selectRecUserList(recUser);
        return getDataTable(list);
    }

    /**
     * 根据系统用户userId，查询用户联系方式
     */
    @ApiOperation("用户-联系方式查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ComUserId", value = "企业用户ID", required =true,paramType="query"),
            @ApiImplicitParam(name = "cvId", value = "简历ID", required =true,paramType="query")
    })
    @PostMapping("/getUserPhone")
    @ResponseBody
    public AjaxResult getUserPhone(@ApiIgnore RecDeliver recDeliver)
    {
        RecCv recCv = recCvService.selectRecCvById(recDeliver.getCvId());
        if (null == recCv){
            return AjaxResult.error("用户简历获取失败，请刷新后重试");
        }
        RecUser recUser = recUserService.selectRecUserById(recCv.getUserId());
        if (null == recUser){
            return AjaxResult.error("用户信息异常，无法获取到联系方式");
        }
        SysUser sysUser = userService.selectUserById(recUser.getUserId());
        if (null == sysUser){
            return AjaxResult.error("用户信息异常，无法获取到联系方式");
        }
        String phone = sysUser.getPhonenumber();
        // 自动将建立放入待沟通
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        return recDeliverService.recDeliverOpp(recDeliver, EnvConstants.CONNECT, loginUser,phone);
//        return AjaxResult.success("获取联系方式成功", phone);
    }

    /**
     * 导出用户基本列表
     */
    @PreAuthorize("@ss.hasPermi('env:user:export')")
    @Log(title = "用户基本", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(RecUser recUser)
    {
        List<RecUser> list = recUserService.selectRecUserList(recUser);
        ExcelUtil<RecUser> util = new ExcelUtil<RecUser>(RecUser.class);
        return util.exportExcel(list, "user");
    }

    /**
     * 新增用户基本
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存用户基本
     */
    @PreAuthorize("@ss.hasPermi('env:user:add')")
    @Log(title = "用户基本", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(RecUser recUser)
    {
        return toAjax(recUserService.insertRecUser(recUser));
    }

    /**
     * 平台用户注册
     */
    @ApiOperation("用户-用户注册")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phonenumber", value = "手机号", required =true,paramType="query"),
            @ApiImplicitParam(name = "password", value = "密码", required =true,paramType="query"),
            @ApiImplicitParam(name = "userType", value = "用户类型0：招聘企事业；1：求职者；", required =true,paramType="query")
    })
    @Log(title = "用户注册", businessType = BusinessType.INSERT)
    @PostMapping("/register")
    @ResponseBody
    public AjaxResult register(@RequestParam("phonenumber") String phonenumber,@RequestParam("password") String password,
                               @RequestParam("userType") Integer userType)
    {

        return toAjax(recUserService.register(phonenumber,password,userType));
    }

    /**
     * 用户登陆
     *
     * @param loginBody 登录信息
     * @return 结果
     */
    @ApiOperation("用户-登陆接口-以手机号（账号）+ 密码 + 验证码的方式进行登陆验证")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", value = "用户名", required =true,paramType="query"),
            @ApiImplicitParam(name = "password", value = "密码", required =true,paramType="query"),
            @ApiImplicitParam(name = "code", value = "验证码", required =true,paramType="query"),
            @ApiImplicitParam(name = "uuid", value = "uuid", required =false,paramType="query")
    })
    @PostMapping("/login")
    public AjaxResult login(@RequestBody LoginBody loginBody)
    {
        AjaxResult ajax = AjaxResult.success();
        // 生成令牌
        String token = loginService.login(loginBody.getUsername(), loginBody.getPassword(), loginBody.getCode(),
                loginBody.getUuid());
        ajax.put(Constants.TOKEN, token);
        return ajax;
    }

    /**
     * 用户登陆，免验证码
     *
     * @param username 用户账号
     * @param password 密码
     * @return 结果
     */
    @ApiOperation("用户-登陆接口-以手机号（账号）+ 密码（免验证码）的方式进行登陆验证")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", value = "用户名", required =true,paramType="query"),
            @ApiImplicitParam(name = "password", value = "密码", required =true,paramType="query")
    })
    @PostMapping("/loginNoKey")
    public AjaxResult loginNoKey(@RequestParam("username") String username,@RequestParam("password") String password)
    {
        AjaxResult ajax = AjaxResult.success();
        // 生成令牌
        String token = loginService.loginNoKey(username, password);
        ajax.put(Constants.TOKEN, token);
        return ajax;
    }


    /**
     * 平台用户设置
     */
    @ApiOperation("用户-个人设置-根据系统用户userId修改用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "系统用户ID", required =true,paramType="query"),
            @ApiImplicitParam(name = "userType", value = "用户类型0：招聘企事业；1：求职者；", required =false,paramType="query"),
            @ApiImplicitParam(name = "userName", value = "用户名称 类型为0时，企业名称，为1时，姓名。简单注册时可默认为电话号码", required =false,paramType="query"),
            @ApiImplicitParam(name = "sex", value = "性别 类型为1or2时用，0：女；1：男", required =false,paramType="query"),
            @ApiImplicitParam(name = "positionName", value = "职位 类型为0时用", required =false,paramType="query"),
            @ApiImplicitParam(name = "industry", value = "从事行业 类型为2时用", required =false,paramType="query"),
            @ApiImplicitParam(name = "email", value = "邮箱", required =false,paramType="query"),
            @ApiImplicitParam(name = "userIdentity", value = "求职身份 类型为1时用，0：在校生-在校/应届；1：无经验-往届；2：职场人-正式工作经历", required =false,paramType="query"),
            @ApiImplicitParam(name = "birthday", value = "出生日期 类型为1时用", required =false,paramType="query"),
            @ApiImplicitParam(name = "firstWorkDate", value = "参加工作年月 类型为1时用", required =false,paramType="query"),
            @ApiImplicitParam(name = "workStatus", value = "当前求职状态 类型为1时用，0：在校-实习；1：在校-找工作；2：在职-考虑机会；3：在职-月内到岗；5：在职-暂不考虑；6：离职-立即到岗", required =false,paramType="query")

    })
    @Log(title = "用户设置", businessType = BusinessType.UPDATE)
    @PostMapping("/updateUserByUserId")
    @ResponseBody
    public AjaxResult updateUserByUserId(@ApiIgnore @RequestBody RecUser recUser)
    {
        RecUser recUser1 = recUserService.selectRecUserByUserId(recUser.getUserId());
        recUser.setId(recUser1.getId());
        return toAjax(recUserService.updateRecUser(recUser));
    }

    /**
     * 平台用户设置
     */
    @ApiOperation("用户-个人设置-根据注册用户id修改用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "注册用户ID", required =true,paramType="query"),
            @ApiImplicitParam(name = "userType", value = "用户类型0：招聘企事业；1：求职者；", required =false,paramType="query"),
            @ApiImplicitParam(name = "userName", value = "用户名称 类型为0时，企业名称，为1时，姓名。简单注册时可默认为电话号码", required =false,paramType="query"),
            @ApiImplicitParam(name = "sex", value = "性别 类型为1or2时用，0：女；1：男", required =false,paramType="query"),
            @ApiImplicitParam(name = "positionName", value = "职位 类型为0时用", required =false,paramType="query"),
            @ApiImplicitParam(name = "industry", value = "从事行业 类型为2时用", required =false,paramType="query"),
            @ApiImplicitParam(name = "email", value = "邮箱", required =false,paramType="query"),
            @ApiImplicitParam(name = "userIdentity", value = "求职身份 类型为1时用，0：在校生-在校/应届；1：无经验-往届；2：职场人-正式工作经历", required =false,paramType="query"),
            @ApiImplicitParam(name = "birthday", value = "出生日期 类型为1时用", required =false,paramType="query"),
            @ApiImplicitParam(name = "firstWorkDate", value = "参加工作年月 类型为1时用", required =false,paramType="query"),
            @ApiImplicitParam(name = "workStatus", value = "当前求职状态 类型为1时用，0：在校-实习；1：在校-找工作；2：在职-考虑机会；3：在职-月内到岗；5：在职-暂不考虑；6：离职-立即到岗", required =false,paramType="query")

    })
    @Log(title = "用户设置", businessType = BusinessType.UPDATE)
    @PostMapping("/updateUserById")
    @ResponseBody
    public AjaxResult updateUserById(@ApiIgnore @RequestBody RecUser recUser)
    {
        return toAjax(recUserService.updateRecUser(recUser));
    }

    /**
     * 平台用户密码重置
     */
    @ApiOperation("用户-密码重置-根据系统用户userId重置密码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "系统用户ID", required =true,paramType="query"),
            @ApiImplicitParam(name = "password", value = "密码", required =true,paramType="query")
    })
    @Log(title = "密码重置", businessType = BusinessType.UPDATE)
    @PostMapping("/resetPwd")
    @ResponseBody
    public AjaxResult resetPwd(@RequestParam("userId") Long userId,@RequestParam("password") String password)
    {
        SysUser sysUser = new SysUser();
        sysUser.setUserId(userId);
        sysUser.setPassword(SecurityUtils.encryptPassword(password));
        sysUser.setUpdateBy(userId.toString());
        sysUser.setUpdateTime(DateUtils.getNowDate());

        return toAjax(userService.resetPwd(sysUser));
    }

    /**
     * 修改用户基本
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        RecUser recUser = recUserService.selectRecUserById(id);
        mmap.put("recUser", recUser);
        return prefix + "/edit";
    }

    /**
     * 修改保存用户基本
     */
    @PreAuthorize("@ss.hasPermi('env:user:edit')")
    @Log(title = "用户基本", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(RecUser recUser)
    {
        return toAjax(recUserService.updateRecUser(recUser));
    }

    /**
     * 删除用户基本
     */
    @PreAuthorize("@ss.hasPermi('env:user:remove')")
    @Log(title = "用户基本", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(recUserService.deleteRecUserByIds(ids));
    }
}
