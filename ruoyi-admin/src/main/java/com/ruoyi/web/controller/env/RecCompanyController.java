package com.ruoyi.web.controller.env;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.manage.constant.EnvConstants;
import com.ruoyi.manage.domain.RecCompany;
import com.ruoyi.manage.domain.RecLog;
import com.ruoyi.manage.domain.RecUser;
import com.ruoyi.manage.service.IRecCompanyService;
import com.ruoyi.manage.service.IRecLogService;
import com.ruoyi.manage.service.IRecUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * 企业信息Controller
 * 企业Controller控制器，主要实现就创平台的所有企业相关的接口实现
 * @author fanpq
 * @date 2020-11-28
 */
@Api(value="企业信息Controller",tags={"企业信息管理接口"})
@RestController
@RequestMapping("/env/company")
public class RecCompanyController extends BaseController
{
    private String prefix = "env/company";

    @Autowired
    private IRecCompanyService recCompanyService;
    @Autowired
    private IRecLogService recLogService;
    @Autowired
    private IRecUserService recUserService;

    @Autowired
    private TokenService tokenService;

    @PreAuthorize("@ss.hasPermi('env:company:view')")
    @GetMapping()
    public String company()
    {
        return prefix + "/company";
    }

    /**
     * 查询企业信息列表
     */
    @PreAuthorize("@ss.hasPermi('env:company:list')")
    @GetMapping("/list")
    @ResponseBody
    public TableDataInfo list(@ApiIgnore RecCompany recCompany)
    {
        startPage();
        List<RecCompany> list = recCompanyService.selectRecCompanyList(recCompany);
        return getDataTable(list);
    }

    /**
     * 企业检索
     */
    @ApiOperation("企业-企业检索-根据检索套件检索招聘企业信息List")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "comName", value = "企业名称", required =false,paramType="query"),
            @ApiImplicitParam(name = "comAdr", value = "企业地址", required =false,paramType="query"),
            @ApiImplicitParam(name = "comShortName", value = "企业简称", required =false,paramType="query"),
            @ApiImplicitParam(name = "industry", value = "所属行业", required =false,paramType="query"),
            @ApiImplicitParam(name = "comNumPeople", value = "公司人数", required =false,paramType="query"),
            @ApiImplicitParam(name = "comPeople", value = "法人代表", required =false,paramType="query"),
            @ApiImplicitParam(name = "comRegCapital", value = "注册资本(万元)", required =false,paramType="query"),
            @ApiImplicitParam(name = "comDesc", value = "公司简介", required =false,paramType="query"),
            @ApiImplicitParam(name = "comRegDate", value = "成立时间", required =false,paramType="query")
    })
    @PostMapping("/getList")
    @ResponseBody
    public TableDataInfo getList(@ApiIgnore @RequestBody RecCompany recCompany)
    {
        startPage(recCompany.getParams());
        List<RecCompany> list = recCompanyService.selectRecCompanyList(recCompany);
        return getDataTable(list);
    }


    /**
     * 根据企业用户关联的企业，查询企业信息详情
     */
    @ApiOperation("企业信息详情-按企业用户ID")
    @ApiImplicitParam(name = "userId", value = "系统用户ID", required =true,dataType="Long",paramType="path")
    @GetMapping("/getByUserId/{userId}")
    @ResponseBody
    public AjaxResult getByUserId(@PathVariable("userId") Long userId)
    {
        return AjaxResult.success(recCompanyService.selectRecCompanyByUserId(userId));
    }

    /**
     * 按当前userId查询收藏的企业列表
     */
    @ApiOperation("企业-我的收藏-根据当前用户userId查询收藏的企业列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "系统用户ID", required =true,paramType="query"),
            @ApiImplicitParam(name = "pageNum", value = "当前页码", required =false,paramType="query"),
            @ApiImplicitParam(name = "pageSize", value = "分页数量", required =false,paramType="query")
    })
    @GetMapping("/getCollectComListByUserId")
    @ResponseBody
    public TableDataInfo getCollectComListByUserId(@RequestParam("userId") Long userId,@RequestParam("pageNum") Integer pageNum,
                                                   @RequestParam("pageSize") Integer pageSize)
    {
        RecLog recLog = new RecLog();
        recLog.setLogUserId(userId);
        recLog.setLogType(EnvConstants.COLLECT);
        List<RecLog> lists = recLogService.selectRecLogList(recLog);
        if (lists.size() <= 0) {
            return getNoDataTable("未查到该用户关联企业，请确认后重试");
        }else {
            StringBuilder stringBuffer = new StringBuilder();
            for (RecLog list : lists) {
                stringBuffer.append(list.getLogObjId());
                stringBuffer.append(",");
            }
            String ids = stringBuffer.delete(stringBuffer.lastIndexOf(","),stringBuffer.length()).toString();

            startPage();
            return getDataTable(recCompanyService.selectRecCompanyListByIds(ids));
        }
    }

    /**
     * 按当前userId查询投递的企业列表
     */
    @ApiOperation("企业-我的投递-根据当前用户userId查询投递的企业列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "系统用户ID", required =true,paramType="query"),
            @ApiImplicitParam(name = "pageNum", value = "当前页码", required =false,paramType="query"),
            @ApiImplicitParam(name = "pageSize", value = "分页数量", required =false,paramType="query")
    })
    @GetMapping("/getDeliverComListByUserId")
    @ResponseBody
    public TableDataInfo getDeliverComListByUserId(@RequestParam("userId") Long userId,@RequestParam("pageNum") Integer pageNum,@RequestParam("pageSize") Integer pageSize)
    {
        RecLog recLog = new RecLog();
        recLog.setLogUserId(userId);
        recLog.setLogType(EnvConstants.DELIVER);
        List<RecLog> lists = recLogService.selectRecLogList(recLog);
        if (lists.size() <= 0) {
            return getNoDataTable("未查询到投递记录，赶紧去投递你心仪的企业");
        }else {
            StringBuilder stringBuffer = new StringBuilder();
            for (RecLog list : lists) {
                stringBuffer.append(list.getLogObjId());
                stringBuffer.append(",");
            }
            String ids = stringBuffer.delete(stringBuffer.lastIndexOf(","), stringBuffer.length()).toString();

            startPage();
            return getDataTable(recCompanyService.selectRecCompanyListByIds(ids));
        }
    }

    /**
     * 根据企业ID和userId，查询企业信息详情
     * 企业id必传，如果userId为空，则仅返回企业详情。
     * 如果userId不为空，则返回企业详情和该用户对该企业的状态（无、收藏、投递）
     */
    @ApiOperation("企业信息详情-按企业ID和userId(系统用户ID)，如果userId不为空，则同时返回企业详情和该用户对该企业的状态（无、收藏、投递）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "企业ID", required =true,paramType="query"),
            @ApiImplicitParam(name = "userId", value = "用户ID", required =false,paramType="query")
    })
    @PostMapping("/getByIdOrUserId")
    @ResponseBody
    public AjaxResult getByIdOrUserId(@ApiIgnore RecCompany recCompany)
    {
        RecCompany recCompanyResult = new RecCompany();
        recCompanyResult =  recCompanyService.selectRecCompanyById(recCompany.getId());
        if (null != recCompany.getUserId()) {
            // 需要统计传入userId对该公司的收藏和投递
            RecLog recLog = new RecLog();
            recLog.setLogObjId(recCompany.getId());
            recLog.setLogUserId(recCompany.getUserId());
            // 简历投递
            recLog.setLogType(EnvConstants.DELIVER);
            recCompanyResult.setDeliverFlag(recLogService.selectRecLogCount(recLog));
        }
        return AjaxResult.success(recCompanyResult);
    }

    /**
     * 根据userId，对该用户的收藏数量和投递数量进行统计
     */
    @ApiOperation("企业-投递和收藏统计，根据用户userId查询已收藏和已投递的数量统计")
    @ApiImplicitParam(name = "userId", value = "系统用户ID", required =true,dataType="Long",paramType="path")
    @GetMapping("/getNumByUserId/{userId}")
    @ResponseBody
    public AjaxResult getNumByUserId(@PathVariable("userId") Long userId)
    {
        //0:浏览;1、查看;2、简历收藏;3、简历投递;4、公司收藏
        // 需要统计传入userId对该公司的收藏和投递
        RecCompany recCompanyResult = new RecCompany();
        RecLog recLog = new RecLog();
        recLog.setLogUserId(userId);
        // 简历投递
        recLog.setLogType(EnvConstants.DELIVER);
        recCompanyResult.setDeliverFlag(recLogService.selectRecLogCount(recLog));
        return AjaxResult.success(recCompanyResult);
    }

    /**
     * 根据企业ID和userId，记录企业收藏操作日志
     */
    @ApiOperation("企业-企业收藏，点击感兴趣后，将企业信息进行收藏")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "企业ID", required =true,paramType="query"),
            @ApiImplicitParam(name = "userId", value = "系统用户ID", required =true,paramType="query")
    })
    @PostMapping("/comCollect")
    @ResponseBody
    public AjaxResult comCollect(@ApiIgnore RecCompany recCompany)
    {
        RecCompany recCompanyResult =  recCompanyService.selectRecCompanyById(recCompany.getId());
        // 记录求职者对收藏企业日志记录
        //0:浏览;1、查看;2、简历收藏;3、简历投递;4、公司收藏
        RecLog recLog = new RecLog();
        RecUser recUser =  recUserService.selectRecUserByUserId(recCompany.getUserId());
        if (null == recUser){
            return AjaxResult.error("用户信息异常，请确认");
        }
        recLog.setCreateBy(recUser.getUserId().toString());
        recLog.setUpdateBy(recUser.getUserId().toString());
        recLog.setLogType(EnvConstants.COLLECT);
        recLog.setLogObjId(recCompany.getId());
        recLog.setLogUserId(recUser.getUserId());
        recLog.setLogUserName(recUser.getUserName());
        if (null != recCompanyResult) {
            recLog.setLogAbstract("收藏了" + recCompanyResult.getComName());
        }else{
            recLog.setLogAbstract(recUser.getUserName()+"点击了收藏了");
        }
        return toAjax(recLogService.insertRecLog(recLog));
    }

    /**
     * 导出企业信息列表
     */
    @PreAuthorize("@ss.hasPermi('env:company:export')")
    @Log(title = "企业信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(RecCompany recCompany)
    {
        List<RecCompany> list = recCompanyService.selectRecCompanyList(recCompany);
        ExcelUtil<RecCompany> util = new ExcelUtil<RecCompany>(RecCompany.class);
        return util.exportExcel(list, "company");
    }

    /**
     * 新增企业信息
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存企业信息
     */
    @PreAuthorize("@ss.hasPermi('env:company:add')")
    @Log(title = "企业信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@ApiIgnore RecCompany recCompany)
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        if (null == loginUser){
            return AjaxResult.error("用户未登陆或登陆已失效");
        }else {
            recCompany.setCreateBy(loginUser.getUser().getUserId().toString());
            recCompany.setUpdateBy(loginUser.getUser().getUserId().toString());
        }
        return toAjax(recCompanyService.insertRecCompany(recCompany));
    }

    /**
     * 新增保存企业信息
     */
    @ApiOperation("新增保存企业信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "comName", value = "企业名称", required =true,paramType="query"),
            @ApiImplicitParam(name = "comAdr", value = "企业地址", required =true,paramType="query"),
            @ApiImplicitParam(name = "comShortName", value = "企业简称", required =false,paramType="query"),
            @ApiImplicitParam(name = "industry", value = "所属行业", required =false,paramType="query"),
            @ApiImplicitParam(name = "comNumPeople", value = "公司人数", required =true,paramType="query"),
            @ApiImplicitParam(name = "comPeople", value = "法人代表", required =false,paramType="query"),
            @ApiImplicitParam(name = "comRegCapital", value = "注册资本(万元)", required =false,paramType="query"),
            @ApiImplicitParam(name = "comDesc", value = "公司简介", required =false,paramType="query"),
            @ApiImplicitParam(name = "comRegDate", value = "成立时间", required =false,paramType="query")

    })
    @Log(title = "企业信息", businessType = BusinessType.INSERT)
    @PostMapping("/addCom")
    @ResponseBody
    public AjaxResult addCom(@ApiIgnore @RequestBody RecCompany recCompany)
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        if (null == loginUser){
            return AjaxResult.error("用户未登陆或登陆已失效");
        }else {
            recCompany.setCreateBy(loginUser.getUser().getUserId().toString());
            recCompany.setUpdateBy(loginUser.getUser().getUserId().toString());
        }

        return toAjax(recCompanyService.insertRecCompany(recCompany));
    }

    /**
     * 修改企业信息
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        RecCompany recCompany = recCompanyService.selectRecCompanyById(id);
        mmap.put("recCompany", recCompany);
        return prefix + "/edit";
    }

    /**
     * 修改保存企业信息
     */
    @PreAuthorize("@ss.hasPermi('env:company:edit')")
    @Log(title = "企业信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(RecCompany recCompany)
    {
        return toAjax(recCompanyService.updateRecCompany(recCompany));
    }

    /**
     * 维护保存企业信息
     */

    @ApiOperation("维护企业信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "企业ID", required =true,paramType="query"),
            @ApiImplicitParam(name = "userId", value = "系统用户ID", required =true,paramType="query"),
            @ApiImplicitParam(name = "comName", value = "企业名称", required =true,paramType="query"),
            @ApiImplicitParam(name = "comAdr", value = "企业地址", required =true,paramType="query"),
            @ApiImplicitParam(name = "comShortName", value = "企业简称", required =false,paramType="query"),
            @ApiImplicitParam(name = "industry", value = "所属行业", required =false,paramType="query"),
            @ApiImplicitParam(name = "comNumPeople", value = "公司人数", required =true,paramType="query"),
            @ApiImplicitParam(name = "comPeople", value = "法人代表", required =false,paramType="query"),
            @ApiImplicitParam(name = "comRegCapital", value = "注册资本(万元)", required =false,paramType="query"),
            @ApiImplicitParam(name = "comDesc", value = "公司简介", required =false,paramType="query"),
            @ApiImplicitParam(name = "comRegDate", value = "成立时间", required =false,paramType="query")

    })
    @Log(title = "企业关联用户维护保存自己企业信息", businessType = BusinessType.UPDATE)
    @PostMapping("/update")
    @ResponseBody
    public AjaxResult update(@ApiIgnore @RequestBody RecCompany recCompany)
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        if (null == loginUser){
            return AjaxResult.error("用户未登陆或登陆已失效");
        }
        recCompany.setUpdateBy(loginUser.getUser().getUserId().toString());
        if (!recCompany.getUserId().toString().equals(recCompany.getUpdateBy())) {
            return AjaxResult.error("你无权修改别人的公司");
        }
        // 不允许修改企业对应的企业用户（来源招聘用户表）
        recCompany.setUserId(null);
        recCompany.setUserName(null);
        return toAjax(recCompanyService.updateRecCompany(recCompany));
    }

    /**
     * 删除企业信息
     */
    @PreAuthorize("@ss.hasPermi('env:company:remove')")
    @Log(title = "企业信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(recCompanyService.deleteRecCompanyByIds(ids));
    }


    /**
     * 审核公司信息
     */
    @PreAuthorize("@ss.hasPermi('env:company:edit')")
    @Log(title = "公司信息", businessType = BusinessType.UPDATE)
    @PostMapping( "/confirmRecCompanys")
    @ResponseBody
    public AjaxResult confirmRecCompanys(String ids)
    {
        return toAjax(recCompanyService.confirmRecCompanys(ids));
    }

    /**
     * 取消审核公司信息
     */
    @PreAuthorize("@ss.hasPermi('env:company:edit')")
    @Log(title = "公司信息", businessType = BusinessType.UPDATE)
    @PostMapping( "/confirmCancelRecCompanys")
    @ResponseBody
    public AjaxResult confirmCancelRecCompanys(String ids)
    {
        List<RecCompany> recCompanyList = recCompanyService.selectRecCompanyListByIds(ids);
        for (RecCompany recCompany:recCompanyList){
            if (!EnvConstants.COMCONFIRM.equals(recCompany.getStatus())){
                return AjaxResult.error(recCompany.getComName() + "的状态不是有效状态，取消失败");
            }
        }
        return toAjax(recCompanyService.confirmCancelRecCompanys(ids));
    }
}
