package com.ruoyi.web.controller.system;

import java.util.List;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.system.domain.CmsCategory;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.service.ICmsCategoryService;
import com.ruoyi.common.utils.poi.ExcelUtil;

/**
 * 栏目分类Controller
 * 
 * @author fanpq
 * @date 2020-12-20
 */
@Api(value="栏目分类接口",tags={"栏目分类管理"})
@RestController
@RequestMapping("/system/category")
public class CmsCategoryController extends BaseController
{
    @Autowired
    private ICmsCategoryService cmsCategoryService;

    /**
     * 查询栏目分类列表
     */
    @PreAuthorize("@ss.hasPermi('system:category:list')")
    @GetMapping("/list")
    public AjaxResult list(CmsCategory cmsCategory)
    {
        List<CmsCategory> list = cmsCategoryService.selectCmsCategoryList(cmsCategory);
        return AjaxResult.success(list);
    }

    /**
     * 导出栏目分类列表
     */
    @PreAuthorize("@ss.hasPermi('system:category:export')")
    @Log(title = "栏目分类", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(CmsCategory cmsCategory)
    {
        List<CmsCategory> list = cmsCategoryService.selectCmsCategoryList(cmsCategory);
        ExcelUtil<CmsCategory> util = new ExcelUtil<CmsCategory>(CmsCategory.class);
        return util.exportExcel(list, "category");
    }

    /**
     * 获取栏目分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:category:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(cmsCategoryService.selectCmsCategoryById(id));
    }

    /**
     * 获取栏目分类下拉树列表
     */
    @GetMapping("/treeselect")
    public AjaxResult treeselect(CmsCategory cmsCategory)
    {
        List<CmsCategory> cmsCategories = cmsCategoryService.selectCmsCategoryList(cmsCategory);
        return AjaxResult.success(cmsCategoryService.buildCateTreeSelect(cmsCategories));
    }


    /**
     * 获取栏目分类指定分类下的下拉树列表
     */
    @ApiOperation("栏目分类-获取栏目分类指定分类下的下拉树列表")
    @ApiImplicitParam(name = "id", value = "栏目分类ID", required =true,dataType="Long",paramType="path")
    @GetMapping("/getCategoryById/{id}")
    public AjaxResult getCategoryById(@PathVariable("id") Long id)
    {
        CmsCategory cmsCategory = cmsCategoryService.selectCmsCategoryById(id);
        if(null == cmsCategory){
            return AjaxResult.error("栏目分类ID错误，获取栏目分类失败");
        }
        List<CmsCategory> cmsCategories = cmsCategoryService.selectCmsCategoryByLevel(cmsCategory.getLevel()+":"+cmsCategory.getId().toString());
        cmsCategories.add(cmsCategory);
        return AjaxResult.success(cmsCategoryService.buildCateTreeSelect(cmsCategories));
    }

    /**
     * 新增栏目分类
     */
    @PreAuthorize("@ss.hasPermi('system:category:add')")
    @Log(title = "栏目分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CmsCategory cmsCategory)
    {
        if (UserConstants.NOT_UNIQUE.equals(cmsCategoryService.checkCateNameUnique(cmsCategory)))
        {
            return AjaxResult.error("新增栏目分类'" + cmsCategory.getCategoryTitle() + "'失败，栏目分类名称已存在");
        }
        return toAjax(cmsCategoryService.insertCmsCategory(cmsCategory));
    }

    /**
     * 修改栏目分类
     */
    @PreAuthorize("@ss.hasPermi('system:category:edit')")
    @Log(title = "栏目分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CmsCategory cmsCategory)
    {
        return toAjax(cmsCategoryService.updateCmsCategory(cmsCategory));
    }

    /**
     * 删除栏目分类
     */
    @PreAuthorize("@ss.hasPermi('system:category:remove')")
    @Log(title = "栏目分类", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cmsCategoryService.deleteCmsCategoryByIds(ids));
    }
}
