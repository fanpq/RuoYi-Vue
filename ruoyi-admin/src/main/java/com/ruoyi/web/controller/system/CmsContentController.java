package com.ruoyi.web.controller.system;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.exception.CustomException;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.manage.domain.RecPosition;
import com.ruoyi.system.domain.CmsCategory;
import com.ruoyi.system.service.ICmsCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.CmsContent;
import com.ruoyi.system.service.ICmsContentService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 内容文章Controller
 * 
 * @author fanpq
 * @date 2020-12-20
 */
@Api(value="内容文章接口",tags={"内容文章查询"})
@RestController
@RequestMapping("/system/content")
public class CmsContentController extends BaseController
{
    @Autowired
    private ICmsContentService cmsContentService;
    @Autowired
    private ICmsCategoryService cmsCategoryService;

    /**
     * 查询内容文章列表
     */
    @PreAuthorize("@ss.hasPermi('system:content:list')")
    @GetMapping("/list")
    public TableDataInfo list(CmsContent cmsContent)
    {
        startPage();
        List<CmsContent> list = cmsContentService.selectCmsContentList(cmsContent);
        return getDataTable(list);
    }

    /**
     * 内容文章检索-按文章内容标题和文章内容描述检索文章内容列表
     */
    @ApiOperation("文章内容-按文章内容标题和文章内容描述检索文章内容列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "searchValue", value = "综合检索条件", required =false,paramType="query"),
            @ApiImplicitParam(name = "contentTitle", value = "文章内容标题", required =false,paramType="query"),
            @ApiImplicitParam(name = "contentDescription", value = "文章内容描述", required =false,paramType="query")
    })
    @PostMapping("/getContentList")
    @ResponseBody
    public TableDataInfo getContentList(@Validated @RequestBody CmsContent cmsContent)
    {
        startPage(cmsContent.getParams());
        cmsContent.setStatus("1");
        List<CmsContent> list = cmsContentService.selectCmsContentList(cmsContent);
        return getDataTable(list);
    }

    /**
     * 按文章内容分类ID获取文章内容列表
     */
    @ApiOperation("文章内容-按文章内容分类ID获取文章当前分类下的内容列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "categoryId", value = "文章内容分类ID", required =true,paramType="query"),
            @ApiImplicitParam(name = "pageNum", value = "当前页码", required =true,paramType="query"),
            @ApiImplicitParam(name = "pageSize", value = "分页数量", required =true,paramType="query")
    })
    @GetMapping("/getContentListByCategoryId")
    @ResponseBody
    public TableDataInfo getContentListByCategoryId(@RequestParam("categoryId") Long categoryId,
                                                    @RequestParam("pageNum") Integer pageNum,
                                                    @RequestParam("pageSize") Integer pageSize)
    {
        CmsCategory cmsCategory = cmsCategoryService.selectCmsCategoryById(categoryId);
        if(null == cmsCategory){
            throw new CustomException("栏目分类ID错误，获取栏目分类失败");
        }
        List<Long> idList = new ArrayList<Long>();
        List<CmsCategory> cmsCategories = cmsCategoryService.selectCmsCategoryByLevel(cmsCategory.getLevel()+":"+cmsCategory.getId().toString());
        for (CmsCategory category:cmsCategories){
            idList.add(category.getId());
        }
        idList.add(categoryId);
        startPage();
        List<CmsContent> cmsContentList = cmsContentService.selectCmsContentByCategoryIds(idList);
        return getDataTable(cmsContentList);
    }


    /**
     * 导出内容文章列表
     */
    @PreAuthorize("@ss.hasPermi('system:content:export')")
    @Log(title = "内容文章", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(CmsContent cmsContent)
    {
        List<CmsContent> list = cmsContentService.selectCmsContentList(cmsContent);
        ExcelUtil<CmsContent> util = new ExcelUtil<CmsContent>(CmsContent.class);
        return util.exportExcel(list, "content");
    }

    /**
     * 按文章ID获取内容文字详情
     */
    @ApiOperation("文章内容-按文章ID获取内容文字详情")
    @ApiImplicitParam(name = "id", value = "文章内容ID", required =true,dataType="Long",paramType="path")
    @GetMapping(value = "/getContentById/{id}")
    public AjaxResult getContentById(@PathVariable("id") Long id)
    {
        return AjaxResult.success(cmsContentService.selectCmsContentById(id));
    }

    /**
     * 获取内容文章详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:content:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(cmsContentService.selectCmsContentById(id));
    }

    /**
     * 新增内容文章
     */
    @PreAuthorize("@ss.hasPermi('system:content:add')")
    @Log(title = "内容文章", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Validated @RequestBody CmsContent cmsContent)
    {
        return toAjax(cmsContentService.insertCmsContent(cmsContent));
    }

    /**
     * 修改内容文章
     */
    @PreAuthorize("@ss.hasPermi('system:content:edit')")
    @Log(title = "内容文章", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Validated @RequestBody CmsContent cmsContent)
    {
        cmsContent.setUpdateBy(SecurityUtils.getLoginUser().getUser().getUserId().toString());
        return toAjax(cmsContentService.updateCmsContent(cmsContent));
    }

    /**
     * 删除内容文章
     */
    @PreAuthorize("@ss.hasPermi('system:content:remove')")
    @Log(title = "内容文章", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cmsContentService.deleteCmsContentByIds(ids));
    }

}
