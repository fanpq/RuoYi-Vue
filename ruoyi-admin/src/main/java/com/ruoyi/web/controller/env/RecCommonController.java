package com.ruoyi.web.controller.env;


import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysDictData;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.service.ISysDictDataService;
import com.ruoyi.system.service.ISysDictTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Api(value="公共接口",tags={"公共接口"})
@RestController
@RequestMapping("/env/common")
public class RecCommonController extends BaseController {

    @Autowired
    private ISysDictDataService dictDataService;
    @Autowired
    private ISysDictTypeService dictTypeService;

    /**
     * 根据字典类型查询字典数据信息
     */
    @ApiOperation("数据字典-根据字典类型查询字典数据信息")
    @ApiImplicitParam(name = "dictType", value = "字典类型", required =true,dataType="String",paramType="path")
    @GetMapping(value = "/getDict/{dictType}")
    public AjaxResult getDict(@PathVariable String dictType)
    {
        List<SysDictData> data = dictTypeService.selectDictDataByType(dictType);
        if (StringUtils.isNull(data))
        {
            data = new ArrayList<SysDictData>();
        }else{
//            重写字典返回字段，将就创平台的字典值转化为int
            for (SysDictData list:data){
                try {
                    if (dictType.indexOf("env") >= 0) {
                        list.setDictCode(Long.valueOf(list.getDictValue()));
                    }
                }catch (NumberFormatException nex){
                    break;
                }
            }
        }
        return AjaxResult.success(data);
    }
}
