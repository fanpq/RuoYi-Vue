package com.ruoyi.web.controller.env;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.manage.constant.EnvConstants;
import com.ruoyi.manage.domain.*;
import com.ruoyi.manage.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * 简历信息Controller
 *
 * @author ruoyi
 * @date 2020-12-01
 */
@Api(value="简历信息Controller",tags={"简历信息管理接口"})
@RestController
@RequestMapping("/env/cv")
public class RecCvController extends BaseController
{
    private String prefix = "env/cv";

    @Autowired
    private IRecCvService recCvService;
    @Autowired
    private IRecUserService recUserService;
    @Autowired
    private IRecDeliverService recDeliverService;
    @Autowired
    private IRecCompanyService recCompanyService;
    @Autowired
    private TokenService tokenService;

    @PreAuthorize("@ss.hasPermi('env:cv:view')")
    @GetMapping()
    public String cv()
    {
        return prefix + "/cv";
    }

    /**
     * 按ID查询简历详情信息
     */
    @ApiOperation("简历-简历详情--按ID查询简历详情信息")
    @ApiImplicitParam(name = "id", value = "简历ID", required =true,dataType="Long",paramType="path")
    @GetMapping("/getCvById/{id}")
    @ResponseBody
    public AjaxResult getCvById(@PathVariable("id") Long id)
    {
        return AjaxResult.success(recCvService.selectRecCvById(id));
    }

    /**
     * 根据简历ID和企业用户userId，查询简历信息详情
     * 简历id必传，如果userId为空，则仅返回简历详情。
     * 如果userId不为空，则返回简历详情和该企业对该简历的状态（9:无、8:已收藏；0：待沟通；1：已入职；2：不合适；3、已剔除）
     */
    @ApiOperation("简历-简历详情查询-根据简历id或登陆用户ID查询简历详情。简历id必传，如果userId为空，则仅返回简历详情。如果userId不为空，则返回简历详情和该企业用户对该简历（投递）的状态（无、待沟通、已入职、不适合等）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "简历ID", required =true,paramType="query"),
            @ApiImplicitParam(name = "userId", value = "用户ID", required =false,paramType="query")
    })
    @PostMapping("/getCvByIdOrUserId")
    @ResponseBody
    public AjaxResult getCvByIdOrUserId(@ApiIgnore RecCv recCv)
    {
        RecCv recCvResult = new RecCv();
        recCvResult =  recCvService.selectRecCvById(recCv.getId());
        if (null != recCv.getUserId()) {
            //9:无、8:已收藏；0：待沟通；1：已入职；2：不合适；3、已剔除
            // 需要统计传入userId对该简历的状态
            RecUser recUser =  recUserService.selectRecUserByUserId(recCv.getUserId());
            if (null == recUser){
                recCvResult.setComFlag(new Integer(9));//企业用户对简历的状态为9:无
            }else{
                RecDeliver recDeliver = new RecDeliver();
                recDeliver.setCvId(recCv.getId());
                recDeliver.setComUserId(recUser.getId());
                List<RecDeliver> recDeliverList = recDeliverService.selectRecDeliverList(recDeliver);
                if (recDeliverList.size() > 0 ){
                    recCvResult.setComFlag(recDeliverList.get(0).getStatus());//企业用户对简历的状态
                }else {
                    recCvResult.setComFlag(new Integer(9));//企业用户对简历的状态为9:无
                }
            }
        }
        return AjaxResult.success(recCvResult);
    }

    /**
     * 查询简历信息列表
     */
    @PreAuthorize("@ss.hasPermi('env:cv:list')")
    @GetMapping("/list")
    @ResponseBody
    public TableDataInfo list(@ApiIgnore RecCv recCv)
    {
        startPage();
        List<RecCv> list = recCvService.selectRecCvList(recCv);
        return getDataTable(list);
    }

    /**
     * 简历-投递简历查询
     * 企业用户对收藏或投递到公司的简历按状态进行查询，以便进一步处理
     */
    @ApiOperation("简历-投递简历查询-企业用户对收藏或投递到公司的简历按状态进行查询，以便进一步处理,9:无、8:已收藏；0：待沟通；1：已入职；2：不合适；3、已剔除")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "comUserId", value = "企业用户ID", required =true,paramType="query"),
            @ApiImplicitParam(name = "status", value = "简历状态", required =false,paramType="query")
    })
    @PostMapping("/listByStatus")
    @ResponseBody
    public  TableDataInfo listByStatus(@ApiIgnore RecDeliver recDeliver)
    {
        RecCompany recCompany = recCompanyService.selectRecCompanyByRecUserId(recDeliver.getComUserId());
        if (null == recCompany){
            return getNoDataTable("未查到用户对应的企业或公司，请确认是否已维护企业或公司");
        }else{
            recDeliver.setComId(recCompany.getId());
            startPage(recDeliver.getParams());
            List<RecDeliver> recDeliverList = recDeliverService.selectRecDeliverListByStatus(recDeliver);
            return getDataTable(recDeliverList);
        }
    }

    /**
     * 根据简历各条件检索求职简历列表
     * 前端只允许检索发布状态的求职简历
     */
    @ApiOperation("简历-简历检索-根据检索条件检索求职简历List，求职简历状态为发布")
    @PostMapping("/getList")
    @ResponseBody
    public TableDataInfo getList(@Validated @RequestBody RecCv recCv)
    {
        // 根据前端传入的经验要求处理成对于的工龄条件  经验要求0：不限；1：1-3年；2：3-5年：3：5-10年；4：10年以上;5:实习
        if(null != recCv.getWorkAge()) {
            if (recCv.getWorkAge().equals(new Integer(0))) {
                recCv.setWorkAge(null);
            } else if (recCv.getWorkAge().equals(new Integer(1))) {
                recCv.setWorkAgeS(new Integer(1));
                recCv.setWorkAgeE(new Integer(3));
            } else if (recCv.getWorkAge().equals(new Integer(2))) {
                recCv.setWorkAgeS(new Integer(3));
                recCv.setWorkAgeE(new Integer(5));
            } else if (recCv.getWorkAge().equals(new Integer(3))) {
                recCv.setWorkAgeS(new Integer(5));
                recCv.setWorkAgeE(new Integer(10));
            } else if (recCv.getWorkAge().equals(new Integer(4))) {
                recCv.setWorkAgeS(new Integer(10));
                recCv.setWorkAgeE(new Integer(999));
            } else if (recCv.getWorkAge().equals(new Integer(5))) {
                recCv.setWorkStatus(new Integer(0));
            }
        }
        // 根据前端传入的年龄要求处理成对于的年龄条件 0：16岁~18岁；1：19岁~20岁；2：21岁~24岁；3：25岁~27岁；4：28岁~30岁；5：31岁及以上
        if (null != recCv.getAge()){
            if ( recCv.getAge().equals(new Integer(0))){
                recCv.setAgeS(new Integer(16));
                recCv.setAgeE(new Integer(18));
            }else if (recCv.getAge().equals(new Integer(1))){
                recCv.setAgeS(new Integer(19));
                recCv.setAgeE(new Integer(20));
            }else if (recCv.getAge().equals(new Integer(2))){
                recCv.setAgeS(new Integer(21));
                recCv.setAgeE(new Integer(24));
            }else if (recCv.getAge().equals(new Integer(3))){
                recCv.setAgeS(new Integer(25));
                recCv.setAgeE(new Integer(27));
            }else if (recCv.getAge().equals(new Integer(4))){
                recCv.setAgeS(new Integer(28));
                recCv.setAgeE(new Integer(30));
            }else if (recCv.getAge().equals(new Integer(5))){
                recCv.setAgeS(new Integer(31));
                recCv.setAgeE(new Integer(999));
            }
        }
        recCv.setStatus(new Integer(2));
        recCv.setCvType(new Integer(0));
        startPage(recCv.getParams());
        List<RecCv> list = recCvService.selectRecCvList(recCv);
        return getDataTable(list);
    }

    /**
     * 根据userId查询自己的简历列表
     */
    @ApiOperation("简历-我的简历-根据当前登陆用户userId查询自己的简历列表")
    @PostMapping("/getListSelf")
    @ResponseBody
    public TableDataInfo getListSelf()
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        if (null == loginUser){
            return getNoDataTable("用户未登陆或登陆已失效", HttpStatus.UNAUTHORIZED);
        }
        RecUser recUser =  recUserService.selectRecUserByUserId(loginUser.getUser().getUserId());
        if(null == recUser) {
            return getNoDataTable("用户信息获取失败", HttpStatus.UNAUTHORIZED);
        }else {
            RecCv recCv = new RecCv();
            recCv.setUserId(recUser.getId());
            startPage();
            List<RecCv> list = recCvService.selectRecCvList(recCv);
            return getDataTable(list);
        }
    }

    /**
     * 导出简历信息列表
     */
    @PreAuthorize("@ss.hasPermi('env:cv:export')")
    @Log(title = "简历信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(RecCv recCv)
    {
        List<RecCv> list = recCvService.selectRecCvList(recCv);
        ExcelUtil<RecCv> util = new ExcelUtil<RecCv>(RecCv.class);
        return util.exportExcel(list, "cv");
    }

    /**
     * 新增简历信息
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存简历信息
     */
    @PreAuthorize("@ss.hasPermi('env:cv:add')")
    @Log(title = "简历信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(RecCv recCv)
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        if (null == loginUser){
            return AjaxResult.error("用户未登录，不允许维护简历");
        }else {
            recCv.setCreateBy(loginUser.getUser().getUserId().toString());
            recCv.setUpdateBy(loginUser.getUser().getUserId().toString());
        }
        return toAjax(recCvService.insertRecCv(recCv));
    }

    /**
     * 新增保存求职简历信息
     */
    @ApiOperation("简历-求职简历新增")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "姓名", required =true,paramType="query"),
            @ApiImplicitParam(name = "photoUrl", value = "头像", required =false,paramType="query"),
            @ApiImplicitParam(name = "sex", value = "性别0:女；1:男", required =false,paramType="query"),
            @ApiImplicitParam(name = "age", value = "年龄", required =false,paramType="query"),
            @ApiImplicitParam(name = "workAge", value = "工龄", required =false,paramType="query"),
            @ApiImplicitParam(name = "birthday", value = "出生日期", required =false,paramType="query"),
            @ApiImplicitParam(name = "workStatus", value = "当前在职状态 0：在校-实习；1：在校-找工作；2：在职-考虑机会；3：在职-月内到岗；5：在职-暂不考虑；6：离职-立即到岗", required =true,paramType="query"),
            @ApiImplicitParam(name = "school", value = "毕业院校", required =true,paramType="query"),
            @ApiImplicitParam(name = "qualifications", value = "学历 1:高/中专及以下；2：大专：3：本科；4：硕士；5：博士及以上", required =true,paramType="query"),
            @ApiImplicitParam(name = "speciality", value = "毕业专业", required =false,paramType="query"),
            @ApiImplicitParam(name = "graduateDate", value = "毕业时间", required =false,paramType="query"),
            @ApiImplicitParam(name = "reqPositon", value = "求职岗位", required =true,paramType="query"),
            @ApiImplicitParam(name = "reqCity", value = "期望城市", required =false,paramType="query"),
            @ApiImplicitParam(name = "reqIndustry", value = "期望行业", required =false,paramType="query"),
            @ApiImplicitParam(name = "reqSalary", value = "期望薪资", required =false,paramType="query"),
            @ApiImplicitParam(name = "selfDesc", value = "自我描述", required =false,paramType="query"),
            @ApiImplicitParam(name = "proDesc", value = "专业及擅长描述", required =false,paramType="query"),
            @ApiImplicitParam(name = "cvUrl", value = "简历附件", required =false,paramType="query"),
            @ApiImplicitParam(name = "workHistory", value = "工作经历", required =false,paramType="query")
    })
    @Log(title = "求职简历信息", businessType = BusinessType.INSERT)
    @PostMapping("/addRecCv")
    @ResponseBody
    public AjaxResult addRecCv(@ApiIgnore @RequestBody RecCv recCv)
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        if (null == loginUser){
            return AjaxResult.error("用户未登录，不允许维护简历");
        }else {
            RecUser recUser =  recUserService.selectRecUserByUserId(loginUser.getUser().getUserId());
            if(null == recUser) {
                return AjaxResult.error("用户信息获取失败");
            }
            RecCv recCv1 = recCvService.selectRecCvByUserId(recUser.getId());
            if(null != recCv1){
                return AjaxResult.error("用户简历已经存在，不允许新增");
            }
            recCv.setCreateBy(loginUser.getUser().getUserId().toString());
            recCv.setUpdateBy(loginUser.getUser().getUserId().toString());
            recCv.setUserId(recUser.getId());
            recCv.setUserName(recUser.getUserName());
        }
        // 设置简历类型为0：求职者
        recCv.setCvType(new Integer(0));
        return toAjax(recCvService.insertRecCv(recCv));
    }

    /**
     * 修改简历信息
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        RecCv recCv = recCvService.selectRecCvById(id);
        mmap.put("recCv", recCv);
        return prefix + "/edit";
    }

    /**
     * 修改保存简历信息
     */
    @PreAuthorize("@ss.hasPermi('env:cv:edit')")
    @Log(title = "简历信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(RecCv recCv)
    {
        return toAjax(recCvService.updateRecCv(recCv));
    }

    /**
     * 修改保存简历信息
     */
    @ApiOperation("简历-简历编辑-对未发布的简历进行编辑维护")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "简历ID", required =true,paramType="query"),
            @ApiImplicitParam(name = "name", value = "姓名", required =false,paramType="query"),
            @ApiImplicitParam(name = "photoUrl", value = "头像", required =false,paramType="query"),
            @ApiImplicitParam(name = "sex", value = "性别0:女；1:男", required =false,paramType="query"),
            @ApiImplicitParam(name = "age", value = "年龄", required =false,paramType="query"),
            @ApiImplicitParam(name = "workAge", value = "工龄", required =false,paramType="query"),
            @ApiImplicitParam(name = "birthday", value = "出生日期", required =false,paramType="query"),
            @ApiImplicitParam(name = "workStatus", value = "当前在职状态 0：在校-实习；1：在校-找工作；2：在职-考虑机会；3：在职-月内到岗；5：在职-暂不考虑；6：离职-立即到岗", required =false,paramType="query"),
            @ApiImplicitParam(name = "school", value = "毕业院校", required =false,paramType="query"),
            @ApiImplicitParam(name = "qualifications", value = "学历 1:高/中专及以下；2：大专：3：本科；4：硕士；5：博士及以上", required =false,paramType="query"),
            @ApiImplicitParam(name = "speciality", value = "毕业专业", required =false,paramType="query"),
            @ApiImplicitParam(name = "graduateDate", value = "毕业时间", required =false,paramType="query"),
            @ApiImplicitParam(name = "reqPositon", value = "求职岗位", required =false,paramType="query"),
            @ApiImplicitParam(name = "reqCity", value = "期望城市", required =false,paramType="query"),
            @ApiImplicitParam(name = "reqIndustry", value = "期望行业", required =false,paramType="query"),
            @ApiImplicitParam(name = "reqSalary", value = "期望薪资", required =false,paramType="query"),
            @ApiImplicitParam(name = "selfDesc", value = "自我描述", required =false,paramType="query"),
            @ApiImplicitParam(name = "proDesc", value = "专业及擅长描述", required =false,paramType="query"),
            @ApiImplicitParam(name = "cvUrl", value = "简历附件", required =false,paramType="query"),
            @ApiImplicitParam(name = "workHistory", value = "工作经历", required =false,paramType="query")

    })
    @Log(title = "求职简历信息", businessType = BusinessType.UPDATE)
    @PostMapping("/update")
    @ResponseBody
    public AjaxResult update(@ApiIgnore @RequestBody RecCv recCv)
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        if (null == loginUser){
            return AjaxResult.error("用户未登录，不允许维护简历");
        }else {
            recCv.setUpdateBy(loginUser.getUser().getUserId().toString());
        }
        // 前端只要编辑，都把状态改为编辑
        recCv.setStatus(new Integer(0));
        recCv.setUserId(null);
        recCv.setUserName(null);
        recCv.setCvType(null);
        recCv.setCvNo(null);
        return toAjax(recCvService.updateRecCv(recCv));
    }

    /**
     * 简历在职状态更新-仅更新在职状态
     */
    @ApiOperation("简历-简历在职状态更新-仅更新在职状态")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "简历ID", required =true,paramType="query"),
            @ApiImplicitParam(name = "workStatus", value = "当前在职状态 0：在校-实习；1：在校-找工作；2：在职-考虑机会；3：在职-月内到岗；5：在职-暂不考虑；6：离职-立即到岗", required =true,paramType="query")
    })
    @Log(title = "求职简历信息", businessType = BusinessType.UPDATE)
    @PostMapping("/updateWorkStatus")
    @ResponseBody
    public AjaxResult updateWorkStatus(@ApiIgnore @RequestBody RecCv recCv)
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        if (null == loginUser){
            return AjaxResult.error("用户未登录，不允许更新简历状态");
        }
        // 只允许修改简历在职状态
        RecCv recCv1 = new RecCv();
        recCv1.setId(recCv.getId());
        recCv1.setUpdateBy(loginUser.getUser().getUserId().toString());
        recCv1.setWorkStatus(recCv.getWorkStatus());
        return toAjax(recCvService.updateRecCv(recCv1));
    }

    /**
     * 简历审核发布
     */
    @ApiOperation("简历-简历审核发布-对简历进行审核发布，使未发布的简历可见，后台使用")
    @ApiImplicitParam(name = "id", value = "简历ID", required =true,dataType="Long",paramType="path")
    @Log(title = "简历信息", businessType = BusinessType.UPDATE)
    @PostMapping("/publish/{id}")
    @ResponseBody
    public AjaxResult publish(@PathVariable("id") Long id)
    {
        RecCv recCv = recCvService.selectRecCvById(id);
        // 简历状态为2，不允许发布。也即编辑和取消发布状态都可以发布
        if (recCv.getStatus().equals(new Integer(2))) {
            return AjaxResult.error("简历状态不为编辑或关闭，不允许发布");
        }
        recCv.setStatus(new Integer(2));
        recCv.setUpdateBy(tokenService.getLoginUser(ServletUtils.getRequest()).getUser().getUserId().toString());
        recCv.setUpdateTime(DateUtils.getNowDate());
        return toAjax(recCvService.updateRecCv(recCv));
    }

    /**
     * 简历关闭
     */
    @ApiOperation("简历-简历关闭-对简历进行关闭，使已发布的简历不可见")
    @ApiImplicitParam(name = "id", value = "简历ID", required =true,dataType="Long",paramType="path")
    @Log(title = "简历信息", businessType = BusinessType.UPDATE)
    @PostMapping("/publishCancel/{id}")
    @ResponseBody
    public AjaxResult publishCancel(@PathVariable("id") Long id)
    {
        RecCv recCv = recCvService.selectRecCvById(id);
        if (!recCv.getStatus().equals(new Integer(2))) {
            return AjaxResult.error("简历状态不为已发布，不允许关闭");
        }
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        if (null == loginUser){
            return AjaxResult.error("用户未登陆或登陆已失效");
        }else {
            recCv.setUpdateBy(loginUser.getUser().getUserId().toString());
        }
        recCv.setStatus(new Integer(3));
        return toAjax(recCvService.updateRecCv(recCv));
    }

    /**
     * 根据企业ID和简历Id，记录简历投递操作日志
     */
    @ApiOperation("投递-简历投递-点击投递简历，将简历投递到企业的简历库中")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "comId", value = "公司ID", required =true,paramType="query"),
            @ApiImplicitParam(name = "cvId", value = "简历ID", required =true,paramType="query"),
            @ApiImplicitParam(name = "comPositionId", value = "投递职位ID", required =false,paramType="query"),
            @ApiImplicitParam(name = "comPositionName", value = "投递职位名称", required =false,paramType="query"),
            @ApiImplicitParam(name = "comPositionSalary", value = "职位薪资", required =false,paramType="query")
    })
    @PostMapping("/deliver")
    @ResponseBody
    public AjaxResult deliver(@ApiIgnore @RequestBody RecDeliver recDeliver)
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        return recDeliverService.recDeliverOpp(recDeliver, EnvConstants.DELIVER, loginUser,"");
    }

    /**
     * 企业用户收藏简历，并自动计入投递表中已收藏，记录简历收藏操作日志
     */
    @ApiOperation("简历-收藏简历-企业用户收藏简历")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "comUserId", value = "系统用户ID", required =true,paramType="query"),
            @ApiImplicitParam(name = "cvId", value = "简历ID", required =true,paramType="query")
    })
    @PostMapping("/cvCollect")
    @ResponseBody
    public AjaxResult cvCollect(@ApiIgnore RecDeliver recDeliver)
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        return recDeliverService.recDeliverOpp(recDeliver, EnvConstants.COLLECT, loginUser,"");
    }

    /**
     * 企业用户收藏简历，删除投递表中已收藏，记录简历取消收藏操作日志
     */
    @ApiOperation("简历-取消收藏简历-企业用户取消收藏简历")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "待沟通简历ID", required =true,paramType="query")
    })
    @PostMapping("/cvCollectCancel")
    @ResponseBody
    public AjaxResult cvCollectCancel(@ApiIgnore RecDeliver recDeliver)
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        return recDeliverService.recDeliverOpp(recDeliver, EnvConstants.COLLECTCANCEL, loginUser,"");
    }

    /**
     * 企业用户处理简历，已入职，记录简历入职操作日志
     */
    @ApiOperation("简历-已入职-企业用户对简历用户执行已入职操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "待沟通简历ID", required =true,paramType="query")
    })
    @PostMapping("/cvEntry")
    @ResponseBody
    public AjaxResult cvEntry(@ApiIgnore RecDeliver recDeliver)
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        return recDeliverService.recDeliverOpp(recDeliver, EnvConstants.ENTRY, loginUser,"");
    }

    /**
     * 企业用户处理简历，不合适，记录简历相应操作日志
     */
    @ApiOperation("简历-不合适-企业用户对简历用户执行不合适操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "待沟通简历ID", required =true,paramType="query"),
            @ApiImplicitParam(name = "rejectReason", value = "不合适原因", required =true,paramType="query")
    })
    @PostMapping("/cvImproper")
    @ResponseBody
    public AjaxResult cvImproper(@ApiIgnore RecDeliver recDeliver)
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        return recDeliverService.recDeliverOpp(recDeliver, EnvConstants.IMPROPER, loginUser,"");
    }

    /**
     * 删除简历信息
     */
    @PreAuthorize("@ss.hasPermi('env:cv:remove')")
    @Log(title = "简历信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(recCvService.deleteRecCvByIds(ids));
    }

    /**
     * 审核简历信息
     */
    @ApiOperation("简历-审核简历信息")
    @PreAuthorize("@ss.hasPermi('env:cv:edit')")
    @Log(title = "简历信息", businessType = BusinessType.UPDATE)
    @PostMapping( "/confirmRecCvs")
    @ResponseBody
    public AjaxResult confirmRecCvs(@RequestParam("ids") String ids)
    {
        return toAjax(recCvService.confirmRecCvs(ids));
    }

    /**
     * 取消审核简历信息
     */
    @PreAuthorize("@ss.hasPermi('env:cv:edit')")
    @Log(title = "简历信息", businessType = BusinessType.UPDATE)
    @PostMapping( "/confirmCancelRecCvs")
    @ResponseBody
    public AjaxResult confirmCancelRecCvs(@RequestParam("ids") String ids)
    {
        List<RecCv> recCvList = recCvService.selectRecCvListByIds(ids);
        for (RecCv recCv:recCvList){
            if (!EnvConstants.CVCONFIRM.equals(recCv.getStatus())){
                return AjaxResult.error(recCv.getUserName() + "的简历状态不为发布，取消失败");
            }
        }
        return toAjax(recCvService.confirmCancelRecCvs(ids));
    }
}
