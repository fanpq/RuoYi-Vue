package com.ruoyi.web.controller.env;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.manage.constant.EnvConstants;
import com.ruoyi.manage.domain.RecCompany;
import com.ruoyi.manage.domain.RecPosition;
import com.ruoyi.manage.service.IRecCompanyService;
import com.ruoyi.manage.service.IRecPositionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * 职位信息Controller
 * 职位Controller控制器，主要实现就创平台的所有企业职位维护、发布、收藏等相关的接口实现
 * @author fanpq
 * @date 2020-12-01
 */

@Api(value="职位信息Controller",tags={"企业职位管理接口"})
@RestController
@RequestMapping("/env/position")
public class RecPositionController extends BaseController
{
    private String prefix = "env/position";

    @Autowired
    private IRecPositionService recPositionService;
    @Autowired
    private IRecCompanyService recCompanyService;
    @Autowired
    private TokenService tokenService;

    @PreAuthorize("@ss.hasPermi('env:position:view')")
    @GetMapping()
    public String position()
    {
        return prefix + "/position";
    }

    /**
     * 按ID查询职位详情信息
     */
    @ApiOperation("职位-职位详情--按ID查询职位详情信息")
    @ApiImplicitParam(name = "id", value = "职位ID", required =true,dataType="Long",paramType="path")
    @GetMapping("/getPositionById/{id}")
    @CrossOrigin
    @ResponseBody
    public AjaxResult getPositionById(@PathVariable("id") Long id)
    {
        return AjaxResult.success(recPositionService.selectRecPositionById(id));
    }

    /**
     * 查询职位信息列表
     */
    @PreAuthorize("@ss.hasPermi('env:position:list')")
    @GetMapping("/list")
    @ResponseBody
    public TableDataInfo list(@ApiIgnore RecPosition recPosition)
    {
        startPage();
        List<RecPosition> list = recPositionService.selectRecPositionList(recPosition);
        return getDataTable(list);
    }

    /**
     * 根据职位各条件检索职位列表
     * 前端只允许检索发布状态的职位
     */
    @ApiOperation("职位-职位检索-根据职位各条件检索职位列表，职位状态为发布")
    @PostMapping("/getList")
    @ResponseBody
    public TableDataInfo getList(@Validated @RequestBody RecPosition recPosition)
    {
        startPage(recPosition.getParams());
        recPosition.setStatus(new Integer(2));
        List<RecPosition> list = recPositionService.selectRecPositionList(recPosition);
        return getDataTable(list);
    }

    /**
     * 根据企业用户查询企业所属的职位列表
     * 前端只允许检索发布状态的职位
     */
    @ApiOperation("职位-职位查询-根据企业用户查询企业所属的职位列表，职位状态为发布")
    @ApiImplicitParam(name = "comId", value = "所属企业ID", required =true,paramType="query")
    @PostMapping("/getListByComId")
    @ResponseBody
    public TableDataInfo getListByComId(@ApiIgnore RecPosition recPosition)
    {
        startPage(recPosition.getParams());
        recPosition.setStatus(new Integer(2));
        List<RecPosition> list = recPositionService.selectRecPositionList(recPosition);
        return getDataTable(list);
    }

    /**
     * 查询当前登陆人企业所属的职位列表
     */
    @ApiOperation("职位-职位查询-查询当前登陆人企业所属的职位列表，职位状态不限制")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "当前页码", required =false,paramType="query"),
            @ApiImplicitParam(name = "pageSize", value = "分页数量", required =false,paramType="query")
    })
    @PostMapping("/getListSelf")
    @ResponseBody
    public TableDataInfo getListSelf(@RequestParam("pageNum") Integer pageNum,@RequestParam("pageSize") Integer pageSize)
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        if (null == loginUser){
            return getNoDataTable("用户未登陆或登陆已失效", HttpStatus.FORBIDDEN);
        }
        RecCompany recCompany = recCompanyService.selectRecCompanyByUserId(loginUser.getUser().getUserId());
        if(null == recCompany) {
            return getNoDataTable("企业用户未编辑企业或企业状态异常");
        }
        RecPosition recPosition = new RecPosition();
        recPosition.setComId(recCompany.getId());

        startPage();
        List<RecPosition> list = recPositionService.selectRecPositionList(recPosition);
        return getDataTable(list);
    }


    /**
     * 导出职位信息列表
     */
    @PreAuthorize("@ss.hasPermi('env:position:export')")
    @Log(title = "职位信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(RecPosition recPosition)
    {
        List<RecPosition> list = recPositionService.selectRecPositionList(recPosition);
        ExcelUtil<RecPosition> util = new ExcelUtil<RecPosition>(RecPosition.class);
        return util.exportExcel(list, "position");
    }

    /**
     * 新增职位信息
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存职位信息
     */
    @PreAuthorize("@ss.hasPermi('env:position:add')")
    @Log(title = "职位信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@ApiIgnore RecPosition recPosition)
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        if (null == loginUser){
            return AjaxResult.error("用户未登陆或登陆已失效");
        }else {
            recPosition.setCreateBy(loginUser.getUser().getUserId().toString());
            recPosition.setUpdateBy(loginUser.getUser().getUserId().toString());
        }
        RecCompany recCompany = recCompanyService.selectRecCompanyByUserId(new Long(recPosition.getCreateBy()));
        if (null != recCompany) {
            recPosition.setComId(recCompany.getId());
            recPosition.setComName(recCompany.getComName());
        }
        return toAjax(recPositionService.insertRecPosition(recPosition));
    }

    /**
     * 新增保存职位信息
     */
    @ApiOperation("职位-职位新增-根据企业用户绑定的企业信息进行职位新增")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "recruitType", value = "招聘类型", required =true,paramType="query"),
            @ApiImplicitParam(name = "positionName", value = "职位名称", required =true,paramType="query"),
            @ApiImplicitParam(name = "workCity", value = "工作城市", required =false,paramType="query"),
            @ApiImplicitParam(name = "workAdr", value = "工作地点", required =false,paramType="query"),
            @ApiImplicitParam(name = "expReq", value = "经验要求", required =true,paramType="query"),
            @ApiImplicitParam(name = "eduReq", value = "学历要求", required =false,paramType="query"),
            @ApiImplicitParam(name = "salaryReq", value = "月薪资范围", required =false,paramType="query"),
            @ApiImplicitParam(name = "postName", value = "岗位名称", required =false,paramType="query"),
            @ApiImplicitParam(name = "postKey", value = "岗位关键词", required =false,paramType="query"),
            @ApiImplicitParam(name = "postDesc", value = "岗位描述", required =false,paramType="query")

    })
    @Log(title = "职位信息", businessType = BusinessType.INSERT)
    @PostMapping("/addPosition")
    @ResponseBody
    public AjaxResult addPosition(@ApiIgnore @RequestBody RecPosition recPosition)
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        if (null == loginUser){
            return AjaxResult.error("用户未登陆或登陆已失效");
        }else {
            recPosition.setCreateBy(loginUser.getUser().getUserId().toString());
            recPosition.setUpdateBy(loginUser.getUser().getUserId().toString());
        }
        RecCompany recCompany = recCompanyService.selectRecCompanyByUserId(new Long(recPosition.getCreateBy()));
        if (null != recCompany) {
            recPosition.setComId(recCompany.getId());
            recPosition.setComName(recCompany.getComName());
        }
        return toAjax(recPositionService.insertRecPosition(recPosition));
    }

    /**
     * 修改职位信息
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        RecPosition recPosition = recPositionService.selectRecPositionById(id);
        mmap.put("recPosition", recPosition);
        return prefix + "/edit";
    }

    /**
     * 修改保存职位信息
     */
    @PreAuthorize("@ss.hasPermi('env:position:edit')")
    @Log(title = "职位信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave( RecPosition recPosition)
    {
        // 职位状态为2，不允许编辑。也即编辑和取消发布状态都可以编辑
        if (recPosition.getStatus().equals(new Integer(2))) {
            return AjaxResult.error("职位状态不为编辑或关闭，不允许修改");
        }
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        if (null == loginUser){
            return AjaxResult.error("用户未登陆或登陆已失效");
        }else {
            recPosition.setUpdateBy(loginUser.getUser().getUserId().toString());
        }
        return toAjax(recPositionService.updateRecPosition(recPosition));
    }

    /**
     * 修改保存职位信息
     */
    @ApiOperation("职位-职位编辑-对未发布的职位进行编辑维护")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "职位ID", required =true,paramType="query"),
            @ApiImplicitParam(name = "recruitType", value = "招聘类型", required =false,paramType="query"),
            @ApiImplicitParam(name = "positionName", value = "职位名称", required =false,paramType="query"),
            @ApiImplicitParam(name = "workCity", value = "工作城市", required =false,paramType="query"),
            @ApiImplicitParam(name = "workAdr", value = "工作地点", required =false,paramType="query"),
            @ApiImplicitParam(name = "expReq", value = "经验要求", required =false,paramType="query"),
            @ApiImplicitParam(name = "eduReq", value = "学历要求", required =false,paramType="query"),
            @ApiImplicitParam(name = "salaryReq", value = "月薪资范围", required =false,paramType="query"),
            @ApiImplicitParam(name = "postName", value = "岗位名称", required =false,paramType="query"),
            @ApiImplicitParam(name = "postKey", value = "岗位关键词", required =false,paramType="query"),
            @ApiImplicitParam(name = "postDesc", value = "岗位描述", required =false,paramType="query")

    })
    @Log(title = "职位信息", businessType = BusinessType.UPDATE)
    @PostMapping("/update")
    @ResponseBody
    public AjaxResult update(@ApiIgnore @RequestBody RecPosition recPosition)
    {
        RecPosition recPosition1 = recPositionService.selectRecPositionById(recPosition.getId());
        // 职位状态为2，不允许编辑。也即编辑和取消发布状态都可以编辑
        if (null == recPosition1 || recPosition1.getStatus().equals(new Integer(2))) {
            return AjaxResult.error("公司职位未查到或职位状态不为编辑或关闭，不允许修改");
        }
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        if (null == loginUser){
            return AjaxResult.error("用户未登陆或登陆已失效");
        }else {
            recPosition.setUpdateBy(loginUser.getUser().getUserId().toString());
        }
        // 职位对应的公司信息及状态不允许修改
        recPosition.setComId(null);
        recPosition.setComName(null);
        recPosition.setStatus(null);
        return toAjax(recPositionService.updateRecPosition(recPosition));
    }

    /**
     * 职位发布
     */
    @ApiOperation("职位-职位发布-对职位进行发布，使未发布的职位可见")
    @ApiImplicitParam(name = "id", value = "职位ID", required =true,dataType="Long",paramType="path")
    @Log(title = "职位信息", businessType = BusinessType.UPDATE)
    @PostMapping("/publish/{id}")
    @ResponseBody
    public AjaxResult publish(@PathVariable("id") Long id)
    {
        RecPosition recPosition = recPositionService.selectRecPositionById(id);
        // 职位状态为2，不允许发布。也即编辑和取消发布状态都可以发布
        // 状态0：编辑；1：待审核；2：发布：3：取消发布；
        if (recPosition.getStatus().equals(new Integer(2))) {
            return AjaxResult.error("职位状态不为编辑或关闭，不允许发布");
        }
        // 前端的发布对应后台的待审核
        recPosition.setStatus(new Integer(1));
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        if (null == loginUser){
            return AjaxResult.error("用户未登陆或登陆已失效");
        }else {
            recPosition.setUpdateBy(loginUser.getUser().getUserId().toString());
        }
        return toAjax(recPositionService.updateRecPosition(recPosition));
    }

    /**
     * 职位关闭
     */
    @ApiOperation("职位-职位关闭-对职位进行关闭，使已发布的职位不可见")
    @ApiImplicitParam(name = "id", value = "职位ID", required =true,dataType="Long",paramType="path")
    @Log(title = "职位信息", businessType = BusinessType.UPDATE)
    @PostMapping("/publishCancel/{id}")
    @ResponseBody
    public AjaxResult publishCancel(@PathVariable("id") Long id)
    {
        RecPosition recPosition = recPositionService.selectRecPositionById(id);
        if (!recPosition.getStatus().equals(new Integer(2))) {
            return AjaxResult.error("职位状态不为已发布，不允许关闭");
        }
        // 状态0：编辑；1：待审核；2：发布：3：取消发布；
        recPosition.setStatus(new Integer(3));
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        if (null == loginUser){
            return AjaxResult.error("用户未登陆或登陆已失效");
        }else {
            recPosition.setUpdateBy(loginUser.getUser().getUserId().toString());
        }
        return toAjax(recPositionService.updateRecPosition(recPosition));
    }

    /**
     * 删除职位信息
     */
    @PreAuthorize("@ss.hasPermi('env:position:remove')")
    @Log(title = "职位信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(recPositionService.deleteRecPositionByIds(ids));
    }

    /**
     * 审核职位信息
     */
    @PreAuthorize("@ss.hasPermi('env:position:edit')")
    @Log(title = "职位信息", businessType = BusinessType.UPDATE)
    @PostMapping( "/confirmRecPositions")
    @ResponseBody
    public AjaxResult confirmRecPositions(String ids)
    {
        return toAjax(recPositionService.confirmRecPositions(ids));
    }

    /**
     * 取消审核职位信息
     */
    @PreAuthorize("@ss.hasPermi('env:position:edit')")
    @Log(title = "职位信息", businessType = BusinessType.UPDATE)
    @PostMapping( "/confirmCancelRecPositions")
    @ResponseBody
    public AjaxResult confirmCancelRecPositions(String ids)
    {
        List<RecPosition> recPositionList = recPositionService.selectRecPositionListByIds(ids);
        for (RecPosition recPosition:recPositionList){
            if (!EnvConstants.POSCONFIRM.equals(recPosition.getStatus())){
                return AjaxResult.error(recPosition.getPositionName() + "的状态不是发布状态，取消失败");
            }
        }
        return toAjax(recPositionService.confirmCancelRecPositions(ids));
    }
}
