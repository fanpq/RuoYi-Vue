package com.ruoyi.manage.mapper;

import java.util.List;
import com.ruoyi.manage.domain.RecDeliver;

/**
 * 简历投递Mapper接口
 * 
 * @author fanpq
 * @date 2020-12-03
 */
public interface RecDeliverMapper 
{
    /**
     * 查询简历投递
     * 
     * @param id 简历投递ID
     * @return 简历投递
     */
    public RecDeliver selectRecDeliverById(Long id);

    /**
     * 查询简历投递列表
     * 
     * @param recDeliver 简历投递
     * @return 简历投递集合
     */
    public List<RecDeliver> selectRecDeliverList(RecDeliver recDeliver);

    /**
     * 查询简历投递列表-按企业用户的操作状态
     *
     * @param recDeliver 简历投递
     * @return 简历投递集合
     */
    public List<RecDeliver> selectRecDeliverListByStatus(RecDeliver recDeliver);

    /**
     * 新增简历投递
     * 
     * @param recDeliver 简历投递
     * @return 结果
     */
    public int insertRecDeliver(RecDeliver recDeliver);

    /**
     * 修改简历投递
     * 
     * @param recDeliver 简历投递
     * @return 结果
     */
    public int updateRecDeliver(RecDeliver recDeliver);

    /**
     * 删除简历投递
     * 
     * @param id 简历投递ID
     * @return 结果
     */
    public int deleteRecDeliverById(Long id);

    /**
     * 批量删除简历投递
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteRecDeliverByIds(String[] ids);
}
