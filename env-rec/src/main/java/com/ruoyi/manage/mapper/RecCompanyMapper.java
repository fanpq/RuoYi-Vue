package com.ruoyi.manage.mapper;

import java.util.List;
import com.ruoyi.manage.domain.RecCompany;

/**
 * 企业信息Mapper接口
 * 
 * @author fanpq
 * @date 2020-12-03
 */
public interface RecCompanyMapper 
{
    /**
     * 查询企业信息
     * 
     * @param id 企业信息ID
     * @return 企业信息
     */
    public RecCompany selectRecCompanyById(Long id);

    /**
     * 按查询条件查询企业信息列表
     * 
     * @param recCompany 企业信息
     * @return 企业信息集合
     */
    public List<RecCompany> selectRecCompanyList(RecCompany recCompany);

    /**
     * 按企业ids查询企业信息列表
     *
     * @param ids 企业ids
     * @return 企业信息集合
     */
    public List<RecCompany> selectRecCompanyListByIds(String[] ids);

    /**
     * 新增企业信息
     * 
     * @param recCompany 企业信息
     * @return 结果
     */
    public int insertRecCompany(RecCompany recCompany);

    /**
     * 修改企业信息
     * 
     * @param recCompany 企业信息
     * @return 结果
     */
    public int updateRecCompany(RecCompany recCompany);

    /**
     * 删除企业信息
     * 
     * @param id 企业信息ID
     * @return 结果
     */
    public int deleteRecCompanyById(Long id);

    /**
     * 批量删除企业信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteRecCompanyByIds(Long[] ids);

    /**
     * 批量审核企业信息
     *
     * @param ids 需要审核的数据ID
     * @return 结果
     */
    public int confirmRecCompanys(String[] ids);

    /**
     * 批量取消审核企业信息
     *
     * @param ids 需要取消审核的数据ID
     * @return 结果
     */
    public int confirmCancelRecCompanys(String[] ids);
}
