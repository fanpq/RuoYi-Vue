package com.ruoyi.manage.mapper;

import java.util.List;
import com.ruoyi.manage.domain.RecUser;

/**
 * 用户基本Mapper接口
 * 
 * @author fanpq
 * @date 2020-12-03
 */
public interface RecUserMapper 
{
    /**
     * 根据id查询用户基本
     * 
     * @param id 用户基本ID
     * @return 用户基本
     */
    public RecUser selectRecUserById(Long id);

    /**
     * 根据userId查询用户基本
     *
     * @param userId UserID
     * @return 用户基本
     */
    public RecUser selectRecUserByUserId(Long userId);

    /**
     * 查询用户基本列表
     * 
     * @param recUser 用户基本
     * @return 用户基本集合
     */
    public List<RecUser> selectRecUserList(RecUser recUser);

    /**
     * 新增用户基本
     * 
     * @param recUser 用户基本
     * @return 结果
     */
    public int insertRecUser(RecUser recUser);

    /**
     * 修改用户基本
     * 
     * @param recUser 用户基本
     * @return 结果
     */
    public int updateRecUser(RecUser recUser);

    /**
     * 删除用户基本
     * 
     * @param id 用户基本ID
     * @return 结果
     */
    public int deleteRecUserById(Long id);

    /**
     * 批量删除用户基本
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteRecUserByIds(String[] ids);
}
