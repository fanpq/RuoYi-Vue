package com.ruoyi.manage.service;

import java.util.List;
import com.ruoyi.manage.domain.RecLog;

/**
 * 操作日志记录Service接口
 * 
 * @author ruoyi
 * @date 2020-12-03
 */
public interface IRecLogService 
{
    /**
     * 查询操作日志记录
     * 
     * @param id 操作日志记录ID
     * @return 操作日志记录
     */
    public RecLog selectRecLogById(Long id);

    /**
     * 查询操作日志记录列表
     * 
     * @param recLog 操作日志记录
     * @return 操作日志记录集合
     */
    public List<RecLog> selectRecLogList(RecLog recLog);

    /**
     * 按操作日志用户查询其操作记录
     *
     * @param logUserId 操作日志记录
     * @return 操作日志记录集合
     */
    public List<RecLog> selectRecLogByUserid(Long logUserId);

    /**
     * 统计操作日志的数量，主要用于收藏、投递等业务的数量统计
     *
     * @param recLog 操作日志记录
     * @return 统计梳理
     */
    public Integer selectRecLogCount(RecLog recLog);


    /**
     * 新增操作日志记录
     * 
     * @param recLog 操作日志记录
     * @return 结果
     */
    public int insertRecLog(RecLog recLog);

    /**
     * 修改操作日志记录
     * 
     * @param recLog 操作日志记录
     * @return 结果
     */
    public int updateRecLog(RecLog recLog);

    /**
     * 批量删除操作日志记录
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteRecLogByIds(String ids);

    /**
     * 删除操作日志记录信息
     * 
     * @param id 操作日志记录ID
     * @return 结果
     */
    public int deleteRecLogById(Long id);
}
