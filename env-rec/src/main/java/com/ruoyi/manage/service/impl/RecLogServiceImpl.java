package com.ruoyi.manage.service.impl;

import java.util.List;

import com.ruoyi.common.exception.CustomException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.bean.BeanUtils;
import com.ruoyi.manage.domain.RecUser;
import com.ruoyi.manage.mapper.RecUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.manage.mapper.RecLogMapper;
import com.ruoyi.manage.domain.RecLog;
import com.ruoyi.manage.service.IRecLogService;
import com.ruoyi.common.core.text.Convert;

/**
 * 操作日志记录Service业务层处理
 * 
 * @author fanpq
 * @date 2020-12-03
 */
@Service
public class RecLogServiceImpl implements IRecLogService 
{
    @Autowired
    private RecLogMapper recLogMapper;
    @Autowired
    private RecUserMapper recUserMapper;

    /**
     * 查询操作日志记录
     * 
     * @param id 操作日志记录ID
     * @return 操作日志记录
     */
    @Override
    public RecLog selectRecLogById(Long id)
    {
        return recLogMapper.selectRecLogById(id);
    }

    /**
     * 查询操作日志记录列表
     * 
     * @param recLog 操作日志记录
     * @return 操作日志记录
     */
    @Override
    public List<RecLog> selectRecLogList(RecLog recLog)
    {
        return recLogMapper.selectRecLogList(recLog);
    }

    /**
     * 按操作日志用户查询其操作记录
     *
     * @param logUserId 操作日志记录
     * @return 操作日志记录集合
     */
    public List<RecLog> selectRecLogByUserid(Long logUserId){
        return recLogMapper.selectRecLogByUserid(logUserId);
    }

    /**
     * 统计操作日志的数量，主要用于收藏、投递等业务的数量统计
     *
     * @param recLog 操作日志记录
     * @return 统计梳理
     */
    public Integer selectRecLogCount(RecLog recLog){
        return recLogMapper.selectRecLogCount(recLog);
    }

    /**
     * 新增操作日志记录
     * 
     * @param recLog 操作日志记录
     * @return 结果
     */
    @Override
    public int insertRecLog(RecLog recLog)
    {
        RecLog recLogNew = new RecLog();
        BeanUtils.copyBeanProp(recLogNew,recLog);

        recLogNew.setCreateTime(DateUtils.getNowDate());
        recLogNew.setUpdateTime(DateUtils.getNowDate());
        recLogNew.setDel(new Integer(0));
        recLogNew.setStatus(new Integer(0));
        return recLogMapper.insertRecLog(recLogNew);
    }

    /**
     * 修改操作日志记录
     * 
     * @param recLog 操作日志记录
     * @return 结果
     */
    @Override
    public int updateRecLog(RecLog recLog)
    {
        recLog.setUpdateTime(DateUtils.getNowDate());
        return recLogMapper.updateRecLog(recLog);
    }

    /**
     * 删除操作日志记录对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteRecLogByIds(String ids)
    {
        return recLogMapper.deleteRecLogByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除操作日志记录信息
     * 
     * @param id 操作日志记录ID
     * @return 结果
     */
    @Override
    public int deleteRecLogById(Long id)
    {
        return recLogMapper.deleteRecLogById(id);
    }
}
