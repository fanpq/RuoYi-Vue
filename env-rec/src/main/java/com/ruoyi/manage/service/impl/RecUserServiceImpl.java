package com.ruoyi.manage.service.impl;

import java.util.List;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.exception.CustomException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.manage.mapper.RecUserMapper;
import com.ruoyi.manage.domain.RecUser;
import com.ruoyi.manage.service.IRecUserService;
import com.ruoyi.common.core.text.Convert;
import org.springframework.transaction.annotation.Transactional;

/**
 * 用户基本Service业务层处理
 * 
 * @author fanpq
 * @date 2020-12-01
 */
@Service
public class RecUserServiceImpl implements IRecUserService 
{
    @Autowired
    private RecUserMapper recUserMapper;
    @Autowired
    private ISysUserService userService;

    /**
     * 查询用户基本-按招聘用户ID
     * 
     * @param id 招聘用户基本ID
     * @return 用户基本
     */
    @Override
    public RecUser selectRecUserById(Long id)
    {
        return recUserMapper.selectRecUserById(id);
    }

    /**
     * 查询用户基本-按系统用户ID
     *
     * @param userId 系统用户基本ID
     * @return 用户基本
     */
    @Override
    public RecUser selectRecUserByUserId(Long userId)
    {
        return recUserMapper.selectRecUserByUserId(userId);
    }

    /**
     * 查询用户基本列表
     * 
     * @param recUser 用户基本
     * @return 用户基本
     */
    @Override
    public List<RecUser> selectRecUserList(RecUser recUser)
    {
        return recUserMapper.selectRecUserList(recUser);
    }

    /**
     * 新增用户基本
     * 
     * @param recUser 用户基本
     * @return 结果
     */
    @Override
    public int insertRecUser(RecUser recUser)
    {
        return recUserMapper.insertRecUser(recUser);
    }

    /**
     * 平台用户注册
     *
     * @param password 密码
     * @param phonenumber 手机号
     * @param userType 用户类型0：招聘企事业；1：求职者；
     * @return 结果
     */
    @Override
    @Transactional
    public int register(String phonenumber,String password,Integer userType)
    {
        // 先注册系统用户
        if (StringUtils.isNotEmpty(phonenumber) &&
                UserConstants.NOT_UNIQUE.equals(userService.checkPhoneUniqueByPhone(phonenumber))){
            throw new CustomException("用户注册'" + phonenumber + "'失败，手机号已存在");
        }
        // 默认为求职者
        if (null == userType){
            userType = new Integer(1);
        }
        SysUser sysUser = new SysUser();
        sysUser.setCreateBy("admin");
        sysUser.setCreateTime(DateUtils.getNowDate());
        sysUser.setPassword(SecurityUtils.encryptPassword(password));
        sysUser.setPhonenumber(phonenumber);
        sysUser.setUserName(phonenumber);
        sysUser.setNickName(phonenumber);
        sysUser.setRemark("前端自主注册");

        userService.insertUser(sysUser);

        // 注册到环保平台用户
        RecUser recUser = new RecUser();
        recUser.setCreateBy(sysUser.getUserId().toString());
        recUser.setCreateTime(DateUtils.getNowDate());
        recUser.setDel(new Integer(0));
        recUser.setStatus(new Integer(0));
        recUser.setUserId(sysUser.getUserId());
        recUser.setUserName(sysUser.getNickName());
        recUser.setUserType(userType);

        return recUserMapper.insertRecUser(recUser);
    }

    /**
     * 修改用户基本
     * 
     * @param recUser 用户基本
     * @return 结果
     */
    @Override
    public int updateRecUser(RecUser recUser)
    {
        return recUserMapper.updateRecUser(recUser);
    }

    /**
     * 删除用户基本对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteRecUserByIds(String ids)
    {
        return recUserMapper.deleteRecUserByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除用户基本信息
     * 
     * @param id 用户基本ID
     * @return 结果
     */
    @Override
    public int deleteRecUserById(Long id)
    {
        return recUserMapper.deleteRecUserById(id);
    }
}
