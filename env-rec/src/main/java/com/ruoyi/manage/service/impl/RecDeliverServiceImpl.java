package com.ruoyi.manage.service.impl;

import java.util.List;

import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.manage.constant.EnvConstants;
import com.ruoyi.manage.domain.RecCompany;
import com.ruoyi.manage.domain.RecCv;
import com.ruoyi.manage.domain.RecLog;
import com.ruoyi.manage.service.IRecCompanyService;
import com.ruoyi.manage.service.IRecCvService;
import com.ruoyi.manage.service.IRecLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.manage.mapper.RecDeliverMapper;
import com.ruoyi.manage.domain.RecDeliver;
import com.ruoyi.manage.service.IRecDeliverService;
import com.ruoyi.common.core.text.Convert;

/**
 * 简历投递Service业务层处理
 * 主要实现求职者投递简历，以及公司收藏简历、以及处理处理简历的业务实现
 * @author fanpq
 * @date 2020-12-03
 */
@Service
public class RecDeliverServiceImpl implements IRecDeliverService 
{
    @Autowired
    private RecDeliverMapper recDeliverMapper;

    @Autowired
    private IRecCvService recCvService;
    @Autowired
    private IRecCompanyService recCompanyService;
    @Autowired
    private IRecLogService recLogService;



    /**
     * 查询简历投递
     * 
     * @param id 简历投递ID
     * @return 简历投递
     */
    @Override
    public RecDeliver selectRecDeliverById(Long id)
    {
        return recDeliverMapper.selectRecDeliverById(id);
    }

    /**
     * 查询简历投递列表
     * 
     * @param recDeliver 简历投递
     * @return 简历投递
     */
    @Override
    public List<RecDeliver> selectRecDeliverList(RecDeliver recDeliver)
    {
        return recDeliverMapper.selectRecDeliverList(recDeliver);
    }

    /**
     * 查询简历投递列表-按企业用户的操作状态
     *
     * @param recDeliver 简历投递
     * @return 简历投递
     */
    @Override
    public List<RecDeliver> selectRecDeliverListByStatus(RecDeliver recDeliver)
    {
        return recDeliverMapper.selectRecDeliverListByStatus(recDeliver);
    }

    /**
     * 新增简历投递
     * 
     * @param recDeliver 简历投递
     * @return 结果
     */
    @Override
    public int insertRecDeliver(RecDeliver recDeliver)
    {
        recDeliver.setCreateTime(DateUtils.getNowDate());
        recDeliver.setDel(new Integer(0));
        return recDeliverMapper.insertRecDeliver(recDeliver);
    }

    /**
     * 修改简历投递
     * 
     * @param recDeliver 简历投递
     * @return 结果
     */
    @Override
    public int updateRecDeliver(RecDeliver recDeliver)
    {
        recDeliver.setUpdateTime(DateUtils.getNowDate());
        return recDeliverMapper.updateRecDeliver(recDeliver);
    }

    /**
     * 删除简历投递对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteRecDeliverByIds(String ids)
    {
        return recDeliverMapper.deleteRecDeliverByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除简历投递信息
     * 
     * @param id 简历投递ID
     * @return 结果
     */
    @Override
    public int deleteRecDeliverById(Long id)
    {
        return recDeliverMapper.deleteRecDeliverById(id);
    }

    /**
     * 企业用户对简历的操作
     * 操作包括8:已收藏；7:取消收藏；0：待沟通（联系）；1：已入职；2：不合适；
     * 求职者对简历的操作
     * 操作包括：投递
     * @param recDeliver 简历投递
     * @return 结果
     */
    @Override
    public AjaxResult recDeliverOpp(RecDeliver recDeliver, Integer oppType, LoginUser loginUser,String phone)
    {
        // 翻译对简历的操作
        String oppName = "";
        Integer logOppType = oppType;
        if(EnvConstants.COLLECT.equals(oppType)){
            oppName = "收藏";
            recDeliver.setCollectTime(DateUtils.getNowDate());
            recDeliver.setLastestOpp(EnvConstants.COLLECT);
            recDeliver.setLastestTime(DateUtils.getNowDate());
        }else if(EnvConstants.COLLECTCANCEL.equals(oppType)){
            oppName = "取消收藏";
            recDeliver.setCollectTime(null);
            recDeliver.setLastestOpp(EnvConstants.COLLECTCANCEL);
            recDeliver.setLastestTime(DateUtils.getNowDate());
        }else if(EnvConstants.CONNECT.equals(oppType)){
            oppName = "沟通";
            recDeliver.setConnectTime(DateUtils.getNowDate());
            recDeliver.setLastestOpp(EnvConstants.CONNECT);
            recDeliver.setLastestTime(DateUtils.getNowDate());
        }else if(EnvConstants.DELIVER.equals(oppType)){
            oppName = "投递";
            // 改变投递简历状态为待沟通
            oppType = EnvConstants.CONNECT;
            recDeliver.setConnectTime(DateUtils.getNowDate());
            recDeliver.setLastestOpp(EnvConstants.DELIVER);
            recDeliver.setLastestTime(DateUtils.getNowDate());
        }else if(EnvConstants.ENTRY.equals(oppType)){
            oppName = "通过";
            recDeliver.setEntryTime(DateUtils.getNowDate());
            recDeliver.setLastestOpp(EnvConstants.ENTRY);
            recDeliver.setLastestTime(DateUtils.getNowDate());
        }else if(EnvConstants.IMPROPER.equals(oppType)){
            oppName = "剔除";
            recDeliver.setRejectTime(DateUtils.getNowDate());
            recDeliver.setLastestOpp(EnvConstants.IMPROPER);
            recDeliver.setLastestTime(DateUtils.getNowDate());
        }
        // 投递简历ID为空，说明是简历新增，否则为修改
        // 投递简历ID为空，按传入的cvId和comId进行查询，否则按查询投递简历后的cvId和comId进行查询
        RecDeliver recDeliverUpdate = new RecDeliver();
        RecCv recCv = new RecCv();
        RecCompany recCompany = new RecCompany();
        int returnNum = 0;
        String oppLog = "";
        if(null == recDeliver.getId()){
            // 判断同一份简历是否投递到同一家公司？如是，则不允许投递
            RecDeliver recDeliver1 = new RecDeliver();
            recDeliver1.setCvId(recDeliver.getCvId());
            recDeliver1.setComId(recDeliver.getComId());
            List<RecDeliver> recDeliverList = this.selectRecDeliverList(recDeliver1);
            if(recDeliverList.size() > 0) {
                // 如果是投递则返回错误提示，不允许再投递。否则，直接返回成功，实际不再处理。
                if(EnvConstants.DELIVER.equals(oppType)) {
                    return AjaxResult.error("已经投递过该公司");
                }else{
                    return AjaxResult.success("操作成功",phone);
                }
            }
            // 获取简历
            recCv = recCvService.selectRecCvById(recDeliver.getCvId());
            if (null == recCv ){
                return AjaxResult.error("未查到简历信息或简历状态异常，请刷新后重试");
            }
            // 获取公司
            recCompany = recCompanyService.selectRecCompanyById(recDeliver.getComId());
            if (null == recCompany ){
                recCompany = recCompanyService.selectRecCompanyByUserId(recDeliver.getComUserId());
                if (null == recCompany ) {
                    // 返回特殊code，方便前端判断跳转
                    return AjaxResult.error(HttpStatus.NO_CONTENT,"未查到公司信息或公司状态异常，请刷新后重试");
                }
            }
            // 组织投递数据并新增投递表
            recDeliver.setCreateBy(loginUser.getUser().getUserId().toString());
            recDeliver.setUpdateBy(loginUser.getUser().getUserId().toString());
            recDeliver.setUpdateTime(DateUtils.getNowDate());
            recDeliver.setComId(recCompany.getId());
            recDeliver.setComName(recCompany.getComName());
            recDeliver.setComUserId(recCompany.getUserId());
            recDeliver.setComUserName(recCompany.getUserName());
            // 8:已收藏；0：待沟通；1：已入职；2：不合适
            recDeliver.setStatus(oppType);
            recDeliver.setUserId(recCv.getUserId());
            recDeliver.setUserName(recCv.getUserName());
            recDeliver.setCvId(recCv.getId());
            recDeliver.setCvName(recCv.getName());
            // 2020-12-27 fanpq 增加简历用户相关的简历用户年龄，简历用户学历，简历用户期望薪资
            recDeliver.setUserAge(recCv.getAge());
            recDeliver.setUserQualifications(recCv.getQualifications());
            recDeliver.setUserReqSalary(recCv.getReqSalary());

            recDeliver.setRemark(oppLog);
            returnNum = this.insertRecDeliver(recDeliver);
        }else {
            recDeliverUpdate = this.selectRecDeliverById(recDeliver.getId());

            recCv = recCvService.selectRecCvById(recDeliverUpdate.getCvId());
            recCompany = recCompanyService.selectRecCompanyById(recDeliverUpdate.getComId());

            // 限制只能修改如下与操作相关的字段
            recDeliverUpdate.setStatus(oppType);
            recDeliverUpdate.setConnectTime(recDeliver.getConnectTime());
            recDeliverUpdate.setCollectTime(recDeliver.getCollectTime());
            recDeliverUpdate.setEntryTime(recDeliver.getEntryTime());
            recDeliverUpdate.setRejectTime(recDeliver.getRejectTime());
            recDeliverUpdate.setRejectReason(recDeliver.getRejectReason());
            recDeliverUpdate.setLastestOpp(recDeliver.getLastestOpp());
            recDeliverUpdate.setLastestTime(recDeliver.getLastestTime());
            recDeliverUpdate.setUpdateBy(loginUser.getUser().getUserId().toString());
            recDeliverUpdate.setUpdateTime(DateUtils.getNowDate());

            if (EnvConstants.COLLECTCANCEL.equals(oppType)){
                returnNum = this.deleteRecDeliverById(recDeliver.getId());
            }else {
                returnNum = this.updateRecDeliver(recDeliverUpdate);
            }
        }

        if (returnNum <= 0) {
            return AjaxResult.error(oppName + "失败，请刷新后重试");
        }

        // 记录企业用户操作简历的日志记录
        // 8:已收藏；7:取消收藏；0：待沟通；1：已入职；2：不合适；
        RecLog recLog = new RecLog();
        if(EnvConstants.DELIVER.equals(logOppType)){
            oppLog = DateUtils.getTime() +"，"+ recCv.getUserName() + oppName + recCompany.getComName();
            recLog.setLogObjId(recCompany.getId());
            recLog.setObj1(recCv.getId().toString());
            recLog.setObj2(recCv.getName());
        }else {
            oppLog = DateUtils.getTime() + "，" + recCompany.getComName() + oppName + recCv.getUserName();
            recLog.setLogObjId(recCv.getId());
            recLog.setObj1(recCompany.getId().toString());
            recLog.setObj2(recCompany.getComName());
        }
        recLog.setCreateBy(loginUser.getUser().getUserId().toString());
        recLog.setUpdateBy(loginUser.getUser().getUserId().toString());
        recLog.setLogUserId(loginUser.getUser().getUserId());
        recLog.setLogUserName(loginUser.getUser().getUserName());
        recLog.setLogType(logOppType);
        recLog.setLogAbstract(oppLog);
        recLogService.insertRecLog(recLog);
        return AjaxResult.success("操作成功",phone);
    }
}
