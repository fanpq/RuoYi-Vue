package com.ruoyi.manage.service;

import java.util.List;

import com.ruoyi.manage.domain.RecCompany;
import com.ruoyi.manage.domain.RecCv;

/**
 * 简历信息Service接口
 * 
 * @author ruoyi
 * @date 2020-12-01
 */
public interface IRecCvService 
{
    /**
     * 查询简历信息
     * 
     * @param id 简历信息ID
     * @return 简历信息
     */
    public RecCv selectRecCvById(Long id);

    /**
     * 查询简历信息
     *
     * @param userId 招聘用户ID
     * @return 简历信息
     */
    public RecCv selectRecCvByUserId(Long userId);

    /**
     * 查询简历信息列表
     * 
     * @param recCv 简历信息
     * @return 简历信息集合
     */
    public List<RecCv> selectRecCvList(RecCv recCv);

    /**
     * 按ids简历信息列表
     *
     * @param ids 简历信息ids
     * @return 简历信息集合
     */
    public List<RecCv> selectRecCvListByIds(String ids);

    /**
     * 新增简历信息
     * 
     * @param recCv 简历信息
     * @return 结果
     */
    public int insertRecCv(RecCv recCv);

    /**
     * 修改简历信息
     * 
     * @param recCv 简历信息
     * @return 结果
     */
    public int updateRecCv(RecCv recCv);

    /**
     * 批量删除简历信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteRecCvByIds(Long[] ids);

    /**
     * 删除简历信息信息
     * 
     * @param id 简历信息ID
     * @return 结果
     */
    public int deleteRecCvById(Long id);

    /**
     * 批量审核简历信息
     *
     * @param ids 需要审核的数据ID
     * @return 结果
     */
    public int confirmRecCvs(String ids);

    /**
     * 批量取消审核简历信息
     *
     * @param ids 需要取消审核的数据ID
     * @return 结果
     */
    public int confirmCancelRecCvs(String ids);
}
