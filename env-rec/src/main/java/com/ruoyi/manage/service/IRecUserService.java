package com.ruoyi.manage.service;

import java.util.List;
import com.ruoyi.manage.domain.RecUser;

/**
 * 用户基本Service接口
 * 
 * @author ruoyi
 * @date 2020-12-01
 */
public interface IRecUserService 
{
    /**
     * 查询用户基本-按招聘用户ID
     * 
     * @param id 按招聘用户ID
     * @return 用户基本
     */
    public RecUser selectRecUserById(Long id);

    /**
     * 查询用户基本-按系统用户ID
     *
     * @param userId 系统用户基本ID
     * @return 用户基本
     */
    public RecUser selectRecUserByUserId(Long userId);

    /**
     * 查询用户基本列表
     * 
     * @param recUser 用户基本
     * @return 用户基本集合
     */
    public List<RecUser> selectRecUserList(RecUser recUser);

    /**
     * 新增用户基本
     * 
     * @param recUser 用户基本
     * @return 结果
     */
    public int insertRecUser(RecUser recUser);

    /**
     * 平台用户注册
     *
     * @param password 密码
     * @param phonenumber 手机号
     * @param userType 用户类型0：招聘企事业；1：求职者；
     * @return 结果
     */
    public int register(String phonenumber,String password,Integer userType);

    /**
     * 修改用户基本
     * 
     * @param recUser 用户基本
     * @return 结果
     */
    public int updateRecUser(RecUser recUser);

    /**
     * 批量删除用户基本
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteRecUserByIds(String ids);

    /**
     * 删除用户基本信息
     * 
     * @param id 用户基本ID
     * @return 结果
     */
    public int deleteRecUserById(Long id);
}
