package com.ruoyi.manage.service;

import java.util.List;
import com.ruoyi.manage.domain.RecCompany;

/**
 * 企业信息Service接口
 * 
 * @author ruoyi
 * @date 2020-11-28
 */
public interface IRecCompanyService 
{
    /**
     * 查询企业信息
     * 
     * @param userId 系统用户ID
     * @return 企业信息
     */
    public RecCompany selectRecCompanyByUserId(Long userId);

    /**
     * 查询企业信息
     *
     * @param recUserId 招聘用户ID
     * @return 企业信息
     */
    public RecCompany selectRecCompanyByRecUserId(Long recUserId);

    /**
     * 查询企业信息-系统用户ID
     *
     * @param id 企业信息ID
     * @return 企业信息
     */
    public RecCompany selectRecCompanyById(Long id);

    /**
     * 查询企业信息列表
     * 
     * @param recCompany 企业信息
     * @return 企业信息集合
     */
    public List<RecCompany> selectRecCompanyList(RecCompany recCompany);

    /**
     * 按企业ids查询企业信息列表
     *
     * @param ids 企业ids
     * @return 企业信息集合
     */
    public List<RecCompany> selectRecCompanyListByIds(String ids);

    /**
     * 新增企业信息
     * 
     * @param recCompany 企业信息
     * @return 结果
     */
    public int insertRecCompany(RecCompany recCompany);

    /**
     * 修改企业信息
     * 
     * @param recCompany 企业信息
     * @return 结果
     */
    public int updateRecCompany(RecCompany recCompany);

    /**
     * 批量删除企业信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteRecCompanyByIds(Long[] ids);

    /**
     * 删除企业信息信息
     * 
     * @param id 企业信息ID
     * @return 结果
     */
    public int deleteRecCompanyById(Long id);

    /**
     * 批量审核企业信息
     *
     * @param ids 需要审核的数据ID
     * @return 结果
     */
    public int confirmRecCompanys(String ids);

    /**
     * 批量取消审核企业信息
     *
     * @param ids 需要取消审核的数据ID
     * @return 结果
     */
    public int confirmCancelRecCompanys(String ids);
}
