package com.ruoyi.manage.service.impl;

import java.util.List;

import com.ruoyi.common.exception.CustomException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.manage.mapper.RecCompanyMapper;
import com.ruoyi.manage.mapper.RecUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.manage.mapper.RecPositionMapper;
import com.ruoyi.manage.domain.RecPosition;
import com.ruoyi.manage.service.IRecPositionService;
import com.ruoyi.common.core.text.Convert;

/**
 * 职位信息Service业务层处理
 * 
 * @author fanpq
 * @date 2020-12-01
 */
@Service
public class RecPositionServiceImpl implements IRecPositionService 
{
    @Autowired
    private RecPositionMapper recPositionMapper;
    @Autowired
    private RecUserMapper recUserMapper;
    @Autowired
    private RecCompanyMapper recCompanyMapper;

    /**
     * 查询职位信息
     * 
     * @param id 职位信息ID
     * @return 职位信息
     */
    @Override
    public RecPosition selectRecPositionById(Long id)
    {
        return recPositionMapper.selectRecPositionById(id);
    }

    /**
     * 查询职位信息列表
     * 
     * @param recPosition 职位信息
     * @return 职位信息
     */
    @Override
    public List<RecPosition> selectRecPositionList(RecPosition recPosition)
    {
        return recPositionMapper.selectRecPositionList(recPosition);
    }


    /**
     * 按ids查询职位信息列表
     *
     * @param ids 职位信息ids
     * @return 职位信息集合
     */
    @Override
    public List<RecPosition> selectRecPositionListByIds(String ids)
    {
        return recPositionMapper.selectRecPositionListByIds(Convert.toStrArray(ids));
    }

    /**
     * 新增职位信息
     * 
     * @param recPosition 职位信息
     * @return 结果
     */
    @Override
    public int insertRecPosition(RecPosition recPosition)
    {
        recPosition.setCreateTime(DateUtils.getNowDate());
        recPosition.setUpdateTime(DateUtils.getNowDate());
        recPosition.setDel(new Integer(0));
        recPosition.setStatus(new Integer(0));
        // 重新职位查询条件，只需要职位名称
        RecPosition recPosition1 = new RecPosition();

        recPosition1.setPositionName(recPosition.getPositionName());
        recPosition1.setComId(recPosition.getComId());
        List<RecPosition>  recPositionLists= recPositionMapper.selectRecPositionList(recPosition1);
        if (recPositionLists.size() > 0) {
            throw new CustomException(recPosition.getComName() + "的职位名称重复，不允许新增");
        }else {
            return recPositionMapper.insertRecPosition(recPosition);
        }
    }

    /**
     * 修改职位信息
     * 
     * @param recPosition 职位信息
     * @return 结果
     */
    @Override
    public int updateRecPosition(RecPosition recPosition)
    {
        recPosition.setUpdateTime(DateUtils.getNowDate());
        return recPositionMapper.updateRecPosition(recPosition);
    }

    /**
     * 删除职位信息对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteRecPositionByIds(Long[] ids)
    {
        return recPositionMapper.deleteRecPositionByIds(ids);
    }

    /**
     * 删除职位信息信息
     * 
     * @param id 职位信息ID
     * @return 结果
     */
    @Override
    public int deleteRecPositionById(Long id)
    {
        return recPositionMapper.deleteRecPositionById(id);
    }

    /**
     * 审核职位信息对象
     *
     * @param ids 需要审核的数据ID
     * @return 结果
     */
    @Override
    public int confirmRecPositions(String ids)
    {
        return recPositionMapper.confirmRecPositions(Convert.toStrArray(ids));
    }

    /**
     * 取消审核职位信息对象
     *
     * @param ids 需要取消审核的数据ID
     * @return 结果
     */
    @Override
    public int confirmCancelRecPositions(String ids)
    {
        return recPositionMapper.confirmCancelRecPositions(Convert.toStrArray(ids));
    }
}
