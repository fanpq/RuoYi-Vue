package com.ruoyi.manage.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.manage.mapper.RecCvMapper;
import com.ruoyi.manage.domain.RecCv;
import com.ruoyi.manage.service.IRecCvService;
import com.ruoyi.common.core.text.Convert;

/**
 * 简历信息Service业务层处理
 * 
 * @author fanpq
 * @date 2020-12-01
 */
@Service
public class RecCvServiceImpl implements IRecCvService 
{
    @Autowired
    private RecCvMapper recCvMapper;

    /**
     * 查询简历信息
     * 
     * @param id 简历信息ID
     * @return 简历信息
     */
    @Override
    public RecCv selectRecCvById(Long id)
    {
        return recCvMapper.selectRecCvById(id);
    }

    /**
     * 查询简历信息
     *
     * @param userId 招聘用户ID
     * @return 简历信息
     */
    @Override
    public RecCv selectRecCvByUserId(Long userId)
    {
        return recCvMapper.selectRecCvByUserId(userId);
    }

    /**
     * 查询简历信息列表
     * 
     * @param recCv 简历信息
     * @return 简历信息
     */
    @Override
    public List<RecCv> selectRecCvList(RecCv recCv)
    {
        return recCvMapper.selectRecCvList(recCv);
    }

    /**
     * 按ids查询简历信息列表
     *
     * @param ids 简历信息ids
     * @return 简历信息集合
     */
    @Override
    public List<RecCv> selectRecCvListByIds(String ids)
    {
        return recCvMapper.selectRecCvListByIds(Convert.toStrArray(ids));
    }

    /**
     * 新增简历信息
     * 
     * @param recCv 简历信息
     * @return 结果
     */
    @Override
    public int insertRecCv(RecCv recCv)
    {
        recCv.setCreateTime(DateUtils.getNowDate());
        recCv.setUpdateTime(DateUtils.getNowDate());
        recCv.setDel(new Integer(0));
        recCv.setStatus(new Integer(0));
        return recCvMapper.insertRecCv(recCv);
    }

    /**
     * 修改简历信息
     * 
     * @param recCv 简历信息
     * @return 结果
     */
    @Override
    public int updateRecCv(RecCv recCv)
    {
        recCv.setUpdateTime(DateUtils.getNowDate());
        return recCvMapper.updateRecCv(recCv);
    }

    /**
     * 删除简历信息对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteRecCvByIds(Long[] ids)
    {
        return recCvMapper.deleteRecCvByIds(ids);
    }

    /**
     * 删除简历信息信息
     * 
     * @param id 简历信息ID
     * @return 结果
     */
    @Override
    public int deleteRecCvById(Long id)
    {
        return recCvMapper.deleteRecCvById(id);
    }

    /**
     * 审核简历信息对象
     *
     * @param ids 需要审核的数据ID
     * @return 结果
     */
    @Override
    public int confirmRecCvs(String ids)
    {
        return recCvMapper.confirmRecCvs(Convert.toStrArray(ids));
    }

    /**
     * 取消审核简历信息对象
     *
     * @param ids 需要取消审核的数据ID
     * @return 结果
     */
    @Override
    public int confirmCancelRecCvs(String ids)
    {
        return recCvMapper.confirmCancelRecCvs(Convert.toStrArray(ids));
    }
}
