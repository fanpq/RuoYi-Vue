package com.ruoyi.manage.service;

import java.util.List;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.manage.domain.RecDeliver;

/**
 * 简历投递Service接口
 * 
 * @author ruoyi
 * @date 2020-12-03
 */
public interface IRecDeliverService 
{
    /**
     * 查询简历投递
     * 
     * @param id 简历投递ID
     * @return 简历投递
     */
    public RecDeliver selectRecDeliverById(Long id);

    /**
     * 查询简历投递列表
     * 
     * @param recDeliver 简历投递
     * @return 简历投递集合
     */
    public List<RecDeliver> selectRecDeliverList(RecDeliver recDeliver);

    /**
     * 查询简历投递列表-按企业用户的操作状态
     *
     * @param recDeliver 简历投递
     * @return 简历投递集合
     */
    public List<RecDeliver> selectRecDeliverListByStatus(RecDeliver recDeliver);

    /**
     * 新增简历投递
     * 
     * @param recDeliver 简历投递
     * @return 结果
     */
    public int insertRecDeliver(RecDeliver recDeliver);

    /**
     * 修改简历投递
     * 
     * @param recDeliver 简历投递
     * @return 结果
     */
    public int updateRecDeliver(RecDeliver recDeliver);

    /**
     * 批量删除简历投递
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteRecDeliverByIds(String ids);

    /**
     * 删除简历投递信息
     * 
     * @param id 简历投递ID
     * @return 结果
     */
    public int deleteRecDeliverById(Long id);

    /**
     * 企业用户对简历的操作
     * 操作包括8:已收藏；0：待沟通；1：已入职；2：不合适；
     * @param recDeliver 简历投递
     * @return 结果
     */
    public AjaxResult recDeliverOpp(RecDeliver recDeliver, Integer oppType, LoginUser loginUser,String phone);
}
