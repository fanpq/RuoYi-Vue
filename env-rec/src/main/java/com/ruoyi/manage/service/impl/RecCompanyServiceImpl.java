package com.ruoyi.manage.service.impl;

import java.util.List;

import com.ruoyi.common.exception.CustomException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.manage.domain.RecUser;
import com.ruoyi.manage.mapper.RecUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.manage.mapper.RecCompanyMapper;
import com.ruoyi.manage.domain.RecCompany;
import com.ruoyi.manage.service.IRecCompanyService;
import com.ruoyi.common.core.text.Convert;

/**
 * 企业信息Service业务层处理
 * 提供企业注册、企业/公司信息维护、企业招聘职位发布等业务实现
 * @author fanpq
 * @date 2020-11-28
 */
@Service
public class RecCompanyServiceImpl implements IRecCompanyService 
{
    @Autowired
    private RecCompanyMapper recCompanyMapper;
    @Autowired
    private RecUserMapper recUserMapper;

    /**
     * 查询企业信息
     * 
     * @param id 企业信息ID
     * @return 企业信息
     */
    @Override
    public RecCompany selectRecCompanyById(Long id)
    {
        return recCompanyMapper.selectRecCompanyById(id);
    }

    /**
     * 查询企业信息-系统用户ID
     * 按企业信息所属的系统用户ID查询企业信息
     * @param userId 系统用户ID
     * @return 企业信息
     */
    @Override
    public RecCompany selectRecCompanyByUserId(Long userId)
    {
        RecCompany recCompany = new RecCompany();
        RecUser recUser = recUserMapper.selectRecUserByUserId(userId);
        if (null == recUser) {
            return null;
        }
        recCompany.setUserId(recUser.getId());
        List<RecCompany> lists = this.selectRecCompanyList(recCompany);
        if (lists.size() <= 0){
            return null;
        }else {
            return lists.get(0);
        }
    }

    /**
     * 查询企业信息-招聘用户ID
     * 按企业信息所属的招聘用户ID查询企业信息
     * @param recUserId 招聘用户ID
     * @return 企业信息
     */
    @Override
    public RecCompany selectRecCompanyByRecUserId(Long recUserId)
    {
        RecCompany recCompany = new RecCompany();
        recCompany.setUserId(recUserId);
        List<RecCompany> lists = this.selectRecCompanyList(recCompany);
        if (lists.size() <= 0){
            return null;
        }else {
            return lists.get(0);
        }
    }

    /**
     * 查询企业信息列表
     * 
     * @param recCompany 企业信息
     * @return 企业信息
     */
    @Override
    public List<RecCompany> selectRecCompanyList(RecCompany recCompany)
    {
        return recCompanyMapper.selectRecCompanyList(recCompany);
    }

    /**
     * 按企业ids查询企业信息列表
     *
     * @param ids 企业ids
     * @return 企业信息集合
     */
    public List<RecCompany> selectRecCompanyListByIds(String ids)
    {
        return recCompanyMapper.selectRecCompanyListByIds(Convert.toStrArray(ids));
    }

    /**
     * 新增企业信息
     *
     * 需要验证公司是否重复
     * 如果新增人在招聘人员表中存在，且已录入过公司则不允许再新增公司
     * @param recCompany 企业信息
     * @return 结果
     */
    @Override
    public int insertRecCompany(RecCompany recCompany)
    {
        recCompany.setCreateTime(DateUtils.getNowDate());
        recCompany.setUpdateTime(DateUtils.getNowDate());
        recCompany.setDel(new Integer(0));
        recCompany.setStatus(new Integer(0));
        // 重新组织公司查询条件，只需要公司名称
        RecCompany recCompany1 = new RecCompany();
        // 按当前登陆人查询注册的用户
        RecUser recUser = recUserMapper.selectRecUserByUserId(new Long(recCompany.getCreateBy()));
        //非注册用户，新增公司数不限制，但不允许公司名称、简称重复
        if (null == recUser) {
            recCompany1.setComName(recCompany.getComName());
            List<RecCompany> recCompanyList = recCompanyMapper.selectRecCompanyList(recCompany1);
            if (recCompanyList.size() > 0) {
                throw new CustomException("企业或公司重复，不允许新增");
            }
        }else{
            recCompany1.setUserId(recUser.getId());
            List<RecCompany> recCompanyList = recCompanyMapper.selectRecCompanyList(recCompany1);
            //如果当前登陆人属于注册用户，且关联企业已经存在，则不允许新增
            if (recCompanyList.size()>0){
                throw new CustomException("一个用户不允许有多家企业或公司");
            }else {
                recCompany.setUserId(recUser.getId());
                recCompany.setUserName(recUser.getUserName());
            }
        }
        return recCompanyMapper.insertRecCompany(recCompany);
    }

    /**
     * 修改企业信息
     * 
     * @param recCompany 企业信息
     * @return 结果
     */
    @Override
    public int updateRecCompany(RecCompany recCompany)
    {
        recCompany.setUpdateTime(DateUtils.getNowDate());
        return recCompanyMapper.updateRecCompany(recCompany);
    }

    /**
     * 删除企业信息对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteRecCompanyByIds(Long[] ids)
    {
        return recCompanyMapper.deleteRecCompanyByIds(ids);
    }

    /**
     * 删除企业信息信息
     * 
     * @param id 企业信息ID
     * @return 结果
     */
    @Override
    public int deleteRecCompanyById(Long id)
    {
        return recCompanyMapper.deleteRecCompanyById(id);
    }

    /**
     * 审核企业信息对象
     *
     * @param ids 需要审核的数据ID
     * @return 结果
     */
    @Override
    public int confirmRecCompanys(String ids)
    {
        return recCompanyMapper.confirmRecCompanys(Convert.toStrArray(ids));
    }

    /**
     * 取消审核企业信息对象
     *
     * @param ids 需要取消审核的数据ID
     * @return 结果
     */
    @Override
    public int confirmCancelRecCompanys(String ids)
    {
        return recCompanyMapper.confirmCancelRecCompanys(Convert.toStrArray(ids));
    }
}
