package com.ruoyi.manage.service;

import java.util.List;

import com.ruoyi.manage.domain.RecCv;
import com.ruoyi.manage.domain.RecPosition;

/**
 * 职位信息Service接口
 * 
 * @author ruoyi
 * @date 2020-12-01
 */
public interface IRecPositionService 
{
    /**
     * 查询职位信息
     * 
     * @param id 职位信息ID
     * @return 职位信息
     */
    public RecPosition selectRecPositionById(Long id);

    /**
     * 查询职位信息列表
     * 
     * @param recPosition 职位信息
     * @return 职位信息集合
     */
    public List<RecPosition> selectRecPositionList(RecPosition recPosition);

    /**
     * 按ids查询职位信息列表
     *
     * @param ids 职位信息ids
     * @return 职位信息集合
     */
    public List<RecPosition> selectRecPositionListByIds(String ids);

    /**
     * 新增职位信息
     * 
     * @param recPosition 职位信息
     * @return 结果
     */
    public int insertRecPosition(RecPosition recPosition);

    /**
     * 修改职位信息
     * 
     * @param recPosition 职位信息
     * @return 结果
     */
    public int updateRecPosition(RecPosition recPosition);

    /**
     * 批量删除职位信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteRecPositionByIds(Long[] ids);

    /**
     * 删除职位信息信息
     * 
     * @param id 职位信息ID
     * @return 结果
     */
    public int deleteRecPositionById(Long id);

    /**
     * 批量审核职位信息
     *
     * @param ids 需要审核的数据ID
     * @return 结果
     */
    public int confirmRecPositions(String ids);

    /**
     * 批量取消审核职位信息
     *
     * @param ids 需要取消审核的数据ID
     * @return 结果
     */
    public int confirmCancelRecPositions(String ids);
}
