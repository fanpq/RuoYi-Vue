package com.ruoyi.manage.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * 简历投递对象 rec_deliver
 *
 * @author fanpq
 * @date 2020-12-03
 */
public class RecDeliver extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;

    /**
     * 删除标志默认0，删除标志：1
     */
    @Excel(name = "删除标志默认0，删除标志：1")
    private Integer del;

    /**
     * 状态0：待沟通；1：已入职；2：不合适；3、已剔除
     */
    @Excel(name = "状态0：待沟通；1：已入职；2：不合适；3、已剔除")
    private Integer status;

    /**
     * 简历用户ID，来源于招聘用户表ID
     */
    @Excel(name = "简历用户ID，来源于招聘用户表ID")
    private Long userId;

    /**
     * 简历用户名称，来源于招聘用户表
     */
    @Excel(name = "简历用户名称，来源于招聘用户表")
    private String userName;

    /**
     * 公司关联用户ID，来源于招聘用户表ID
     */
    @Excel(name = "公司关联用户ID，来源于招聘用户表ID")
    private Long comUserId;

    /**
     * 公司关联用户名称，来源于招聘用户表
     */
    @Excel(name = "公司关联用户名称，来源于招聘用户表")
    private String comUserName;

    /**
     * 公司ID
     */
    @Excel(name = "公司ID")
    private Long comId;

    /**
     * 公司名称
     */
    @Excel(name = "公司名称")
    private String comName;

    /**
     * 简历ID
     */
    @Excel(name = "简历ID")
    private Long cvId;

    /**
     * 简历名称
     */
    @Excel(name = "简历名称")
    private String cvName;

    /**
     * 备用1
     */
    @Excel(name = "备用1")
    private String back1;

    /**
     * 备用2
     */
    @Excel(name = "备用2")
    private String back2;

    /**
     * 备用3
     */
    @Excel(name = "备用3")
    private String back3;

    /**
     * 备用4
     */
    @Excel(name = "备用4")
    private String back4;

    /**
     * 备用5
     */
    @Excel(name = "备用5")
    private String back5;

    // 2020-12-27 fanpq 增加投递简历的相关操作内容字段
    /**
     * 简历用户年龄
     */
    @Excel(name = "简历用户年龄")
    private Integer userAge;

    /**
     * 简历用户学历
     */
    @Excel(name = "简历用户学历")
    private Integer userQualifications;

    /**
     * 简历用户期望薪资
     */
    @Excel(name = "简历用户期望薪资")
    private Long userReqSalary;

    /**
     * 投递职位ID
     */
    @Excel(name = "投递职位ID")
    private Long comPositionId;
    /**
     * 投递职位名称
     */
    @Excel(name = "投递职位名称")
    private String comPositionName;
    /**
     * 职位薪资
     */
    @Excel(name = "职位薪资")
    private Integer comPositionSalary;

    /**
     * 收藏时间
     */
    @Excel(name = "收藏时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date collectTime;

    /**
     * 沟通时间
     */
    @Excel(name = "沟通时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date connectTime;
    /**
     * 投递时间
     */
    @Excel(name = "投递时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date deliverTime;
    /**
     * 入职时间
     */
    @Excel(name = "入职时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date entryTime;
    /**
     * 拒绝时间
     */
    @Excel(name = "拒绝时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date rejectTime;
    /**
     * 拒绝理由
     */
    @Excel(name = "拒绝理由")
    private String rejectReason;
    /**
     * 最新操作
     */
    @Excel(name = "最新操作")
    private Integer lastestOpp;
    /**
     * 最新操作时间
     */
    @Excel(name = "最新操作时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastestTime;
    /**
     * 简历用户求职最新状态,
     */
    @Excel(name = "简历用户求职最新状态")
    private Integer userWorkStatus;
    /**
     * 职位最新状态
     */
    @Excel(name = "职位最新状态")
    private Integer comPositionStatus;
    /**
     * 简历用户联系电话
     */
    @Excel(name = "简历用户联系电话")
    private String phonenumber;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setDel(Integer del) {
        this.del = del;
    }

    public Integer getDel() {
        return del;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getStatus() {
        return status;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setComUserId(Long comUserId) {
        this.comUserId = comUserId;
    }

    public Long getComUserId() {
        return comUserId;
    }

    public void setComUserName(String comUserName) {
        this.comUserName = comUserName;
    }

    public String getComUserName() {
        return comUserName;
    }

    public void setComId(Long comId) {
        this.comId = comId;
    }

    public Long getComId() {
        return comId;
    }

    public void setComName(String comName) {
        this.comName = comName;
    }

    public String getComName() {
        return comName;
    }

    public void setCvId(Long cvId) {
        this.cvId = cvId;
    }

    public Long getCvId() {
        return cvId;
    }

    public void setCvName(String cvName) {
        this.cvName = cvName;
    }

    public String getCvName() {
        return cvName;
    }

    public void setBack1(String back1) {
        this.back1 = back1;
    }

    public String getBack1() {
        return back1;
    }

    public void setBack2(String back2) {
        this.back2 = back2;
    }

    public String getBack2() {
        return back2;
    }

    public void setBack3(String back3) {
        this.back3 = back3;
    }

    public String getBack3() {
        return back3;
    }

    public void setBack4(String back4) {
        this.back4 = back4;
    }

    public String getBack4() {
        return back4;
    }

    public void setBack5(String back5) {
        this.back5 = back5;
    }

    public String getBack5() {
        return back5;
    }

    public Integer getUserAge() {
        return userAge;
    }

    public void setUserAge(Integer userAge) {
        this.userAge = userAge;
    }

    public Long getUserReqSalary() {
        return userReqSalary;
    }

    public void setUserReqSalary(Long userReqSalary) {
        this.userReqSalary = userReqSalary;
    }

    public Long getComPositionId() {
        return comPositionId;
    }

    public void setComPositionId(Long comPositionId) {
        this.comPositionId = comPositionId;
    }

    public String getComPositionName() {
        return comPositionName;
    }

    public void setComPositionName(String comPositionName) {
        this.comPositionName = comPositionName;
    }

    public Integer getComPositionSalary() {
        return comPositionSalary;
    }

    public void setComPositionSalary(Integer comPositionSalary) {
        this.comPositionSalary = comPositionSalary;
    }

    public Date getCollectTime() {
        return collectTime;
    }

    public void setCollectTime(Date collectTime) {
        this.collectTime = collectTime;
    }

    public Date getDeliverTime() {
        return deliverTime;
    }

    public void setDeliverTime(Date deliverTime) {
        this.deliverTime = deliverTime;
    }

    public Date getEntryTime() {
        return entryTime;
    }

    public void setEntryTime(Date entryTime) {
        this.entryTime = entryTime;
    }

    public Date getRejectTime() {
        return rejectTime;
    }

    public void setRejectTime(Date rejectTime) {
        this.rejectTime = rejectTime;
    }

    public String getRejectReason() {
        return rejectReason;
    }

    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }

    public Integer getLastestOpp() {
        return lastestOpp;
    }

    public void setLastestOpp(Integer lastestOpp) {
        this.lastestOpp = lastestOpp;
    }

    public Date getLastestTime() {
        return lastestTime;
    }

    public void setLastestTime(Date lastestTime) {
        this.lastestTime = lastestTime;
    }

    public Integer getUserQualifications() {
        return userQualifications;
    }

    public void setUserQualifications(Integer userQualifications) {
        this.userQualifications = userQualifications;
    }

    public Date getConnectTime() {
        return connectTime;
    }

    public void setConnectTime(Date connectTime) {
        this.connectTime = connectTime;
    }

    public Integer getUserWorkStatus() {
        return userWorkStatus;
    }

    public void setUserWorkStatus(Integer userWorkStatus) {
        this.userWorkStatus = userWorkStatus;
    }

    public Integer getComPositionStatus() {
        return comPositionStatus;
    }

    public void setComPositionStatus(Integer comPositionStatus) {
        this.comPositionStatus = comPositionStatus;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("del", getDel())
                .append("status", getStatus())
                .append("userId", getUserId())
                .append("userName", getUserName())
                .append("comUserId", getComUserId())
                .append("comUserName", getComUserName())
                .append("comId", getComId())
                .append("comName", getComName())
                .append("cvId", getCvId())
                .append("cvName", getCvName())
                .append("remark", getRemark())

                .append("userAge", getUserAge())
                .append("userQualifications", getUserQualifications())
                .append("userReqSalary", getUserReqSalary())
                .append("comPositionId", getComPositionId())
                .append("comPositionName", getComPositionName())
                .append("comPositionSalary", getComPositionSalary())
                .append("collectTime", getCollectTime())
                .append("connectTime", getConnectTime())
                .append("deliverTime", getDeliverTime())
                .append("entryTime", getEntryTime())
                .append("rejectTime", getRejectTime())
                .append("rejectReason", getRejectReason())
                .append("lastestOpp", getLastestOpp())
                .append("lastestTime", getLastestTime())
                .append("userWorkStatus", getUserWorkStatus())
                .append("comPositionStatus", getComPositionStatus())
                .append("phonenumber", getPhonenumber())

                .append("back1", getBack1())
                .append("back2", getBack2())
                .append("back3", getBack3())
                .append("back4", getBack4())
                .append("back5", getBack5())
                .toString();
    }
}
