package com.ruoyi.manage.domain;

import java.util.Date;

import com.alibaba.fastjson.annotation.JSONPOJOBuilder;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 简历信息对象 rec_cv
 * 
 * @author fanpq
 * @date 2020-12-03
 */
public class RecCv extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 删除标志默认0，删除标志：1 */
    @Excel(name = "删除标志 默认0，删除标志：1")
    private Integer del;

    /** 状态0：编辑；1：待审核；2：发布：3：取消发布； */
    @Excel(name = "状态 0：编辑；1：待审核；2：发布：3：取消发布；")
    private Integer status;

    /** 关联用户ID，来源于招聘用户表ID */
    @Excel(name = "关联用户ID，来源于招聘用户表ID")
    private Long userId;

    /** 类型0：求职简历；1：专家简历 */
    @Excel(name = "类型 0：求职简历；1：专家简历")
    private Integer cvType;

    /** 简历编号 */
    @Excel(name = "简历编号")
    private String cvNo;

    /** 关联用户名称，来源于招聘用户表 */
    @Excel(name = "关联用户名称，来源于招聘用户表")
    private String userName;

    /** 姓名 */
    @Excel(name = "姓名")
    private String name;

    /** 头像 */
    @Excel(name = "头像")
    private String photoUrl;

    /** 性别0:女；1:男 */
    @Excel(name = "性别0:女；1:男")
    private Integer sex;

    /** 年龄 */
    @Excel(name = "年龄")
    private Integer age;

    /** 年龄条件起 */
    private Integer ageS;
    /** 年龄条件止 */
    private Integer ageE;

    /** 工龄 */
    @Excel(name = "工龄")
    private Integer workAge;

    /** 工龄条件起 */
    private Integer workAgeS;
    /** 工龄条件止 */
    private Integer workAgeE;

    /** 出生日期 */
    @Excel(name = "出生日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date birthday;

    /** 当前在职状态0：在校-实习；1：在校-找工作；2：在职-考虑机会；3：在职-月内到岗；5：在职-暂不考虑；6：离职-立即到岗 */
    @Excel(name = "当前在职状态 0：在校-实习；1：在校-找工作；2：在职-考虑机会；3：在职-月内到岗；5：在职-暂不考虑；6：离职-立即到岗")
    private Integer workStatus;

    /** 毕业院校 */
    @Excel(name = "毕业院校")
    private String school;

    /** 学历1:高/中专及以下；2：大专：3：本科；4：硕士；5：博士及以上 */
    @Excel(name = "学历 1:高/中专及以下；2：大专：3：本科；4：硕士；5：博士及以上")
    private Integer qualifications;

    /** 毕业专业 */
    @Excel(name = "毕业专业")
    private Integer speciality;

    /** 毕业时间 */
    @Excel(name = "毕业时间", width = 30, dateFormat = "yyyy-MM")
    @JsonFormat(pattern = "yyyy-MM")
    private Date graduateDate;

    /** 求职岗位 */
    @Excel(name = "求职岗位")
    private String reqPositon;

    /** 期望城市 */
    @Excel(name = "期望城市")
    private String reqCity;

    /** 期望行业 */
    @Excel(name = "期望行业")
    private String reqIndustry;

    /** 期望薪资 */
    @Excel(name = "期望薪资")
    private Long reqSalary;

    /** 自我描述 */
    @Excel(name = "自我描述")
    private String selfDesc;

    /** 专业及擅长描述 */
    @Excel(name = "专业及擅长描述")
    private String proDesc;

    /** 专家介绍类型为1时使用 */
    @Excel(name = "专家介绍 类型为1时使用")
    private String cvDesc;

    /** 简历 */
    @Excel(name = "简历")
    private String cvUrl;

    /** 工作经历 */
    @Excel(name = "工作经历")
    private String workHistory;

    /** 备用1 */
    @Excel(name = "备用1")
    private String back1;

    /** 备用2 */
    @Excel(name = "备用2")
    private String back2;

    /** 备用3 */
    @Excel(name = "备用3")
    private String back3;

    /** 备用4 */
    @Excel(name = "备用4")
    private String back4;

    /** 备用5 */
    @Excel(name = "备用5")
    private String back5;

    /** 企业用户对简历管理标志,9：无、8:已收藏;0：待沟通；1：已入职；2：不合适；3、已剔除 */
    private Integer comFlag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setDel(Integer del) 
    {
        this.del = del;
    }

    public Integer getDel() 
    {
        return del;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setCvType(Integer cvType) 
    {
        this.cvType = cvType;
    }

    public Integer getCvType() 
    {
        return cvType;
    }
    public void setCvNo(String cvNo) 
    {
        this.cvNo = cvNo;
    }

    public String getCvNo() 
    {
        return cvNo;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setPhotoUrl(String photoUrl) 
    {
        this.photoUrl = photoUrl;
    }

    public String getPhotoUrl() 
    {
        return photoUrl;
    }
    public void setSex(Integer sex) 
    {
        this.sex = sex;
    }

    public Integer getSex() 
    {
        return sex;
    }
    public void setAge(Integer age)
    {
        this.age = age;
    }

    public Integer getAge()
    {
        return age;
    }
    public void setWorkAge(Integer workAge)
    {
        this.workAge = workAge;
    }

    public Integer getWorkAge()
    {
        return workAge;
    }
    public void setBirthday(Date birthday) 
    {
        this.birthday = birthday;
    }

    public Date getBirthday() 
    {
        return birthday;
    }
    public void setWorkStatus(Integer workStatus) 
    {
        this.workStatus = workStatus;
    }

    public Integer getWorkStatus() 
    {
        return workStatus;
    }
    public void setSchool(String school) 
    {
        this.school = school;
    }

    public String getSchool() 
    {
        return school;
    }
    public void setQualifications(Integer qualifications) 
    {
        this.qualifications = qualifications;
    }

    public Integer getQualifications() 
    {
        return qualifications;
    }
    public void setSpeciality(Integer speciality)
    {
        this.speciality = speciality;
    }

    public Integer getSpeciality()
    {
        return speciality;
    }
    public void setGraduateDate(Date graduateDate) 
    {
        this.graduateDate = graduateDate;
    }

    public Date getGraduateDate() 
    {
        return graduateDate;
    }
    public void setReqPositon(String reqPositon) 
    {
        this.reqPositon = reqPositon;
    }

    public String getReqPositon() 
    {
        return reqPositon;
    }
    public void setReqCity(String reqCity) 
    {
        this.reqCity = reqCity;
    }

    public String getReqCity() 
    {
        return reqCity;
    }
    public void setReqIndustry(String reqIndustry) 
    {
        this.reqIndustry = reqIndustry;
    }

    public String getReqIndustry() 
    {
        return reqIndustry;
    }
    public void setReqSalary(Long reqSalary)
    {
        this.reqSalary = reqSalary;
    }

    public Long getReqSalary()
    {
        return reqSalary;
    }
    public void setSelfDesc(String selfDesc) 
    {
        this.selfDesc = selfDesc;
    }

    public String getSelfDesc() 
    {
        return selfDesc;
    }
    public void setProDesc(String proDesc) 
    {
        this.proDesc = proDesc;
    }

    public String getProDesc() 
    {
        return proDesc;
    }
    public void setCvDesc(String cvDesc) 
    {
        this.cvDesc = cvDesc;
    }

    public String getCvDesc() 
    {
        return cvDesc;
    }
    public void setCvUrl(String cvUrl) 
    {
        this.cvUrl = cvUrl;
    }

    public String getCvUrl() 
    {
        return cvUrl;
    }
    public void setWorkHistory(String workHistory)
    {
        this.workHistory = workHistory;
    }

    public String getWorkHistory()
    {
        return workHistory;
    }
    public void setBack1(String back1) 
    {
        this.back1 = back1;
    }

    public String getBack1() 
    {
        return back1;
    }
    public void setBack2(String back2) 
    {
        this.back2 = back2;
    }

    public String getBack2() 
    {
        return back2;
    }
    public void setBack3(String back3) 
    {
        this.back3 = back3;
    }

    public String getBack3() 
    {
        return back3;
    }
    public void setBack4(String back4) 
    {
        this.back4 = back4;
    }

    public String getBack4() 
    {
        return back4;
    }
    public void setBack5(String back5) 
    {
        this.back5 = back5;
    }

    public String getBack5() 
    {
        return back5;
    }
    public void setComFlag(Integer comFlag)
    {
        this.comFlag = comFlag;
    }

    public Integer getComFlag()
    {
        return comFlag;
    }

    public Integer getAgeS() {
        return ageS;
    }

    public void setAgeS(Integer ageS) {
        this.ageS = ageS;
    }
    public Integer getAgeE() {
        return ageE;
    }

    public void setAgeE(Integer ageE) {
        this.ageE = ageE;
    }

    public Integer getWorkAgeS() {
        return workAgeS;
    }

    public void setWorkAgeS(Integer workAgeS) {
        this.workAgeS = workAgeS;
    }

    public Integer getWorkAgeE() {
        return workAgeE;
    }

    public void setWorkAgeE(Integer workAgeE) {
        this.workAgeE = workAgeE;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("del", getDel())
            .append("status", getStatus())
            .append("userId", getUserId())
            .append("cvType", getCvType())
            .append("cvNo", getCvNo())
            .append("userName", getUserName())
            .append("name", getName())
            .append("photoUrl", getPhotoUrl())
            .append("sex", getSex())
            .append("age", getAge())
                .append("ageS", getAgeS())
                .append("ageE", getAgeE())
            .append("workAge", getWorkAge())
                .append("workAgeS", getWorkAgeS())
                .append("workAgeE", getWorkAgeE())
            .append("birthday", getBirthday())
            .append("workStatus", getWorkStatus())
            .append("school", getSchool())
            .append("qualifications", getQualifications())
            .append("speciality", getSpeciality())
            .append("graduateDate", getGraduateDate())
            .append("reqPositon", getReqPositon())
            .append("reqCity", getReqCity())
            .append("reqIndustry", getReqIndustry())
            .append("reqSalary", getReqSalary())
            .append("selfDesc", getSelfDesc())
            .append("proDesc", getProDesc())
            .append("cvDesc", getCvDesc())
            .append("cvUrl", getCvUrl())
                .append("workHistory", getWorkHistory())
                .append("remark", getRemark())
            .append("back1", getBack1())
            .append("back2", getBack2())
            .append("back3", getBack3())
            .append("back4", getBack4())
            .append("back5", getBack5())
            .append("comFlag", getComFlag())
            .toString();
    }
}
