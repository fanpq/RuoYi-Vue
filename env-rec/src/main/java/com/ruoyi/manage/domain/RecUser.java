package com.ruoyi.manage.domain;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 用户基本对象 rec_user
 * 
 * @author fanpq
 * @date 2020-12-03
 */
public class RecUser extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 删除标志默认0，删除标志：1 */
    @Excel(name = "删除标志 默认0，删除标志：1")
    private Integer del;

    /** 状态注册即为有效 */
    @Excel(name = "状态 注册即为有效")
    private Integer status;

    /** 用户ID来源于系统用户表ID */
    @Excel(name = "用户ID 来源于系统用户表ID")
    private Long userId;

    /** 用户类型0：招聘企事业；1：求职者；2：专家人才； */
    @Excel(name = "用户类型 0：招聘企事业；1：求职者；2：专家人才；")
    private Integer userType;

    /** 用户名称类型为0时，则手工输入类型为1or2时，则为姓名 */
    @Excel(name = "用户名称 类型为0时，则手工输入 类型为1or2时，则为姓名")
    private String userName;

    /** 性别类型为1or2时用，0：女；1：男 */
    @Excel(name = "性别 类型为1or2时用，0：女；1：男")
    private Integer sex;

    /** 职位类型为0时用 */
    @Excel(name = "职位 类型为0时用")
    private String positionName;

    /** 从事行业类型为2时用 */
    @Excel(name = "从事行业 类型为2时用")
    private String industry;

    /** 所属企业ID类型为0时，如发布招聘信息，则自动会写 */
    @Excel(name = "所属企业ID 类型为0时，如发布招聘信息，则自动会写")
    private Long comId;

    /** 企事业单位名称类型为0时，如发布招聘信息，则自动会写类型为2时，则手工输入 */
    @Excel(name = "企事业单位名称 类型为0时，如发布招聘信息，则自动会写 类型为2时，则手工输入")
    private String comName;

    /** 邮箱类型为0时用 */
    @Excel(name = "邮箱 类型为0时用")
    private String email;

    /** 求职身份类型为1时用，0：在校生-在校/应届；1：无经验-往届；2：职场人-正式工作经历 */
    @Excel(name = "求职身份 类型为1时用，0：在校生-在校/应届；1：无经验-往届；2：职场人-正式工作经历")
    private Integer userIdentity;

    /** 出生日期类型为1时用 */
    @Excel(name = "出生日期 类型为1时用", width = 30, dateFormat = "yyyy-MM-dd")
    private Date birthday;

    /** 参加工作年月类型为1时用 */
    @Excel(name = "参加工作年月 类型为1时用", width = 30, dateFormat = "yyyy-MM-dd")
    private Date firstWorkDate;

    /** 当前求职状态类型为1时用，0：在校-实习；1：在校-找工作；2：在职-考虑机会；3：在职-月内到岗；5：在职-暂不考虑；6：离职-立即到岗 */
    @Excel(name = "当前求职状态 类型为1时用，0：在校-实习；1：在校-找工作；2：在职-考虑机会；3：在职-月内到岗；5：在职-暂不考虑；6：离职-立即到岗")
    private Integer workStatus;

    /** 备用1 */
    @Excel(name = "备用1")
    private String back1;

    /** 备用2 */
    @Excel(name = "备用2")
    private String back2;

    /** 备用3 */
    @Excel(name = "备用3")
    private String back3;

    /** 备用4 */
    @Excel(name = "备用4")
    private String back4;

    /** 备用5 */
    @Excel(name = "备用5")
    private String back5;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setDel(Integer del) 
    {
        this.del = del;
    }

    public Integer getDel() 
    {
        return del;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setUserType(Integer userType) 
    {
        this.userType = userType;
    }

    public Integer getUserType() 
    {
        return userType;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setSex(Integer sex) 
    {
        this.sex = sex;
    }

    public Integer getSex() 
    {
        return sex;
    }
    public void setPositionName(String positionName) 
    {
        this.positionName = positionName;
    }

    public String getPositionName() 
    {
        return positionName;
    }
    public void setIndustry(String industry) 
    {
        this.industry = industry;
    }

    public String getIndustry() 
    {
        return industry;
    }
    public void setComId(Long comId) 
    {
        this.comId = comId;
    }

    public Long getComId() 
    {
        return comId;
    }
    public void setComName(String comName) 
    {
        this.comName = comName;
    }

    public String getComName() 
    {
        return comName;
    }
    public void setEmail(String email) 
    {
        this.email = email;
    }

    public String getEmail() 
    {
        return email;
    }
    public void setUserIdentity(Integer userIdentity) 
    {
        this.userIdentity = userIdentity;
    }

    public Integer getUserIdentity() 
    {
        return userIdentity;
    }
    public void setBirthday(Date birthday) 
    {
        this.birthday = birthday;
    }

    public Date getBirthday() 
    {
        return birthday;
    }
    public void setFirstWorkDate(Date firstWorkDate) 
    {
        this.firstWorkDate = firstWorkDate;
    }

    public Date getFirstWorkDate() 
    {
        return firstWorkDate;
    }
    public void setWorkStatus(Integer workStatus) 
    {
        this.workStatus = workStatus;
    }

    public Integer getWorkStatus() 
    {
        return workStatus;
    }
    public void setBack1(String back1) 
    {
        this.back1 = back1;
    }

    public String getBack1() 
    {
        return back1;
    }
    public void setBack2(String back2) 
    {
        this.back2 = back2;
    }

    public String getBack2() 
    {
        return back2;
    }
    public void setBack3(String back3) 
    {
        this.back3 = back3;
    }

    public String getBack3() 
    {
        return back3;
    }
    public void setBack4(String back4) 
    {
        this.back4 = back4;
    }

    public String getBack4() 
    {
        return back4;
    }
    public void setBack5(String back5) 
    {
        this.back5 = back5;
    }

    public String getBack5() 
    {
        return back5;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("del", getDel())
            .append("status", getStatus())
            .append("userId", getUserId())
            .append("userType", getUserType())
            .append("userName", getUserName())
            .append("sex", getSex())
            .append("positionName", getPositionName())
            .append("industry", getIndustry())
            .append("comId", getComId())
            .append("comName", getComName())
            .append("email", getEmail())
            .append("userIdentity", getUserIdentity())
            .append("birthday", getBirthday())
            .append("firstWorkDate", getFirstWorkDate())
            .append("workStatus", getWorkStatus())
            .append("remark", getRemark())
            .append("back1", getBack1())
            .append("back2", getBack2())
            .append("back3", getBack3())
            .append("back4", getBack4())
            .append("back5", getBack5())
            .toString();
    }
}
