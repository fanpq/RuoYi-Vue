package com.ruoyi.manage.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 操作日志记录对象 rec_log
 * 
 * @author fanpq
 * @date 2020-12-03
 */
public class RecLog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 删除标志默认0，删除标志：1 */
    @Excel(name = "删除标志默认0，删除标志：1")
    private Integer del;

    /** 状态0：编辑；1：待审核；2：发布：3：取消发布； */
    @Excel(name = "状态0：编辑；1：待审核；2：发布：3：取消发布；")
    private Integer status;

    /** 日志记录对象ID */
    @Excel(name = "日志记录对象ID")
    private Long logObjId;

    /** 日志记录类型，0:浏览;1、查看;2、简历收藏;3、简历投递;4、公司收藏 */
    @Excel(name = "日志记录类型，0:浏览;1、查看;2、简历收藏;3、简历投递;4、公司收藏")
    private Integer logType;

    /** 操作人id */
    @Excel(name = "操作人id")
    private Long logUserId;

    /** 操作人姓名 */
    @Excel(name = "操作人姓名")
    private String logUserName;

    /** 日志摘要 */
    @Excel(name = "日志摘要")
    private String logAbstract;

    /** 自定义对象1 */
    @Excel(name = "自定义对象1")
    private String obj1;

    /** 自定义对象2 */
    @Excel(name = "自定义对象2")
    private String obj2;

    /** 自定义对象3 */
    @Excel(name = "自定义对象3")
    private String obj3;

    /** 自定义对象4 */
    @Excel(name = "自定义对象4")
    private String obj4;

    /** 自定义对象5 */
    @Excel(name = "自定义对象5")
    private String obj5;

    /** 自定义json */
    @Excel(name = "自定义json")
    private String text;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setDel(Integer del) 
    {
        this.del = del;
    }

    public Integer getDel() 
    {
        return del;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setLogObjId(Long logObjId) 
    {
        this.logObjId = logObjId;
    }

    public Long getLogObjId() 
    {
        return logObjId;
    }
    public void setLogType(Integer logType) 
    {
        this.logType = logType;
    }

    public Integer getLogType() 
    {
        return logType;
    }
    public void setLogUserId(Long logUserId) 
    {
        this.logUserId = logUserId;
    }

    public Long getLogUserId() 
    {
        return logUserId;
    }
    public void setLogUserName(String logUserName) 
    {
        this.logUserName = logUserName;
    }

    public String getLogUserName() 
    {
        return logUserName;
    }
    public void setLogAbstract(String logAbstract) 
    {
        this.logAbstract = logAbstract;
    }

    public String getLogAbstract() 
    {
        return logAbstract;
    }
    public void setObj1(String obj1) 
    {
        this.obj1 = obj1;
    }

    public String getObj1() 
    {
        return obj1;
    }
    public void setObj2(String obj2) 
    {
        this.obj2 = obj2;
    }

    public String getObj2() 
    {
        return obj2;
    }
    public void setObj3(String obj3) 
    {
        this.obj3 = obj3;
    }

    public String getObj3() 
    {
        return obj3;
    }
    public void setObj4(String obj4) 
    {
        this.obj4 = obj4;
    }

    public String getObj4() 
    {
        return obj4;
    }
    public void setObj5(String obj5) 
    {
        this.obj5 = obj5;
    }

    public String getObj5() 
    {
        return obj5;
    }
    public void setText(String text) 
    {
        this.text = text;
    }

    public String getText() 
    {
        return text;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("del", getDel())
            .append("status", getStatus())
            .append("logObjId", getLogObjId())
            .append("logType", getLogType())
            .append("logUserId", getLogUserId())
            .append("logUserName", getLogUserName())
            .append("logAbstract", getLogAbstract())
            .append("obj1", getObj1())
            .append("obj2", getObj2())
            .append("obj3", getObj3())
            .append("obj4", getObj4())
            .append("obj5", getObj5())
            .append("remark", getRemark())
            .append("text", getText())
            .toString();
    }
}
