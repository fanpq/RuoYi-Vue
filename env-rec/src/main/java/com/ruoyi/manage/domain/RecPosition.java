package com.ruoyi.manage.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 职位信息对象 rec_position
 * 
 * @author fanpq
 * @date 2020-12-03
 */
public class RecPosition extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 删除标志默认0，删除标志：1 */
    @Excel(name = "删除标志 默认0，删除标志：1")
    private Integer del;

    /** 状态0：编辑；1：待审核；2：发布：3：取消发布； */
    @Excel(name = "状态 0：编辑；1：待审核；2：发布：3：取消发布；")
    private Integer status;

    /** 所属企业 */
    @Excel(name = "所属企业")
    private Long comId;

    /** 所属企业名称 */
    @Excel(name = "所属企业名称")
    private String comName;

    /** 招聘类型0:应届校招；1:实习生；2：社招：3:兼职 */
    @Excel(name = "招聘类型 0:应届校招；1:实习生；2：社招：3:兼职")
    private Integer recruitType;

    /** 职位名称 */
    @Excel(name = "职位名称")
    private String positionName;

    /** 工作城市 */
    @Excel(name = "工作城市")
    private String workCity;

    /** 工作地点 */
    @Excel(name = "工作地点")
    private String workAdr;

    /** 经验要求0：不限；1：1-3年；2：3-5年：3：5-10年；4：10年以上 */
    @Excel(name = "经验要求 0：不限；1：1-3年；2：3-5年：3：5-10年；4：10年以上")
    private Integer expReq;

    /** 学历要求0：不限；1:中专及以上；2：大专及以上：3：本科及以上；4：硕士及以上；5：博士及以上 */
    @Excel(name = "学历要求 0：不限；1:中专及以上；2：大专及以上：3：本科及以上；4：硕士及以上；5：博士及以上")
    private Integer eduReq;

    /** 月薪资范围:0:面议；1：5k以下；2：5-8k；3：8-12k；4：12-18k；5：18-25k；6：25k以上 */
    @Excel(name = "月薪资范围:0:面议；1：5k以下；2：5-8k；3：8-12k；4：12-18k；5：18-25k；6：25k以上")
    private Integer salaryReq;

    /** 月薪资范围起K */
    @Excel(name = "月薪资范围起K")
    private Long salaryBegin;

    /** 月薪资范围止K */
    @Excel(name = "月薪资范围止K")
    private Long salaryEnd;

    /** 岗位名称 */
    @Excel(name = "岗位名称")
    private String postName;

    /** 岗位关键词 */
    @Excel(name = "岗位关键词")
    private String postKey;

    /** 岗位描述 */
    @Excel(name = "岗位描述")
    private String postDesc;

    /** 备用1 */
    @Excel(name = "备用1")
    private String back1;

    /** 备用2 */
    @Excel(name = "备用2")
    private String back2;

    /** 备用3 */
    @Excel(name = "备用3")
    private String back3;

    /** 备用4 */
    @Excel(name = "备用4")
    private String back4;

    /** 备用5 */
    @Excel(name = "备用5")
    private String back5;

    // 2021-01-03 fanpq 增加联查企业的一些字段
    private Integer comNumPeople;
    private Long comRegCapital;
    private String industry;
    private String comAdr;
    private String comPhotoUrl;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setDel(Integer del) 
    {
        this.del = del;
    }

    public Integer getDel() 
    {
        return del;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setComId(Long comId) 
    {
        this.comId = comId;
    }

    public Long getComId() 
    {
        return comId;
    }
    public void setComName(String comName) 
    {
        this.comName = comName;
    }

    public String getComName() 
    {
        return comName;
    }
    public void setRecruitType(Integer recruitType) 
    {
        this.recruitType = recruitType;
    }

    public Integer getRecruitType() 
    {
        return recruitType;
    }
    public void setPositionName(String positionName) 
    {
        this.positionName = positionName;
    }

    public String getPositionName() 
    {
        return positionName;
    }
    public void setWorkCity(String workCity) 
    {
        this.workCity = workCity;
    }

    public String getWorkCity() 
    {
        return workCity;
    }
    public void setWorkAdr(String workAdr) 
    {
        this.workAdr = workAdr;
    }

    public String getWorkAdr() 
    {
        return workAdr;
    }
    public void setExpReq(Integer expReq) 
    {
        this.expReq = expReq;
    }

    public Integer getExpReq() 
    {
        return expReq;
    }
    public void setEduReq(Integer eduReq) 
    {
        this.eduReq = eduReq;
    }

    public Integer getEduReq() 
    {
        return eduReq;
    }
    public void setSalaryReq(Integer salaryReq)
    {
        this.salaryReq = salaryReq;
    }

    public Integer getSalaryReq()
    {
        return salaryReq;
    }
    public void setSalaryBegin(Long salaryBegin) 
    {
        this.salaryBegin = salaryBegin;
    }

    public Long getSalaryBegin() 
    {
        return salaryBegin;
    }
    public void setSalaryEnd(Long salaryEnd) 
    {
        this.salaryEnd = salaryEnd;
    }

    public Long getSalaryEnd() 
    {
        return salaryEnd;
    }
    public void setPostName(String postName) 
    {
        this.postName = postName;
    }

    public String getPostName() 
    {
        return postName;
    }
    public void setPostKey(String postKey) 
    {
        this.postKey = postKey;
    }

    public String getPostKey() 
    {
        return postKey;
    }
    public void setPostDesc(String postDesc) 
    {
        this.postDesc = postDesc;
    }

    public String getPostDesc() 
    {
        return postDesc;
    }
    public void setBack1(String back1) 
    {
        this.back1 = back1;
    }

    public String getBack1() 
    {
        return back1;
    }
    public void setBack2(String back2) 
    {
        this.back2 = back2;
    }

    public String getBack2() 
    {
        return back2;
    }
    public void setBack3(String back3) 
    {
        this.back3 = back3;
    }

    public String getBack3() 
    {
        return back3;
    }
    public void setBack4(String back4) 
    {
        this.back4 = back4;
    }

    public String getBack4() 
    {
        return back4;
    }
    public void setBack5(String back5) 
    {
        this.back5 = back5;
    }

    public String getBack5() 
    {
        return back5;
    }

    public Integer getComNumPeople() {
        return comNumPeople;
    }

    public void setComNumPeople(Integer comNumPeople) {
        this.comNumPeople = comNumPeople;
    }

    public Long getComRegCapital() {
        return comRegCapital;
    }

    public void setComRegCapital(Long comRegCapital) {
        this.comRegCapital = comRegCapital;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getComAdr() {
        return comAdr;
    }

    public void setComAdr(String comAdr) {
        this.comAdr = comAdr;
    }

    public String getComPhotoUrl() {
        return comPhotoUrl;
    }

    public void setComPhotoUrl(String comPhotoUrl) {
        this.comPhotoUrl = comPhotoUrl;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("del", getDel())
            .append("status", getStatus())
            .append("comId", getComId())
            .append("comName", getComName())
            .append("recruitType", getRecruitType())
            .append("positionName", getPositionName())
            .append("workCity", getWorkCity())
            .append("workAdr", getWorkAdr())
            .append("expReq", getExpReq())
            .append("eduReq", getEduReq())
            .append("salaryReq", getSalaryReq())
            .append("salaryBegin", getSalaryBegin())
            .append("salaryEnd", getSalaryEnd())
            .append("postName", getPostName())
            .append("postKey", getPostKey())
            .append("postDesc", getPostDesc())
            .append("remark", getRemark())
            .append("back1", getBack1())
            .append("back2", getBack2())
            .append("back3", getBack3())
            .append("back4", getBack4())
            .append("back5", getBack5())
                .append("comPhotoUrl", getComPhotoUrl())
                .append("comAdr", getComAdr())
                .append("industry", getIndustry())
                .append("comNumPeople", getComNumPeople())
                .append("comRegCapital", getComRegCapital())
            .toString();
    }
}
