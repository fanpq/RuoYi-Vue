package com.ruoyi.manage.domain;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 企业信息对象 rec_company
 * 
 * @author fanpq
 * @date 2020-12-03
 */
public class RecCompany extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 删除标志默认0，删除标志：1 */
    @Excel(name = "删除标志 默认0，删除标志：1")
    private Integer del;

    /** 状态0：待审核；1：有效；2：无效： */
    @Excel(name = "状态 0：待审核；1：有效；2：无效：")
    private Integer status;

    /** 企业名称 */
    @Excel(name = "企业名称")
    private String comName;

    /** 关联用户ID，来源于招聘用户表ID */
    @Excel(name = "关联用户ID，来源于招聘用户表ID")
    private Long userId;

    /** 关联用户名称，来源于招聘用户表 */
    @Excel(name = "关联用户名称，来源于招聘用户表")
    private String userName;

    /** 企业照片 */
    @Excel(name = "企业照片")
    private String comPhotoUrl;

    /** 企业地址 */
    @Excel(name = "企业地址")
    private String comAdr;

    /** 地址经度 */
    @Excel(name = "地址经度")
    private String comLong;

    /** 地址纬度 */
    @Excel(name = "地址纬度")
    private String comLat;

    /** 企业简称 */
    @Excel(name = "企业简称")
    private String comShortName;

    /** 所属行业 */
    @Excel(name = "所属行业")
    private String industry;

    /** 公司人数0:20以下；1、20-150人；2：150-500；3:500-999；4:1000以上 */
    @Excel(name = "公司人数 0:20以下；1、20-150人；2：150-500；3:500-999；4:1000以上")
    private Integer comNumPeople;

    /** 法人代表 */
    @Excel(name = "法人代表")
    private String comPeople;

    /** 注册资本万元 */
    @Excel(name = "注册资本 万元")
    private Long comRegCapital;

    /** 成立时间 */
    @Excel(name = "成立时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date comRegDate;

    /** 公司简介 */
    @Excel(name = "公司简介")
    private String comDesc;

    /** 备用1 */
    @Excel(name = "备用1")
    private String back1;

    /** 备用2 */
    @Excel(name = "备用2")
    private String back2;

    /** 备用3 */
    @Excel(name = "备用3")
    private String back3;

    /** 备用4 */
    @Excel(name = "备用4")
    private String back4;

    /** 备用5 */
    @Excel(name = "备用5")
    private String back5;

    /** 收藏标志,0:未收藏；1：已收藏 */
    private Integer collectFlag;

    /** 投递标志，0：未投递；1：已投递 */
    private Integer deliverFlag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setDel(Integer del) 
    {
        this.del = del;
    }

    public Integer getDel() 
    {
        return del;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setComName(String comName) 
    {
        this.comName = comName;
    }

    public String getComName() 
    {
        return comName;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setComPhotoUrl(String comPhotoUrl) 
    {
        this.comPhotoUrl = comPhotoUrl;
    }

    public String getComPhotoUrl() 
    {
        return comPhotoUrl;
    }
    public void setComAdr(String comAdr) 
    {
        this.comAdr = comAdr;
    }

    public String getComAdr() 
    {
        return comAdr;
    }
    public void setComLong(String comLong) 
    {
        this.comLong = comLong;
    }

    public String getComLong() 
    {
        return comLong;
    }
    public void setComLat(String comLat) 
    {
        this.comLat = comLat;
    }

    public String getComLat() 
    {
        return comLat;
    }
    public void setComShortName(String comShortName) 
    {
        this.comShortName = comShortName;
    }

    public String getComShortName() 
    {
        return comShortName;
    }
    public void setIndustry(String industry) 
    {
        this.industry = industry;
    }

    public String getIndustry() 
    {
        return industry;
    }
    public void setComNumPeople(Integer comNumPeople) 
    {
        this.comNumPeople = comNumPeople;
    }

    public Integer getComNumPeople() 
    {
        return comNumPeople;
    }
    public void setComPeople(String comPeople) 
    {
        this.comPeople = comPeople;
    }

    public String getComPeople() 
    {
        return comPeople;
    }
    public void setComRegCapital(Long comRegCapital) 
    {
        this.comRegCapital = comRegCapital;
    }

    public Long getComRegCapital() 
    {
        return comRegCapital;
    }
    public void setComRegDate(Date comRegDate) 
    {
        this.comRegDate = comRegDate;
    }

    public Date getComRegDate() 
    {
        return comRegDate;
    }
    public void setComDesc(String comDesc) 
    {
        this.comDesc = comDesc;
    }

    public String getComDesc() 
    {
        return comDesc;
    }
    public void setBack1(String back1) 
    {
        this.back1 = back1;
    }

    public String getBack1() 
    {
        return back1;
    }
    public void setBack2(String back2) 
    {
        this.back2 = back2;
    }

    public String getBack2() 
    {
        return back2;
    }
    public void setBack3(String back3) 
    {
        this.back3 = back3;
    }

    public String getBack3() 
    {
        return back3;
    }
    public void setBack4(String back4) 
    {
        this.back4 = back4;
    }

    public String getBack4() 
    {
        return back4;
    }
    public void setBack5(String back5) 
    {
        this.back5 = back5;
    }

    public String getBack5() 
    {
        return back5;
    }

    public void setCollectFlag(Integer collectFlag)
    {
        this.collectFlag = collectFlag;
    }

    public Integer getCollectFlag()
    {
        return collectFlag;
    }

    public void setDeliverFlag(Integer deliverFlag)
    {
        this.deliverFlag = deliverFlag;
    }

    public Integer getDeliverFlag()
    {
        return deliverFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("del", getDel())
            .append("status", getStatus())
            .append("comName", getComName())
            .append("userId", getUserId())
            .append("userName", getUserName())
            .append("comPhotoUrl", getComPhotoUrl())
            .append("comAdr", getComAdr())
            .append("comLong", getComLong())
            .append("comLat", getComLat())
            .append("comShortName", getComShortName())
            .append("industry", getIndustry())
            .append("comNumPeople", getComNumPeople())
            .append("comPeople", getComPeople())
            .append("comRegCapital", getComRegCapital())
            .append("comRegDate", getComRegDate())
            .append("comDesc", getComDesc())
            .append("back1", getBack1())
            .append("back2", getBack2())
            .append("remark", getRemark())
            .append("back3", getBack3())
            .append("back4", getBack4())
            .append("back5", getBack5())
            .append("deliverFlag", getDeliverFlag())
            .append("collectFlag", getCollectFlag())
            .toString();
    }
}
