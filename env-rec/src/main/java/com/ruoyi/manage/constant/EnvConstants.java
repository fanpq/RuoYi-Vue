package com.ruoyi.manage.constant;

/**
 * 通用常量信息
 *
 * @author fanpq
 */
public class EnvConstants
{
    /**
     * 企业用户对简历的操作
     * 操作包括8:已收藏；7:取消收藏；0：待沟通（联系）；1：已入职；2：不合适；
     * 求职者对简历的操作
     * 操作包括：投递
     */


    /**
     * 求职者对简历的操作，6:投递
     */
    public static final Integer DELIVER= new Integer(6);

    /**
     * 企业用户对求职者或简历的操作，8:已收藏
     */
    public static final Integer COLLECT= new Integer(8);

    /**
     * 企业用户对求职者或简历的操作，7:取消收藏
     */
    public static final Integer COLLECTCANCEL= new Integer(7);

    /**
     * 企业用户对求职者或简历的操作，0：待沟通(联系)
     */
    public static final Integer CONNECT= new Integer(0);

    /**
     * 企业用户对求职者或简历的操作，1：已入职
     */
    public static final Integer ENTRY= new Integer(1);

    /**
     * 企业用户对求职者或简历的操作，2：不合适，剔除
     */
    public static final Integer IMPROPER= new Integer(2);


    /**
     * 简历状态，状态0：编辑；1：待审核；2：发布：3：取消发布；
     */
    public static final Integer CVEDIT= new Integer(0);
    public static final Integer CVWAITCONFIRM= new Integer(1);
    public static final Integer CVCONFIRM= new Integer(2);
    public static final Integer CVCONFIRMCANCEL= new Integer(3);


    /**
     * 公司状态，状态0：待审核；1：有效；2：无效：
     */
    public static final Integer COMEDIT= new Integer(0);
    public static final Integer COMCONFIRM= new Integer(1);
    public static final Integer COMCONFIRMCANCEL= new Integer(2);

    /**
     * 职位状态，状态0：编辑；1：待审核；2：发布：3：取消发布；
     */
    public static final Integer POSEDIT= new Integer(0);
    public static final Integer POSWAITCONFIRM= new Integer(1);
    public static final Integer POSCONFIRM= new Integer(2);
    public static final Integer POSCONFIRMCANCEL= new Integer(3);

}