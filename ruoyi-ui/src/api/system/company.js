import request from '@/utils/request'

// 查询企业信息列表
export function listCompany(query) {
  return request({
    url: '/env/company/list',
    method: 'get',
    params: query
  })
}

// 查询企业信息详细
export function getCompany(id) {
  return request({
    url: '/env/company/' + id,
    method: 'get'
  })
}

// 新增企业信息
export function addCompany(data) {
  return request({
    url: '/env/company',
    method: 'post',
    data: data
  })
}

// 修改企业信息
export function updateCompany(data) {
  return request({
    url: '/env/company',
    method: 'put',
    data: data
  })
}

// 删除企业信息
export function delCompany(id) {
  return request({
    url: '/env/company/' + id,
    method: 'delete'
  })
}

// 审核信息
export function confirmCom(id) {
  return request({
    url: '/env/company/confirmRecCompanys?ids=' + id,
    method: 'post'
  })
}
// 取消审核信息
export function confirmCancelCom(id) {
  return request({
    url: '/env/company/confirmCancelRecCompanys?ids=' + id,
    method: 'post'
  })
}

// 导出企业信息
export function exportCompany(query) {
  return request({
    url: '/env/company/export',
    method: 'get',
    params: query
  })
}
