import request from '@/utils/request'

// 查询职位信息列表
export function listPosition(query) {
  return request({
    url: '/env/position/list',
    method: 'get',
    params: query
  })
}

// 查询职位信息详细
export function getPosition(id) {
  return request({
    url: '/env/position/' + id,
    method: 'get'
  })
}

// 新增职位信息
export function addPosition(data) {
  return request({
    url: '/env/position',
    method: 'post',
    data: data
  })
}

// 修改职位信息
export function updatePosition(data) {
  return request({
    url: '/env/position',
    method: 'put',
    data: data
  })
}

// 删除职位信息
export function delPosition(id) {
  return request({
    url: '/env/position/' + id,
    method: 'delete'
  })
}

// 审核信息
export function confirmPos(id) {
  return request({
    url: '/env/position/confirmRecPositions?ids=' + id,
    method: 'post'
  })
}
// 取消审核信息
export function confirmCancelPos(id) {
  return request({
    url: '/env/position/confirmCancelRecPositions?ids=' + id,
    method: 'post'
  })
}
// 导出职位信息
export function exportPosition(query) {
  return request({
    url: '/env/position/export',
    method: 'get',
    params: query
  })
}
