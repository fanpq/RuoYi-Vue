import request from '@/utils/request'

// 查询内容文章列表
export function listContent(query) {
  return request({
    url: '/system/content/list',
    method: 'get',
    params: query
  })
}

// 查询内容文章详细
export function getContent(id) {
  return request({
    url: '/system/content/' + id,
    method: 'get'
  })
}

// 新增内容文章
export function addContent(data) {
  return request({
    url: '/system/content',
    method: 'post',
    data: data
  })
}

// 修改内容文章
export function updateContent(data) {
  return request({
    url: '/system/content',
    method: 'put',
    data: data
  })
}

// 删除内容文章
export function delContent(id) {
  return request({
    url: '/system/content/' + id,
    method: 'delete'
  })
}

// 导出内容文章
export function exportContent(query) {
  return request({
    url: '/system/content/export',
    method: 'get',
    params: query
  })
}


// 缩略图上传
export function uploadImgPath() {
  return request({
    url: '/system/content/uploadImg',
    method: 'post',
    data: data
  })
}
