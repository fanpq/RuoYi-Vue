import request from '@/utils/request'

// 查询简历信息列表
export function listCv(query) {
  return request({
    url: '/env/cv/list',
    method: 'get',
    params: query
  })
}

// 查询简历信息详细
export function getCv(id) {
  return request({
    url: '/env/cv/' + id,
    method: 'get'
  })
}

// 新增简历信息
export function addCv(data) {
  return request({
    url: '/env/cv',
    method: 'post',
    data: data
  })
}

// 修改简历信息
export function updateCv(data) {
  return request({
    url: '/env/cv',
    method: 'put',
    data: data
  })
}

// 审核简历信息
export function confirmCv(id) {
  return request({
    url: '/env/cv/confirmRecCvs?ids=' + id,
    method: 'post'
  })
}
// 取消审核简历信息
export function confirmCancelCv(id) {
  return request({
    url: '/env/cv/confirmCancelRecCvs?ids=' + id,
    method: 'post'
  })
}

// 删除简历信息
export function delCv(id) {
  return request({
    url: '/env/cv/' + id,
    method: 'delete'
  })
}

// 导出简历信息
export function exportCv(query) {
  return request({
    url: '/env/cv/export',
    method: 'get',
    params: query
  })
}
