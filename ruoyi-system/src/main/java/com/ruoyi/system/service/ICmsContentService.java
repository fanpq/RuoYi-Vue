package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.CmsContent;

/**
 * 内容文章Service接口
 * 
 * @author fanpq
 * @date 2020-12-20
 */
public interface ICmsContentService 
{
    /**
     * 查询内容文章
     * 
     * @param id 内容文章ID
     * @return 内容文章
     */
    public CmsContent selectCmsContentById(Long id);

    /**
     * 查询内容文章列表
     * 
     * @param cmsContent 内容文章
     * @return 内容文章集合
     */
    public List<CmsContent> selectCmsContentList(CmsContent cmsContent);

    /**
     * 查询内容文章列表
     *
     * @param categoryIds 栏目分类ids
     * @return 内容文章
     */
    public List<CmsContent> selectCmsContentByCategoryIds(List<Long> categoryIds);

    /**
     * 新增内容文章
     * 
     * @param cmsContent 内容文章
     * @return 结果
     */
    public int insertCmsContent(CmsContent cmsContent);

    /**
     * 修改内容文章
     * 
     * @param cmsContent 内容文章
     * @return 结果
     */
    public int updateCmsContent(CmsContent cmsContent);


    /**
     * 批量删除内容文章
     * 
     * @param ids 需要删除的内容文章ID
     * @return 结果
     */
    public int deleteCmsContentByIds(Long[] ids);

    /**
     * 删除内容文章信息
     * 
     * @param id 内容文章ID
     * @return 结果
     */
    public int deleteCmsContentById(Long id);
}
