package com.ruoyi.system.service;

import java.util.List;

import com.ruoyi.system.domain.CmsCategory;
import com.ruoyi.system.domain.vo.TreeSelectEnv;

/**
 * 栏目分类Service接口
 * 
 * @author fanpq
 * @date 2020-12-20
 */
public interface ICmsCategoryService 
{
    /**
     * 查询栏目分类
     * 
     * @param id 栏目分类ID
     * @return 栏目分类
     */
    public CmsCategory selectCmsCategoryById(Long id);

    /**
     * 查询栏目分类列表
     * 
     * @param cmsCategory 栏目分类
     * @return 栏目分类集合
     */
    public List<CmsCategory> selectCmsCategoryList(CmsCategory cmsCategory);

    /**
     * 获取栏目分类指定分类下的下拉树列表
     *
     * @param id 指定栏目分类ID
     * @return 栏目分类集合
     */
    public List<CmsCategory> selectCmsCategoryByLevel(String id);

    /**
     * 查询栏目分类列表
     *
     * @param cmsCategory 栏目分类
     * @return 栏目分类集合
     */
    public List<TreeSelectEnv> buildCateTreeSelect(List<CmsCategory> cmsCategory);

    /**
     * 查询栏目分类列表
     *
     * @param cmsCategory 栏目分类
     * @return 栏目分类集合
     */
    public List<CmsCategory> buildCateTree(List<CmsCategory> cmsCategory);

    /**
     * 新增栏目分类
     * 
     * @param cmsCategory 栏目分类
     * @return 结果
     */
    public int insertCmsCategory(CmsCategory cmsCategory);

    /**
     * 修改栏目分类
     * 
     * @param cmsCategory 栏目分类
     * @return 结果
     */
    public int updateCmsCategory(CmsCategory cmsCategory);

    /**
     * 批量删除栏目分类
     * 
     * @param ids 需要删除的栏目分类ID
     * @return 结果
     */
    public int deleteCmsCategoryByIds(Long[] ids);

    /**
     * 删除栏目分类信息
     * 
     * @param id 栏目分类ID
     * @return 结果
     */
    public int deleteCmsCategoryById(Long id);

    /**
     * 校验栏目名称是否唯一
     *
     * @param menu 菜单信息
     * @return 结果
     */
    public String checkCateNameUnique(CmsCategory cmsCategory);
}
