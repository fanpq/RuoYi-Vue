package com.ruoyi.system.service.impl;

import java.math.BigDecimal;
import java.util.List;

import com.ruoyi.common.exception.CustomException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.CmsContentMapper;
import com.ruoyi.system.domain.CmsContent;
import com.ruoyi.system.service.ICmsContentService;

/**
 * 内容文章Service业务层处理
 * 
 * @author fanpq
 * @date 2020-12-20
 */
@Service
public class CmsContentServiceImpl implements ICmsContentService 
{
    @Autowired
    private CmsContentMapper cmsContentMapper;

    /**
     * 查询内容文章
     * 
     * @param id 内容文章ID
     * @return 内容文章
     */
    @Override
    public CmsContent selectCmsContentById(Long id)
    {
        // 记录内容文章点击次数
        CmsContent cmsContent = cmsContentMapper.selectCmsContentById(id);
        Long hitNum = cmsContent.getContentHit();
        BigDecimal bd = hitNum == null ? new BigDecimal(0):new BigDecimal(hitNum);
        bd = bd.add(new BigDecimal(1));
        cmsContent.setContentHit(bd.longValue());
        int resultNum = this.updateCmsContent(cmsContent);
        if (resultNum <= 0) {
            throw new CustomException("文章获取失败");
        }
        return cmsContent;
    }

    /**
     * 查询内容文章列表
     * 
     * @param cmsContent 内容文章
     * @return 内容文章
     */
    @Override
    public List<CmsContent> selectCmsContentList(CmsContent cmsContent)
    {
        return cmsContentMapper.selectCmsContentList(cmsContent);
    }

    /**
     * 查询内容文章列表
     *
     * @param categoryIds 栏目分类ids
     * @return 内容文章
     */
    @Override
    public List<CmsContent> selectCmsContentByCategoryIds(List<Long> categoryIds)
    {
        return cmsContentMapper.selectCmsContentByCategoryIds(categoryIds);
    }

    /**
     * 新增内容文章
     *
     * @param cmsContent 内容文章
     * @return 结果
     */
    @Override
    public int insertCmsContent(CmsContent cmsContent)
    {
        cmsContent.setCreateTime(DateUtils.getNowDate());
        cmsContent.setCreateBy(SecurityUtils.getLoginUser().getUser().getUserId().toString());
        cmsContent.setUpdateTime(DateUtils.getNowDate());
        cmsContent.setUpdateBy(SecurityUtils.getLoginUser().getUser().getUserId().toString());
        cmsContent.setContentHit(new Long(0));
        if(null == cmsContent.getStatus()) {
            cmsContent.setStatus("0");
        }
        return cmsContentMapper.insertCmsContent(cmsContent);
    }

    /**
     * 修改内容文章
     * 
     * @param cmsContent 内容文章
     * @return 结果
     */
    @Override
    public int updateCmsContent(CmsContent cmsContent)
    {
        cmsContent.setUpdateTime(DateUtils.getNowDate());
        return cmsContentMapper.updateCmsContent(cmsContent);
    }

    /**
     * 批量删除内容文章
     * 
     * @param ids 需要删除的内容文章ID
     * @return 结果
     */
    @Override
    public int deleteCmsContentByIds(Long[] ids)
    {
        return cmsContentMapper.deleteCmsContentByIds(ids);
    }

    /**
     * 删除内容文章信息
     * 
     * @param id 内容文章ID
     * @return 结果
     */
    @Override
    public int deleteCmsContentById(Long id)
    {
        return cmsContentMapper.deleteCmsContentById(id);
    }
}
