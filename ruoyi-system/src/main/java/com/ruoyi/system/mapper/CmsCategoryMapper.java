package com.ruoyi.system.mapper;

import java.util.List;

import com.ruoyi.system.domain.CmsCategory;
import org.apache.ibatis.annotations.Param;

/**
 * 栏目分类Mapper接口
 * 
 * @author fanpq
 * @date 2020-12-20
 */
public interface CmsCategoryMapper 
{
    /**
     * 查询栏目分类
     * 
     * @param id 栏目分类ID
     * @return 栏目分类
     */
    public CmsCategory selectCmsCategoryById(Long id);

    /**
     * 查询栏目分类
     *
     * @param ids 栏目分类IDs
     * @return 栏目分类
     */
    public List<CmsCategory> selectCmsCategoryByIds(Long[] ids);

    /**
     * 查询栏目分类列表
     * 
     * @param cmsCategory 栏目分类
     * @return 栏目分类集合
     */
    public List<CmsCategory> selectCmsCategoryList(CmsCategory cmsCategory);

    /**
     * 获取栏目分类指定分类下的下拉树列表
     *
     * @param id 指定栏目分类ID
     * @return 栏目分类集合
     */
    public List<CmsCategory> selectCmsCategoryByLevel(String id);

    /**
     * 新增栏目分类
     * 
     * @param cmsCategory 栏目分类
     * @return 结果
     */
    public int insertCmsCategory(CmsCategory cmsCategory);

    /**
     * 修改栏目分类
     * 
     * @param cmsCategory 栏目分类
     * @return 结果
     */
    public int updateCmsCategory(CmsCategory cmsCategory);

    /**
     * 删除栏目分类
     * 
     * @param id 栏目分类ID
     * @return 结果
     */
    public int deleteCmsCategoryById(Long id);

    /**
     * 批量删除栏目分类
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCmsCategoryByIds(Long[] ids);

    /**
     * 校验栏目名称是否唯一
     *
     * @param categoryTitle 栏目名称
     * @param categoryParentId 父栏目ID
     * @return 结果
     */
    public CmsCategory checkCateNameUnique(@Param("categoryTitle") String categoryTitle, @Param("categoryParentId") Long categoryParentId);
}
