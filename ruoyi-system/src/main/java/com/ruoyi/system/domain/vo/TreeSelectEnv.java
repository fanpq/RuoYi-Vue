package com.ruoyi.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.ruoyi.system.domain.CmsCategory;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Treeselect树结构实体类
 * 
 * @author ruoyi
 */
public class TreeSelectEnv implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 节点ID */
    private Long id;

    /** 节点名称 */
    private String label;

    /** 子节点 */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<TreeSelectEnv> children;

    public TreeSelectEnv()
    {

    }

    public TreeSelectEnv(CmsCategory cmsCategory)
    {
        this.id = cmsCategory.getId();
        this.label = cmsCategory.getCategoryTitle();
        this.children = cmsCategory.getChildren().stream().map(TreeSelectEnv::new).collect(Collectors.toList());
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getLabel()
    {
        return label;
    }

    public void setLabel(String label)
    {
        this.label = label;
    }

    public List<TreeSelectEnv> getChildren()
    {
        return children;
    }

    public void setChildren(List<TreeSelectEnv> children)
    {
        this.children = children;
    }
}
