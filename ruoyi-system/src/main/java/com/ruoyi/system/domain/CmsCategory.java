package com.ruoyi.system.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 栏目分类对象 cms_category
 * 
 * @author fanpq
 * @date 2020-12-20
 */
public class CmsCategory extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 删除标志默认:0,删除:1 */
    @Excel(name = "删除标志默认:0,删除:1")
    private Integer del;

    /** 状态0:待审核;1:发布;2:取消发布; */
    @Excel(name = "状态0:待审核;1:发布;2:取消发布;")
    private Integer status;

    /** 栏目名称 */
    @Excel(name = "栏目名称")
    private String categoryTitle;

    /** 栏目类型:0:列表;1:封面;2:链接; */
    @Excel(name = "栏目类型:0:列表;1:封面;2:链接;")
    private String categoryType;

    /** 父类ID */
    @Excel(name = "父类ID")
    private Long categoryParentId;

    /** 父类名称 */
    @Excel(name = "父类名称")
    private String categoryParentName;

    /** 缩略图 */
    @Excel(name = "缩略图")
    private String categoryImg;

    /** 栏目属性 */
    @Excel(name = "栏目属性")
    private String categoryFlag;

    /** 栏目描述 */
    @Excel(name = "栏目描述")
    private String categoryDescrip;

    /** 栏目关键字 */
    @Excel(name = "栏目关键字")
    private String categoryKeyword;

    /** 栏目路径 */
    @Excel(name = "栏目路径")
    private String categoryPath;

    /** 字典对应编号 */
    @Excel(name = "字典对应编号")
    private Long dictId;

    /** 所属应用ID */
    @Excel(name = "所属应用ID")
    private Long appId;

    /** 所属应用CODE */
    @Excel(name = "所属应用CODE")
    private Long appCode;

    /** 发布用户id */
    @Excel(name = "发布用户id")
    private Long categoryManagerId;

    /** 发布时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发布时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date categoryDatetime;

    /** 栏目内容模型id */
    @Excel(name = "栏目内容模型id")
    private String mdiyModelId;

    /** 自定义链接 */
    @Excel(name = "自定义链接")
    private String categoryDiyUrl;

    /** 内容模板 */
    @Excel(name = "内容模板")
    private String categoryUrl;

    /** 列表模板 */
    @Excel(name = "列表模板")
    private String categoryListUrl;

    /** 自定义顺序 */
    @Excel(name = "自定义顺序")
    private Long categorySort;

    /** 拼音检索 */
    @Excel(name = "拼音检索")
    private String categoryPinyin;

    /** 分类层级 */
    @Excel(name = "分类层级")
    private String level;

    /** 备用1 */
    @Excel(name = "备用1")
    private String back1;

    /** 备用2 */
    @Excel(name = "备用2")
    private String back2;

    /** 备用3 */
    @Excel(name = "备用3")
    private String back3;

    /** 备用4 */
    @Excel(name = "备用4")
    private String back4;

    /** 备用5 */
    @Excel(name = "备用5")
    private String back5;


    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    /** 子栏目 */
    private List<CmsCategory> children = new ArrayList<CmsCategory>();

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setDel(Integer del) 
    {
        this.del = del;
    }

    public Integer getDel() 
    {
        return del;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setCategoryTitle(String categoryTitle) 
    {
        this.categoryTitle = categoryTitle;
    }

    public String getCategoryTitle() 
    {
        return categoryTitle;
    }
    public void setCategoryType(String categoryType) 
    {
        this.categoryType = categoryType;
    }

    public String getCategoryType() 
    {
        return categoryType;
    }
    public void setCategoryParentId(Long categoryParentId)
    {
        this.categoryParentId = categoryParentId;
    }

    public Long getCategoryParentId()
    {
        return categoryParentId;
    }
    public void setCategoryParentName(String categoryParentName)
    {
        this.categoryParentName = categoryParentName;
    }

    public String getCategoryParentName() 
    {
        return categoryParentName;
    }
    public void setCategoryImg(String categoryImg) 
    {
        this.categoryImg = categoryImg;
    }

    public String getCategoryImg() 
    {
        return categoryImg;
    }
    public void setCategoryFlag(String categoryFlag) 
    {
        this.categoryFlag = categoryFlag;
    }

    public String getCategoryFlag() 
    {
        return categoryFlag;
    }
    public void setCategoryDescrip(String categoryDescrip) 
    {
        this.categoryDescrip = categoryDescrip;
    }

    public String getCategoryDescrip() 
    {
        return categoryDescrip;
    }
    public void setCategoryKeyword(String categoryKeyword) 
    {
        this.categoryKeyword = categoryKeyword;
    }

    public String getCategoryKeyword() 
    {
        return categoryKeyword;
    }
    public void setCategoryPath(String categoryPath) 
    {
        this.categoryPath = categoryPath;
    }

    public String getCategoryPath() 
    {
        return categoryPath;
    }
    public void setDictId(Long dictId) 
    {
        this.dictId = dictId;
    }

    public Long getDictId() 
    {
        return dictId;
    }
    public void setAppId(Long appId) 
    {
        this.appId = appId;
    }

    public Long getAppId() 
    {
        return appId;
    }
    public void setAppCode(Long appCode) 
    {
        this.appCode = appCode;
    }

    public Long getAppCode() 
    {
        return appCode;
    }
    public void setCategoryManagerId(Long categoryManagerId) 
    {
        this.categoryManagerId = categoryManagerId;
    }

    public Long getCategoryManagerId() 
    {
        return categoryManagerId;
    }
    public void setCategoryDatetime(Date categoryDatetime) 
    {
        this.categoryDatetime = categoryDatetime;
    }

    public Date getCategoryDatetime() 
    {
        return categoryDatetime;
    }
    public void setMdiyModelId(String mdiyModelId) 
    {
        this.mdiyModelId = mdiyModelId;
    }

    public String getMdiyModelId() 
    {
        return mdiyModelId;
    }
    public void setCategoryDiyUrl(String categoryDiyUrl) 
    {
        this.categoryDiyUrl = categoryDiyUrl;
    }

    public String getCategoryDiyUrl() 
    {
        return categoryDiyUrl;
    }
    public void setCategoryUrl(String categoryUrl) 
    {
        this.categoryUrl = categoryUrl;
    }

    public String getCategoryUrl() 
    {
        return categoryUrl;
    }
    public void setCategoryListUrl(String categoryListUrl) 
    {
        this.categoryListUrl = categoryListUrl;
    }

    public String getCategoryListUrl() 
    {
        return categoryListUrl;
    }
    public void setCategorySort(Long categorySort) 
    {
        this.categorySort = categorySort;
    }

    public Long getCategorySort() 
    {
        return categorySort;
    }
    public void setCategoryPinyin(String categoryPinyin) 
    {
        this.categoryPinyin = categoryPinyin;
    }

    public String getCategoryPinyin() 
    {
        return categoryPinyin;
    }
    public void setBack1(String back1) 
    {
        this.back1 = back1;
    }

    public String getBack1() 
    {
        return back1;
    }
    public void setBack2(String back2) 
    {
        this.back2 = back2;
    }

    public String getBack2() 
    {
        return back2;
    }
    public void setBack3(String back3) 
    {
        this.back3 = back3;
    }

    public String getBack3() 
    {
        return back3;
    }
    public void setBack4(String back4) 
    {
        this.back4 = back4;
    }

    public String getBack4() 
    {
        return back4;
    }
    public void setBack5(String back5) 
    {
        this.back5 = back5;
    }

    public String getBack5() 
    {
        return back5;
    }


    public List<CmsCategory> getChildren()
    {
        return children;
    }

    public void setChildren(List<CmsCategory> children)
    {
        this.children = children;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("del", getDel())
            .append("status", getStatus())
            .append("categoryTitle", getCategoryTitle())
            .append("categoryType", getCategoryType())
            .append("categoryParentId", getCategoryParentId())
            .append("categoryParentName", getCategoryParentName())
            .append("categoryImg", getCategoryImg())
            .append("categoryFlag", getCategoryFlag())
            .append("categoryDescrip", getCategoryDescrip())
            .append("categoryKeyword", getCategoryKeyword())
            .append("categoryPath", getCategoryPath())
            .append("dictId", getDictId())
            .append("appId", getAppId())
            .append("appCode", getAppCode())
            .append("categoryManagerId", getCategoryManagerId())
            .append("categoryDatetime", getCategoryDatetime())
            .append("mdiyModelId", getMdiyModelId())
            .append("categoryDiyUrl", getCategoryDiyUrl())
            .append("categoryUrl", getCategoryUrl())
            .append("categoryListUrl", getCategoryListUrl())
            .append("categorySort", getCategorySort())
            .append("categoryPinyin", getCategoryPinyin())
                .append("level", getLevel())
            .append("back1", getBack1())
            .append("back2", getBack2())
            .append("back3", getBack3())
            .append("back4", getBack4())
            .append("back5", getBack5())
            .toString();
    }
}
