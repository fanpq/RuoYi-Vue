package com.ruoyi.system.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 内容文章对象 cms_content
 *
 * @author fanpq
 * @date 2020-12-20
 */
public class CmsContent extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;

    /**
     * 删除标志默认:0,删除:1
     */
    @Excel(name = "删除标志默认:0,删除:1")
    private Integer del;

    /**
     * 状态0:待审核;1:发布;2:取消发布;
     */
    @Excel(name = "状态0:待审核;1:发布;2:取消发布;")
    private String status;

    /**
     * 内容标题
     */
    @Excel(name = "内容标题")
    private String contentTitle;

    /**
     * 所属栏目
     */
    @Excel(name = "所属栏目")
    private String contentCategoryId;

    /**
     * 所属应用ID
     */
    @Excel(name = "所属应用ID")
    private Long appId;

    /**
     * 所属应用CODE
     */
    @Excel(name = "所属应用CODE")
    private Long appCode;

    /**
     * 跳转链接地址
     */
    @Excel(name = "跳转链接地址")
    private String contentUrl;

    /**
     * 关键字
     */
    @Excel(name = "关键字")
    private String contentKeyword;

    /**
     * 缩略图
     */
    @Excel(name = "缩略图")
    private String contentImg;
    /**
     * 文章图片1
     */
    @Excel(name = "文章图片1")
    private String contentImg1;
    /**
     * 文章图片2
     */
    @Excel(name = "文章图片2")
    private String contentImg2;
    /**
     * 文章图片3
     */
    @Excel(name = "文章图片3")
    private String contentImg3;
    /**
     * 文章图片4
     */
    @Excel(name = "文章图片4")
    private String contentImg4;

    /**
     * 自定义顺序
     */
    @Excel(name = "自定义顺序")
    private Long contentSort;

    /**
     * 发布时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发布时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date contentDatetime;

    /**
     * 来源
     */
    @Excel(name = "来源")
    private String contentSource;

    /**
     * 作者
     */
    @Excel(name = "作者")
    private String contentAuthor;

    /**
     * 是否显示
     */
    @Excel(name = "是否显示")
    private String contentDisplay;

    /**
     * 内容类型，来源字典
     */
    @Excel(name = "内容类型，来源字典")
    private String contentType;

    /**
     * 点击次数
     */
    @Excel(name = "点击次数")
    private Long contentHit;

    /**
     * 描述
     */
    @Excel(name = "描述")
    private String contentDescription;

    /**
     * 备用1
     */
    @Excel(name = "备用1")
    private String back1;

    /**
     * 备用2
     */
    @Excel(name = "备用2")
    private String back2;

    /**
     * 备用3
     */
    @Excel(name = "备用3")
    private String back3;

    /**
     * 备用4
     */
    @Excel(name = "备用4")
    private String back4;

    /**
     * 备用5
     */
    @Excel(name = "备用5")
    private String back5;

    /**
     * 内容
     */
    @Excel(name = "内容")
    private String contentDetails;

    /**
     * 所属栏目名称
     */
    @Excel(name = "所属栏目名称")
    private String contentCategoryTitle;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setDel(Integer del) {
        this.del = del;
    }

    public Integer getDel() {
        return del;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setContentTitle(String contentTitle) {
        this.contentTitle = contentTitle;
    }

    public String getContentTitle() {
        return contentTitle;
    }

    public void setContentCategoryId(String contentCategoryId) {
        this.contentCategoryId = contentCategoryId;
    }

    public String getContentCategoryId() {
        return contentCategoryId;
    }

    public void setAppId(Long appId) {
        this.appId = appId;
    }

    public Long getAppId() {
        return appId;
    }

    public void setAppCode(Long appCode) {
        this.appCode = appCode;
    }

    public Long getAppCode() {
        return appCode;
    }

    public void setContentUrl(String contentUrl) {
        this.contentUrl = contentUrl;
    }

    public String getContentUrl() {
        return contentUrl;
    }

    public void setContentKeyword(String contentKeyword) {
        this.contentKeyword = contentKeyword;
    }

    public String getContentKeyword() {
        return contentKeyword;
    }

    public void setContentImg(String contentImg) {
        this.contentImg = contentImg;
    }

    public String getContentImg() {
        return contentImg;
    }

    public void setContentSort(Long contentSort) {
        this.contentSort = contentSort;
    }

    public Long getContentSort() {
        return contentSort;
    }

    public void setContentDatetime(Date contentDatetime) {
        this.contentDatetime = contentDatetime;
    }

    public Date getContentDatetime() {
        return contentDatetime;
    }

    public void setContentSource(String contentSource) {
        this.contentSource = contentSource;
    }

    public String getContentSource() {
        return contentSource;
    }

    public void setContentAuthor(String contentAuthor) {
        this.contentAuthor = contentAuthor;
    }

    public String getContentAuthor() {
        return contentAuthor;
    }

    public void setContentDisplay(String contentDisplay) {
        this.contentDisplay = contentDisplay;
    }

    public String getContentDisplay() {
        return contentDisplay;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentHit(Long contentHit) {
        this.contentHit = contentHit;
    }

    public Long getContentHit() {
        return contentHit;
    }

    public void setContentDescription(String contentDescription) {
        this.contentDescription = contentDescription;
    }

    public String getContentDescription() {
        return contentDescription;
    }

    public void setBack1(String back1) {
        this.back1 = back1;
    }

    public String getBack1() {
        return back1;
    }

    public void setBack2(String back2) {
        this.back2 = back2;
    }

    public String getBack2() {
        return back2;
    }

    public void setBack3(String back3) {
        this.back3 = back3;
    }

    public String getBack3() {
        return back3;
    }

    public void setBack4(String back4) {
        this.back4 = back4;
    }

    public String getBack4() {
        return back4;
    }

    public void setBack5(String back5) {
        this.back5 = back5;
    }

    public String getBack5() {
        return back5;
    }

    public void setContentDetails(String contentDetails) {
        this.contentDetails = contentDetails;
    }

    public String getContentDetails() {
        return contentDetails;
    }

    public String getContentCategoryTitle() {
        return contentCategoryTitle;
    }

    public void setContentCategoryTitle(String contentCategoryTitle) {
        this.contentCategoryTitle = contentCategoryTitle;
    }

    public String getContentImg1() {
        return contentImg1;
    }

    public void setContentImg1(String contentImg1) {
        this.contentImg1 = contentImg1;
    }

    public String getContentImg2() {
        return contentImg2;
    }

    public void setContentImg2(String contentImg2) {
        this.contentImg2 = contentImg2;
    }

    public String getContentImg3() {
        return contentImg3;
    }

    public void setContentImg3(String contentImg3) {
        this.contentImg3 = contentImg3;
    }

    public String getContentImg4() {
        return contentImg4;
    }

    public void setContentImg4(String contentImg4) {
        this.contentImg4 = contentImg4;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("del", getDel())
                .append("status", getStatus())
                .append("contentTitle", getContentTitle())
                .append("contentCategoryId", getContentCategoryId())
                .append("appId", getAppId())
                .append("appCode", getAppCode())
                .append("contentUrl", getContentUrl())
                .append("contentKeyword", getContentKeyword())
                .append("contentImg", getContentImg())
                .append("contentImg1", getContentImg1())
                .append("contentImg2", getContentImg2())
                .append("contentImg3", getContentImg3())
                .append("contentImg4", getContentImg4())
                .append("contentSort", getContentSort())
                .append("contentDatetime", getContentDatetime())
                .append("contentSource", getContentSource())
                .append("contentAuthor", getContentAuthor())
                .append("contentDisplay", getContentDisplay())
                .append("contentType", getContentType())
                .append("contentHit", getContentHit())
                .append("contentDescription", getContentDescription())
                .append("back1", getBack1())
                .append("back2", getBack2())
                .append("back3", getBack3())
                .append("back4", getBack4())
                .append("back5", getBack5())
                .append("contentDetails", getContentDetails())
                .append("contentCategoryTitle", getContentCategoryTitle())
                .toString();
    }
}
